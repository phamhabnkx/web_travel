@extends('frontend.master.master')
@section('name','Trang chủ')
@section('content')
		  <div class="main-body">
            <div class="video-area" id="video-area">
                <div class="player" id="video-play" data-property="{videoURL:'{{$data->link_video_background}}', containment:'#video-area', showControls:false, autoPlay:true, zoom:0, loop:true, mute:true, startAt:1, opacity:1, quality:'low',}"></div>
                <div class="video-table">
                    <div class="video-table-cell">
                        <div class="container center-holder">
                            <div class="video-effect">
                                <div class="video-effect-box">
                                    <div class="video-effect-content">
                                        <h2 data-animate-scroll='{"x": "-20","y": "-20","rotation": "-3","alpha": "0", "duration": ".8"}'>Hãy để chúng tôi đưa bạn đi</h2>
                                        <div class="home_search">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="home_search_container">
                                                            <div class="home_search_title">Tìm kiếm chuyến đi</div>
                                                            <div class="home_search_content">
                                                                  <form action="/search" class="home_search_form" method="get" id="home_search_form">
                                                                    <div class="d-flex flex-lg-row flex-column align-items-start justify-content-lg-between justify-content-start">
                                                                        <input type="text" class="search_input search_input_1" placeholder="vd : Đà lạt" name="thanh_pho" required="required">
                                                                        
                                                                        <input type="text" class="search_input search_input_4" placeholder="Chi phí tối thiểu : 1000000 " name="min" required="required">
                                                                        <input type="text" class="search_input search_input_4" placeholder="Chi phí tối đa 1000000000000" name="max" required="required">
                                                                        <button class="home_search_button">Tìm kiếm</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="home_page_nav">
                                            <ul class="d-flex flex-column align-items-end justify-content-end">
                                                <li><a href="#" data-scroll-to="#dia_diem" style="font-family: sans-serif;">Địa điểm phổ biến<span>01</span></a></li>
                                                <li><a href="#" data-scroll-to="#nhan_xet" style="font-family: sans-serif;">Nhận xét & đánh giá<span>02</span></a></li>
                                                <li><a href="#" data-scroll-to="#tin_tuc_new" style="font-family: sans-serif;">Tin tức<span>03</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="destinations" id="dia_diem">
                <div class="container">
                    <div class="row">
                        <div class="col text-center" >
                            
                            <div class="section_title">
                                <h2>Kết quả tìm kiếm</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row destinations_row">
                        <div class="col">
                            <div class="destinations_container item_grid">
                                <!-- Destination -->
                                @foreach ($search as $element)
                                    <div class="destination item col-lg-3" title="{{$element->name}}">
                                        <div class="destination-item-view">
                                            <div class="destination_image">
                                            
                                                <div class="view-eye">
                                                  
                                                    <img src="{{$element->avatar}}" alt="">
                                                    
                                                </div>
                                              

                                            </div>
                                            <div class="destination_content" style="margin-bottom: 40px;">
                                                <div class="destination_title"><a href="" title="{{$element->address->name}}" style="font-family:initial; font-size: 18px; ">{{$element->address->name}}</a></div>
                                                <div class="destination_subtitle">
                                                    <p>{{$element->name}}</p>
                                                </div>
                                                <div class="destination_price">{{number_format($element->price,0,'.','.')}} đ</div>
                                               
                                            </div>
                                            <div class="mt-30 mb-15 float-item-destination">
                                                <a href="/tour/tour-details/{{$element->slug}}-{{$element->id}}.html" class="dark-button button-md">Khám phá</a>
                                                {{-- <a href="/tour/tour-details/{{$element->slug}}-{{$element->id}}.html" class="dark-button button-md" style="background:#FF9600;">Đặt tour</a>  --}} 
                                            </div>  
                                        </div>
                                        

                                    </div>
                                @endforeach
                               
                               
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>


         
           
        </div>
   
@endsection


