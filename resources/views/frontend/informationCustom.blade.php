@extends('frontend.master.master')
@section('name','Đăng nhập')
@section('content')

      <div class="login-page-content-area">
    <div class="container">
		@if(session('fail'))
            <div class="alert alert-danger" role="alert">
               Bạn chưa tích vào ô đồng ý
              
          
            </div>
        @endif
        @if(session('error'))
            <div class="alert alert-danger" role="alert">
               Your current password does not matches with the password you provided. Please try again.
              
          
            </div>
        @endif
        @if(session('errors1'))
            <div class="alert alert-danger" role="alert">
               New Password cannot be same as your current password. Please choose a different password.
              
          
            </div>
        @endif
        @if(session('danger'))
            <div class="alert alert-danger" role="alert">
               email not found
              
          
            </div>
        @endif
        @if(session('password_confirm'))
            <div class="alert alert-danger" role="alert">
               Mật khẩu bạn vừa tạo không đúng khi xác nhận
              
          
            </div>
        @endif
         @if(session('success'))
            <div class="alert alert-success" role="alert">
               Password changed successfully !
              
          
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12 col-md-12">
              <form action="" method="POST"  accept-charset="utf-8" enctype="multipart/form-data">
     			@csrf
                <div class="login-page-wrapper"><!-- login page wrapper -->
                    <div class="or hidden-xs hidden-sm">
                        <span>or</span>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                            <div class="left-content-area">
                                <div class="top-content">
                                    <h4 class="title">Chào mừng quay trở lại</h4>
                                </div>
                                <div class="bottom-content">
                                    <div class="left-content">
                                        <div class="thumb">
                                        	
                                             <div class="form-group">
			                                  
			                                    <input id="img" type="file" name="avatar"  class="form-control hidden"
			                                        onchange="changeImg(this)">
			                                     @if (Auth::user()->avatar == null)
			                                     	<img id="avatar" class="thumbnail" style="max-width: none;width: 275px; height: 275px" src="/upload_avatar/no-img.jpg">
			                                     @else
			                                     	<img id="avatar" class="thumbnail" style="max-width: none;width: 275px; height: 275px" src="{{$user['avatar']}}">
			                                     @endif 
			                                    
			                                   
			                                </div>
                                        </div>

                                    </div>
                                     
                                   
                                </div>
                            </div>
                        </div>
                        <div class="col-lg- col-md-6 col-xs-12 col-sm-12">
                            <div class="right-contnet-area">
                                <div class="top-content">
                                    <h4 class="title">Thông tin tài khoản</h4>
                                </div>
                                <div class="bottom-content">
                                	<div class="login-form">
                                		<div class="form-element">
                                            <input name="full" readonly class="input-field" style="color: black" value="{{$user['full']}}">
                                           
                                        </div>
                                		<div class="form-element">
                                            <input type="email" name="email" readonly class="input-field" style="color: black" value="{{$user['email']}}" placeholder="Nhập tên người dùng hoặc email">
                                           
                                        </div>
                                        <div class="form-element">
                                            <input type="password" name="password_old" class="input-field" placeholder="Vui lòng nhập mật khẩu hiện tại của bạn">
                                            @if ($errors->has('password_old'))
                                            	<span class="invalid-feedback" style="display: block; color:red;">
                                            		<strong>
                                            			{{$errors->first('password_old')}}
                                            		</strong>
                                            	</span>
                                            @endif
                                            
                                        </div>

                                         <div class="form-element">
                                            <input type="password" name="password_new" class="input-field" placeholder="Mật khẩu mới">
                                           
                                        </div>
                                         @if ($errors->has('password_new'))
                                            	<span class="invalid-feedback" style="display: block; color:red;">
                                            		<strong>
                                            			{{$errors->first('password_new')}}
                                            		</strong>
                                            	</span>
                                            @endif
                                        <div class="form-element">
                                            <input type="password" name="password_confirm" class="input-field" placeholder="Xác nhận mật khẩu mới">
                                           
                                        </div>
                                        @if ($errors->has('password_confirm'))
                                            	<span class="invalid-feedback" style="display: block; color:red;">
                                            		<strong>
                                            			{{$errors->first('password_confirm')}}
                                            		</strong>
                                            	</span>
                                            @endif
                                         <div class="form-element">
                                            <div class="g-recaptcha" data-sitekey="{{env('CAPTCHA_KEY')}}"></div>
                                            @if ($errors->has('g-recaptcha-response'))
                                            	<span class="invalid-feedback" style="display: block; color:red;">
                                            		<strong>
                                            			{{$errors->first('g-recaptcha-response')}}
                                            		</strong>
                                            	</span>
                                            @endif
                                           
                                        </div>
                                        
                                        <div class="checkbox">
                                          <label>

                                            <input name="check" type="checkbox" value="1">Tôi đồng ý
                                             @if ($errors->has('check'))
                                            	<span class="invalid-feedback" style="display: block; color:red;">
                                            		<strong>
                                            			{{$errors->first('check')}}
                                            		</strong>
                                            	</span>
                                            @endif
                                          </label>
                                          
                                        </div>
                                        <div class="btn-wrapper">
                                            <button type="submit" class="submit-btn-login">Xác nhận</button>
                                          
                                            
                                            
                                        </div>
                                	</div>
                                    
                                        
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                 </form>
            </div>

        </div>
    </div>
  </div>
  
   
    

@endsection
@section('avatar-change')
<script>
	function changeImg(input){
		    //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
		    if(input.files && input.files[0]){
		        var reader = new FileReader();
		        //Sự kiện file đã được load vào website
		        reader.onload = function(e){
		            //Thay đổi đường dẫn ảnh
		            $('#avatar').attr('src',e.target.result);
		        }
		        reader.readAsDataURL(input.files[0]);
		    }
		}
		$(document).ready(function() {
		    $('#avatar').click(function(){
		        $('#img').click();
		    });
		});
		
</script>
@stop