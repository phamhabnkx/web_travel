<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
        <title>@yield('name')</title>
        <link rel="stylesheet" href="">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=vietnamese" rel="stylesheet">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <base href="{{asset('frontend')}}/" target="_blank, _self, _parent, _top">
       
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
        <link rel="stylesheet" href="asset/css/bootstrap.min.css">
        <link rel="stylesheet" href="boostrap-touchspin/jquery.bootstrap-touchspin.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">

         <link rel="stylesheet" type="text/css" href="lib/bootstrap-datepicker.css" >
        <link rel="stylesheet" href="asset/css/owl.carousel.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick-theme.css"/>
        <link rel="stylesheet" type="text/css" href="asset/css/asset.css">
        <link rel="stylesheet" href="dist/css/swiper.min.css">
    </head>
    <body>
       @include('frontend.master.header')
        <!-- /header -->
        <!-- content -->
       @yield('content')
        <!-- end Content -->
        <!-- footer -->
        @include('frontend.master.subcrise')
        @include('frontend.master.footer')       
          
      
        
        <div class="addthis-smartlayers addthis-smartlayers-desktop" aria-labelledby="at4-share-label" role="region">
            <div id="at4-share-label">AddThis Sharing Sidebar</div>
            <div id="at4-share" class="at4-share addthis_32x32_style atss atss-left addthis-animated slideInLeft">
                <a href="#" tabindex="0" class="at-share-btn at-svc-pinterest_share">
                    <span class="at4-visually-hidden">Share to Pinterest</span>
                    <span class="at-icon-wrapper" style="background-color: rgb(203, 32, 39);">
                        <svg xmlns="" xmlns:xlink="" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-pinterest_share-1" class="at-icon at-icon-pinterest_share">
                            <title id="at-svg-pinterest_share-1">Pinterest</title>
                            <g>
                                <path d="M7 13.252c0 1.81.772 4.45 2.895 5.045.074.014.178.04.252.04.49 0 .772-1.27.772-1.63 0-.428-1.174-1.34-1.174-3.123 0-3.705 3.028-6.33 6.947-6.33 3.37 0 5.863 1.782 5.863 5.058 0 2.446-1.054 7.035-4.468 7.035-1.232 0-2.286-.83-2.286-2.018 0-1.742 1.307-3.43 1.307-5.225 0-1.092-.67-1.977-1.916-1.977-1.692 0-2.732 1.77-2.732 3.165 0 .774.104 1.63.476 2.336-.683 2.736-2.08 6.814-2.08 9.633 0 .87.135 1.728.224 2.6l.134.137.207-.07c2.494-3.178 2.405-3.8 3.533-7.96.61 1.077 2.182 1.658 3.43 1.658 5.254 0 7.614-4.77 7.614-9.067C26 7.987 21.755 5 17.094 5 12.017 5 7 8.15 7 13.252z" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                        <div class="at4-share-count-container"><span class="at4-share-count"><span class="at4-visually-hidden">, Number of shares</span></span></div>
                    </span>
                </a>
                <a role="button" tabindex="0" class="at-share-btn at-svc-linkedin">
                    <span class="at4-visually-hidden">Share to LinkedIn</span>
                    <span class="at-icon-wrapper" style="background-color: rgb(0, 119, 181);">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-linkedin-2" class="at-icon at-icon-linkedin">
                            <title id="at-svg-linkedin-2">LinkedIn</title>
                            <g>
                                <path d="M26 25.963h-4.185v-6.55c0-1.56-.027-3.57-2.175-3.57-2.18 0-2.51 1.7-2.51 3.46v6.66h-4.182V12.495h4.012v1.84h.058c.558-1.058 1.924-2.174 3.96-2.174 4.24 0 5.022 2.79 5.022 6.417v7.386zM8.23 10.655a2.426 2.426 0 0 1 0-4.855 2.427 2.427 0 0 1 0 4.855zm-2.098 1.84h4.19v13.468h-4.19V12.495z" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </span>
                </a>
                <a role="button" tabindex="0" class="at-share-btn at-svc-google">
                    <span class="at4-visually-hidden">Share to Google Bookmark</span>
                    <span class="at-icon-wrapper" style="background-color: rgb(66, 133, 244);">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-google-3" class="at-icon at-icon-google">
                            <title id="at-svg-google-3">Google Bookmark</title>
                            <g>
                                <path d="M16.213 13.998H26.72c.157.693.28 1.342.28 2.255C27 22.533 22.7 27 16.224 27 10.03 27 5 22.072 5 16S10.03 5 16.224 5c3.03 0 5.568 1.09 7.51 2.87l-3.188 3.037c-.808-.748-2.223-1.628-4.322-1.628-3.715 0-6.745 3.024-6.745 6.73 0 3.708 3.03 6.733 6.744 6.733 4.3 0 5.882-2.915 6.174-4.642h-6.185V14z" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </span>
                </a>
                <a role="button" tabindex="0" class="at-share-btn at-svc-facebook">
                    <span class="at4-visually-hidden">Share to Facebook</span>
                    <span class="at-icon-wrapper" style="background-color: rgb(59, 89, 152);">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-facebook-4" class="at-icon at-icon-facebook">
                            <title id="at-svg-facebook-4">Facebook</title>
                            <g>
                                <path d="M22 5.16c-.406-.054-1.806-.16-3.43-.16-3.4 0-5.733 1.825-5.733 5.17v2.882H9v3.913h3.837V27h4.604V16.965h3.823l.587-3.913h-4.41v-2.5c0-1.123.347-1.903 2.198-1.903H22V5.16z" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                        <div class="at4-share-count-container"><span class="at4-share-count"><span class="at4-visually-hidden">, Number of shares</span></span></div>
                    </span>
                </a>
                <a role="button" tabindex="0" class="at-share-btn at-svc-compact">
                    <span class="at4-visually-hidden">More AddThis Share options</span>
                    <span class="at-icon-wrapper" style="background-color: rgb(255, 101, 80);">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-addthis-5" class="at-icon at-icon-addthis">
                            <title id="at-svg-addthis-5">AddThis</title>
                            <g>
                                <path d="M18 14V8h-4v6H8v4h6v6h4v-6h6v-4h-6z" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                        <div class="at4-share-count-container"><span class="at4-share-count"><span class="at4-visually-hidden">, Number of shares</span></span></div>
                    </span>
                </a>
              
            </div>
            <div id="at4-soc" class="at-share-open-control at-share-open-control-left ats-transparent at4-hide" title="Show">
                <div class="at4-arrow at-right">Show</div>
            </div>
        </div>
       
        
        <div class="back-to-top bg-orange" style="display: block;">
            <i class="fas fa-rocket"></i>
        </div>
        @section('script')
        <!--   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            -->   <script src="asset/js/jquery.min.js"></script>
            <script type="text/javascript" src="lib/bootstrap-datepicker.js"></script>
        <script src="asset/js/jquery.mb.YTPlayer.min.js"></script>
        <!--  <script src="asset/js/jquery-3.3.1.min.js"></script> -->
        <script src="asset/js/owl.carousel.js"></script>
        <script src="greensock-js/EaselPlugin.min.js"></script>
        <script src="greensock-js/ScrollToPlugin.min.js"></script>
        <script src="greensock-js/TweenMax.min.js"></script>
        <script src="animate-scroll/animate-scroll.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.min.js"></script>
        <script src="asset/js/bootstrap.min.js"></script>
        <script src="boostrap-touchspin/jquery.bootstrap-touchspin.js"></script>
        <!--  <script src="//cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script> -->
        <script src="plugins/parallax-js-master/parallax.min.js"></script>
        <script src="https://www.google.com/recaptcha/api.js"></script>
        <script src="asset/js/jquery.counterup.min.js"></script>
        <script src="asset/js/waypoints.min.js"></script>
        <script src="asset/js/app.js"></script>
        <script>
            $(document).animateScroll(); 
         

        </script>
        @yield('data-cart-update')
        @yield('avatar-change')
        @yield('script-rating')
        @yield('script-index')
        <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5e05ea477e39ea1242a1ff13/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<script src={{ url('ckeditor/ckeditor.js') }}></script>
    <script>
  
    CKEDITOR.replace( "editor", {
        filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',

    } );
    </script>
    @include('ckfinder::setup')
<!--End of Tawk.to Script-->

    </body>
</html>