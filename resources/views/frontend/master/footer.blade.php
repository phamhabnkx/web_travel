<footer>
    <!-- <div id="particles-js-footer" style="height: 150px;"></div> -->
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12 footer-name">
                <a href="#">FriendsTravel</a>
               
                
            </div>
            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="footer-title">
                    <h2>Bản đồ</h2>
                </div>
                <ul>
                    <li><a href="#">Trang chủ</a></li>
                    <li><a href="#">Dịch vụ</a></li>
                    <li><a href="#">Du lịch</a></li>
                    <li><a href="#">Liên hệ</a></li>
                </ul>               
            </div>
            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="footer-title">
                    <h2>Liên hệ</h2>                 
                </div>
                <ul>
                    <li>Số điện thoại</li>
                    <li>+0987888888</li>
                    <li></li>
                    <li>Mail</li>
                    <li>dant20191@gmail.com</li>                       
                </ul>               
            </div>
            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="footer-title">
                    <h2>Visit Us</h2>
                </div>
                <ul>
                    <li>Địa chỉ</li>
                    <li>C9, Đại học Bách Khoa Hà Nội, Hà Nội, Việt Nam</li>                   
                </ul>                   
            </div>
            <div class="col-md-2 col-sm-4 col-xs-12">
                <div class="footer-title">
                    <h2>Tìm kiếm</h2>
                </div>
                <form method="get">
                    <input type="text" name="s" placeholder="Search ...">
                </form>
                 <div class="mt-50 center-holder">
                    <a href="#"><img src="images/fb.png" alt=""></a>
                    <a href="#"><img src="images/yt.png" alt=""></a>
                   
                    <a href="#"><img src="images/ins.png" alt=""></a>
                </div>               
            </div>                                              
        </div>
    </div>
</footer>