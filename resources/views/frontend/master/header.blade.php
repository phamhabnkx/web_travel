 <header id="header" class="">
            <div class="support-aria"></div>
             <div class="menu-main bg-organe">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 ">
                            <div class="left-content-bar-hidden">
                                <a href="/" title="">
                                FriendsTravel.
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 padding-left-menu">
                            <ul class="menu-2 menu3">
                                <li class="float-left-none"><a href="/" title="">Trang chủ</i></a></li>
                               <!--  <li class="float-left-none"><a href="about.html" title="">Giới thiệu</a></li> -->
                                <!-- <li class="float-left-none"><a href="about.html" title="">Du lịch</a></li> -->
                               
                                <li class="nav-item-blog float-left-none">
                                    <a href="/tour" title="">Du lịch</a>
                                    <div class="dropdown-menu">
                                        <i class="up-item fas fa-caret-up"></i>
                                        <a href="/tour" class="dropdown-item">Du lịch trong nước</a>
                                        <a href="/tour" class="dropdown-item">Du lịch nước ngoài</a>
                                        
                                    </div>
                                </li>
                                {{-- {{dd(Auth::check())}} --}}
                               <!--  <li class="nav-item-blog float-left-none">
                                    <a href="about.html" title="">Dịch vụ</a>
                                    <div class="dropdown-menu">
                                        <i class="up-item fas fa-caret-up"></i>
                                        <a href="blog.html" class="dropdown-item">Khách sạn</a>
                                        <a href="blog-details.html" class="dropdown-item">Vé máy bay</a>
                                        <a href="blog-details.html" class="dropdown-item">Thuê xe</a>
                                    </div>
                                </li> -->
                                <li class="nav-item-blog float-left-none">
                                    <a href="/tin-tuc" title="">Tin tức</a>
                                    <div class="dropdown-menu">
                                        <i class="up-item fas fa-caret-up"></i>
                                       
                                        <a href="/tin-tuc" class="dropdown-item">Cẩm nang du lịch</a>
                                        <a href="/tin-tuc" class="dropdown-item">Kinh nghiệm du lịch</a>
                                        <a href="/tin-tuc" class="dropdown-item">Thông tin du lịch</a>
                                    </div>
                                </li>
                                <li><a href="/contact" title="">Liên hệ</a></li>
                                 <li class="float-left-none">
                                    <a href="/cart" title="">
                                        <img src="images/i-cart.png" alt="">
                                    </a>
                                    <span class="badge">{{count(Cart::content())}}</span>
                                </li>
                                @if (Auth::check())
                                    <li class="nav-item-blog float-left-none">
                                        <a href="/" title="{{Auth::user()->full}}" style="display: block;width: 100px;overflow: hidden;white-space: nowrap;text-overflow: Ellipsis;">{{Auth::user()->full}}</a>
                                        <div class="dropdown-menu">
                                            <i class="up-item fas fa-caret-up"></i>
                                           
                                            <a href="/inforCus" class="dropdown-item">Thông tin cá nhân</a>
                                            <a href="/logout" class="dropdown-item">Đăng Xuất</a>
                                           
                                        </div>
                                    </li>
                                @else
                                    <li  class="nav-item-blog float-left-none">
                                        <a href="/login" title="">Đăng nhập</a>
                                        <div class="dropdown-menu">
                                            <i class="up-item fas fa-caret-up"></i>
                                           
                                            <a href="/login" class="dropdown-item">Đăng kí/ Đăng Nhập</a>
                                            <a href="/facebook/redirect" class="dropdown-item">Facebook</a>
                                            <a href="/google/redirect" class="dropdown-item">Google</a>
                                           
                                        </div>
                                    </li>
                                @endif
                                
                                <li class="search right-btn-wrapper" id="search"><i class="fas fa-search"></i> </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="body-overlay" id="body-overlay">
                <div class="x-hidden"><i class="fas fa-times"></i></div>
                <div class="search-popup home-3 animated fadeInDown delay-10s" id="search-popup">
                    <form action="/search/second" method="get" class="search-popup-form">
                        <div class="form-element">
                            <input type="text" name="search" class="input-field" placeholder="Search.....">
                        </div>
                        <button type="submit" class="submit-btn-hidden"><i class="fas fa-search"></i></button>
                    </form>
                </div>
            </div>
        </header>