@extends('frontend.master.master')
@section('name','Tin tức chi tiết')
@section('content')
	 <div class="n3-breadcrumb">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 ">
                            <div class="breadcrumb">
                                <span itemscope="" itemtype="">
                                    <a href="" itemprop="url">
                                        <span itemprop="title" style="color: #ff002d;">Du lịch</span>
                                    </a>
                                </span>
                                 » 
                                 <span itemscope="" itemtype="">
                                    <a href="" itemprop="url">
                                        <span itemprop="title">Tin tức</span>
                                    </a>
                                </span>
                                  » 
                                 <span itemscope="" itemtype="">
                                    <a href="" itemprop="url">
                                        <span itemprop="title">{{$blogDetails->blogDetails->name}}</span>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section class="blog-details-content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="single-blog-post">
                                <div class="meta-time">
                                    <span class="date">{{$date}}</span>
                                    <span class="month">
                                    @switch($month)
                                        @case(1)
                                            Jan
                                            @break
                                        @case(2)
                                            Feb
                                            @break 
                                        @case(3)
                                            Mar
                                            @break 
                                       @case(4)
                                            Apr
                                            @break 
                                        @case(5)
                                            May
                                            @break 
                                        @case(6)
                                            Jun
                                            @break 
                                        @case(7)
                                            Jul
                                            @break 
                                        @case(8)
                                            Aug
                                            @break 
                                        @case(9)
                                           Sep
                                            @break 
                                        @case(10)
                                           Oct
                                            @break
                                        @case(11)
                                            Nov
                                            @break 
                                        
                                        @case(12)
                                           Dec
                                           @break



                                    @endswitch
                                    
                                    </span>
                                </div>
                                <div class="details-container">
                                    <div class="meta-tags">
                                        <ul>
                                            <li><i class="fas fa-comments"></i> 33 Comments</li>
                                            <li><i class="fas fa-share"></i> 50 Shares</li>
                                        </ul>
                                    </div>
                                    <div class="post-body" style="color:#000">
                                        {{-- chú ý ảnh chọn là maxwith : 600px --}}
                                       {!!$blogDetails->content!!}
                                        <div class="post-bottom-content">
                                            <div class="bottom-content">
                                                <div class="right-content">
                                                    <ul>
                                                        <li class="share-fas"><i class="fas fa-share"></i></li>
                                                        <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                                                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                                                        <li><a href="#"><i class="fab fa-typo3"></i></a></li>
                                                        <li><a href="#"><i class="fab fa-staylinked"></i></a></li>
                                                        <li><a href="#"><i class="fab fa-tumblr"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="single-post-separator"></div>
                                    <div class="comments-area">
                                        
                                    </div>
                                    <div class="single-blog-page-separator"></div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <aside class="sidebar">
                                <div class="widget-area social">
                                    <div class="widget-title">
                                        <h4 class="h4-font-weight">Follow Us</h4>
                                    </div>
                                    <div class="widget-body">
                                        <ul class="social-links">
                                            <li>
                                                <a href="#" class="twitter">
                                                <i class="fab fa-twitter"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="facebook">
                                                <i class="fab fa-facebook-f"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="pinterest">
                                                <i class="fab fa-pinterest-p"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="instagram">
                                                <i class="fab fa-instagram"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="wordpress">
                                                <i class="fab fa-wordpress"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="sidebar-separator"></div>
                                <div class="widget-area category">
                                    <div class="widget-title">
                                        <h4>Danh Mục</h4>
                                    </div>
                                    <div class="widget-body">
                                        <ul class="categories">
                                            <li>
                                                <a href="#">Cẩm nang du lịch
                                                <span class="count"></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">Kinh nghiệm du lịch
                                                <span class="count"></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">Thông tin du lịch
                                                <span class="count"></span>
                                                </a>
                                            </li>
                                        
                                        </ul>
                                    </div>
                                </div>
                                <div class="sidebar-separator category"></div>
                                {{-- <div class="widget-area latest-post">
                                    <div class="widget-title">
                                        <h4>Tin tức cũ hơn</h4>
                                    </div>
                                    <div class="widget-body">
                                        <div class="single-latest-post">
                                            <div class="media">
                                                <img class="mr-3" src="images/01.jpg" alt="latest blog post image">
                                                <div class="media-body">
                                                    <a href="#">
                                                        <h5 class="mt-0" title="Du lịch Mũi Né khám phá làng chài bình yên">Du lịch Mũi Né khám phá làng chài bình yên</h5>
                                                    </a>
                                                    <span class="meta-time">
                                                    <i class="far fa-clock"></i> 6h trước</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="single-latest-post">
                                            <div class="media">
                                                <img class="mr-3" src="images/02.jpg" alt="latest blog post image">
                                                <div class="media-body">
                                                    <a href="#">
                                                        <h5 class="mt-0">Băng qua sườn đồi Mustolanmaki dẫn tới thiên đường mùa đông</h5>
                                                    </a>
                                                    <span class="meta-time">
                                                    <i class="far fa-clock"></i> 7h trước</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="single-latest-post">
                                            <div class="media">
                                                <img class="mr-3" src="images/03.jpg" alt="latest blog post image">
                                                <div class="media-body">
                                                    <a href="#">
                                                        <h5 class="mt-0">Du lịch Mũi Né khám phá làng chài bình yên</h5>
                                                    </a>
                                                    <span class="meta-time">
                                                    <i class="far fa-clock"></i> 9h trước</span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div> --}}
                                <div class="sidebar-separator latest-post"></div>
                            </aside>
                        </div>
                    </div>
                </div>
            </section>
@endsection