@extends('frontend.master.master')
@section('name','Tin tức')
@section('content')
	<div class="n3-breadcrumb">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 ">
                            <div class="breadcrumb">
                                <span itemscope="" itemtype="">
                                    <a href="" itemprop="url">
                                        <span itemprop="title" style="color: #ff002d;">Du lịch</span>
                                    </a>
                                </span>
                                 » 
                                 <span itemscope="" itemtype="">
                                    <a href="" itemprop="url">
                                        <span itemprop="title">Tin tức</span>
                                    </a>
                                </span>
                                 
                                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section class="blog-details-content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            @foreach ($blogDetails as $element)
                                <div class="row mg-bot30">
                                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 mg-bot">
                                        <a href="/tin-tuc/tin-tuc-details/{{str_slug($element->name)}}-{{$element->id}}.html" title=""><img src="{{$element->bai_viet_img}}" class="img-responsive pic-news-l" alt="{{$element->name}}"></a>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                                        <div class="frame-news">
                                            <div class="frame-top">
                                                <h2 class="news-title-l">
                                                    <a href="/tin-tuc/tin-tuc-details/{{str_slug($element->name)}}-{{$element->id}}.html" title="{{$element->name}}" class="dot-dot cut-name ddd-truncated" style="overflow-wrap: break-word;">{{$element->name}} </a>
                                                </h2>
                                                <div class="frame-date">
                                                    <div class="f-left"><img src="images/i-date.png" alt="date"></div>
                                                    @php
                                                        $time=Carbon\Carbon::parse($element->updated_at);
                                                    @endphp
                                                    <div class="f-left date">{{date('H:i d-m-Y', strtotime($time))}}</div>
                                                    <div class="clear" style="clear: both;"></div>
                                                </div>
                                            </div>
                                            <div class="frame-bot">
                                                <div class="des-content dot-dot cut-content post-summary" style="overflow-wrap: break-word;">
                                                   {!!$element->content!!}
                                                </div>
                                                <div class="text-right">
                                                    <a href="/tin-tuc/tin-tuc-details/{{str_slug($element->name)}}-{{$element->id}}.html" class="view_more" title="">Xem thêm &nbsp;<i class="fas fa-long-arrow-alt-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            
                        
                        </div>
                        <div class="col-lg-4">
                            <aside class="sidebar">
                                <div class="widget-area social">
                                    <div class="widget-title">
                                        <h4 class="h4-font-weight">Follow Us</h4>
                                    </div>
                                    <div class="widget-body">
                                        <ul class="social-links">
                                            <li>
                                                <a href="#" class="twitter">
                                                <i class="fab fa-twitter"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="facebook">
                                                <i class="fab fa-facebook-f"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="pinterest">
                                                <i class="fab fa-pinterest-p"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="instagram">
                                                <i class="fab fa-instagram"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#" class="wordpress">
                                                <i class="fab fa-wordpress"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="sidebar-separator"></div>
                                <div class="widget-area category">
                                    <div class="widget-title">
                                        <h4>Danh Mục</h4>
                                    </div>
                                    <div class="widget-body">
                                        <ul class="categories">
                                            @foreach ($blog as $element)
                                                <li>
                                                    <a href="">{{$element->name}}
                                                    <span class="count"></span>
                                                    </a>
                                                </li>
                                            @endforeach
                                            
                                           
                                        
                                        </ul>
                                    </div>
                                </div>
                                <div class="sidebar-separator category"></div>
                                {{-- <div class="widget-area latest-post">
                                    <div class="widget-title">
                                        <h4>Tin tức cũ hơn</h4>
                                    </div>
                                    <div class="widget-body">
                                        <div class="single-latest-post">
                                            <div class="media">
                                                <img class="mr-3" src="images/01.jpg" alt="latest blog post image">
                                                <div class="media-body">
                                                    <a href="#">
                                                        <h5 class="mt-0" title="Du lịch Mũi Né khám phá làng chài bình yên">Du lịch Mũi Né khám phá làng chài bình yên</h5>
                                                    </a>
                                                    <span class="meta-time">
                                                    <i class="far fa-clock"></i> 6h trước</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="single-latest-post">
                                            <div class="media">
                                                <img class="mr-3" src="images/02.jpg" alt="latest blog post image">
                                                <div class="media-body">
                                                    <a href="#">
                                                        <h5 class="mt-0">Băng qua sườn đồi Mustolanmaki dẫn tới thiên đường mùa đông</h5>
                                                    </a>
                                                    <span class="meta-time">
                                                    <i class="far fa-clock"></i> 7h trước</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="single-latest-post">
                                            <div class="media">
                                                <img class="mr-3" src="images/03.jpg" alt="latest blog post image">
                                                <div class="media-body">
                                                    <a href="#">
                                                        <h5 class="mt-0">Du lịch Mũi Né khám phá làng chài bình yên</h5>
                                                    </a>
                                                    <span class="meta-time">
                                                    <i class="far fa-clock"></i> 9h trước</span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div> --}}
                                <div class="sidebar-separator latest-post"></div>
                            </aside>
                        </div>
                    </div>
                </div>
            </section>
@endsection