@extends('frontend.master.master')
@section('name','Đăng nhập')
@section('content')
  
    <div class="login-page-content-area">
    <div class="container">

        <div class="row">
            <div class="col-lg-12 col-md-12">
               
                
                    @if(session('thongbao'))
                        <div class="alert alert-success" role="alert">
                           vui lòng kiểm tra email để xác nhận tài khoản

                        </div>
                    @endif
                    @if(session('tb'))
                        <div class="alert alert-danger" role="alert">
                           Tài khoản chưa xác thực
                           <br>
                            <a href="">Xác Thực lại</a>
                        </div>
                    @endif
                    @if (get_data_user('web','active') == 1)
                        <div class="alert alert-success" role="alert">
                           Tài khoản của bạn chưa xác thực? 
                        </div>

                    @endif
                    @if(session('success'))
                        <div class="alert alert-success" role="alert">
                           Xac Nhan thanh cong
                        </div>
                    @endif

              
                <div class="login-page-wrapper"><!-- login page wrapper -->
                    <div class="or hidden-xs hidden-sm">
                        <span>or</span>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                            <div class="left-content-area">
                                <div class="top-content">
                                    <h4 class="title">Chào mừng quay trở lại</h4>
                                </div>
                                <div class="bottom-content">
                                    <div class="left-content">
                                        <div class="thumb">
                                            <img src="asset/image/login-image.jpg" alt="login image">
                                        </div>
                                    </div>
                                    <div class="right-content">
                                        <ul>
                                            <li class="active">
                                                <a href="#">Đăng nhập lại</a>
                                            </li>
                                            <li>
                                                <a href="#">Xóa tài Khoản</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg- col-md-6 col-xs-12 col-sm-12">
                            <div class="right-contnet-area">
                                <div class="top-content">
                                    <h4 class="title">Đăng nhập tài khoản</h4>
                                </div>
                                <div class="bottom-content">
                                    <form action="/login" method="POST" class="login-form">
                                      @csrf
                                        <div class="form-element">
                                            <input type="email" name="email" class="input-field" placeholder="Nhập tên người dùng hoặc email">
                                             {{showErrors($errors,'email')}}
                                        </div>
                                        <div class="form-element">
                                            <input type="password" name="password" class="input-field" placeholder="Nhập mật khẩu">
                                             {{showErrors($errors,'password')}}
                                        </div>
                                        <div class="checkbox">
                                          <label>

                                            <input name="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}>{{ __('Remember Me') }}

                                          </label>
                                          <a href="/admin/forgotPassword" style="color: red; float: right;">Quên mật khẩu ?</a>
                                          <a href="/xac-thuc-lai" style="color: black; margin-left: 5px;">Xác Thực lại</a>
                                        </div>
                                        <div class="btn-wrapper">
                                            <button type="submit" class="submit-btn-login">Đăng nhập</button>
                                            <button type="" style="height: 50px;" class="submit-btn-login submit-btn-sign-up sub-mit">Đăng kí</button>
                                            
                                            
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
  <div class="body-overlay-sign-up">
    <div class="x-hidden"><i class="fas fa-times"></i></div>
     <div class="search-popup home-3 sign-up-form" id="search-popup">
      <form action="/register" class="sign-up-element search-popup-form" method="POST">
        @csrf
        <div class="all-sign-up">
           <div class="form-element">
            <h2>Sign Up</h2>
          </div>
          <div class="form-element">
            <input type="text" name="email_user" class="input-field input-sign-up" placeholder="Email.....">
            {{showErrors($errors,'email_user')}}
          </div>
          <div class="form-element">
            <input type="text" name="name"  class="input-field input-sign-up" placeholder="Tên người dùng.....">
          </div>
          <div class="form-element">
            <input type="text" name="phone_number"  class="input-field input-sign-up" placeholder="SDT.....">
          </div>
          <div class="form-element">
            <input type="password" name="password_user" class="input-field input-sign-up" placeholder="Mật Khẩu">
            {{showErrors($errors,'password_user')}}
          </div>
          <div class="form-element">
            <input type="password" name="password_confirm" class="input-field input-sign-up" placeholder="Xác nhận mật khẩu.....">
            {{showErrors($errors,'password_confirm')}}
          </div>
          <div class="center-button">
            <button class="btn btn-default acc_btn" type="submit" id="acc_Create"> 
              <span> <i class="fa fa-user btn_icon"></i>Tạo một tài khoản </span> 
           </button>
          </div>

        </div>
         
      </form>
    </div>
  </div>
    

@endsection