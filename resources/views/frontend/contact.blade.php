@extends('frontend.master.master')
@section('name','Liên hệ')
@section('content')

    <div class="main-body">
            <div class="video-area" id="video-area">
                <div class="player" id="video-play" data-property="{videoURL:'https://www.youtube.com/watch?v=r1wZf9do-_Q', containment:'#video-area', showControls:false, autoPlay:true, zoom:0, loop:true, mute:true, startAt:1, opacity:1, quality:'low',}"></div>
                <div class="video-table">
                    <div class="video-table-cell">
                        <div class="container center-holder">
                            <div class="video-effect">
                                <div class="video-effect-box">
                                    <div class="video-effect-content">
                                        <h2 data-animate-scroll='{"x": "-20","y": "-20","rotation": "-3","alpha": "0", "duration": ".8"}'>Hãy để chúng tôi đưa bạn đi</h2>
                                        <div class="home_search">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="home_search_container">
                                                            <div class="home_search_title">Tìm kiếm chuyến đi</div>
                                                            <div class="home_search_content">
                                                                <form action="/search" class="home_search_form" method="get" id="home_search_form">
                                                                    <div class="d-flex flex-lg-row flex-column align-items-start justify-content-lg-between justify-content-start">
                                                                        <input type="text" class="search_input search_input_1" placeholder="vd : Đà lạt" name="thanh_pho" required="required">
                                                                        
                                                                        <input type="text" class="search_input search_input_4" placeholder="Chi phí tối thiểu : 1000000 " name="min" required="required">
                                                                        <input type="text" class="search_input search_input_4" placeholder="Chi phí tối đa 1000000000000" name="max" required="required">
                                                                        <button class="home_search_button">Tìm kiếm</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="n3-breadcrumb">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 ">
                            <div class="breadcrumb">
                                <span itemscope="" itemtype="">
                                    <a href="" itemprop="url">
                                        <span itemprop="title" style="color: #ff002d;">Trang Chủ</span>
                                    </a>
                                </span>
                                 » 
                                 <span itemscope="" itemtype="">
                                    <a href="" itemprop="url">
                                        <span itemprop="title">Liên hệ</span>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="section-block section-div-block">
                <div class="container">
                    <div class="row">
                        <div class="contact-box">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="contact-box-info">
                                    <div class="contact-box-icon clearfix">
                                        <div class="contact-icon">
                                            <i class="fa fa fa-phone"></i>
                                        </div>
                                        <div class="contact-info">
                                            <h5>Số điện thoại</h5>
                                            <p>+84 987888888 </p>
                                           
                                        </div>
                                    </div>
                                    <div class="contact-box-icon clearfix">
                                        <div class="contact-icon">
                                            <i class="fa fa-map-marker"></i>
                                        </div>
                                        <div class="contact-info">
                                            <h5>Địa chỉ</h5>
                                            <p>Nhà C9, Đại học Bách Khoa Hà Nội, Hà Nội, Việt Nam</p>
                                            
                                        </div>
                                    </div>
                                    <div class="contact-box-icon clearfix">
                                        <div class="contact-icon">
                                            <i class="fa fa-envelope"></i>
                                        </div>
                                        <div class="contact-info">
                                            <h5>Email</h5>
                                            <p>datn20191@gmail.com</p>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-8 col-xs-12">
                                <div class="row contact-box-message">
                                    <form class="contact-form" method="post" autocomplete="off">
                                        <div class="col-xs-12">
                                            <input name="name" placeholder="Tên của bạn là">
                                        </div>
                                        <div class="col-xs-6">
                                            <input name="email" placeholder="E-mail" type="email">
                                        </div>
                                        <div class="col-xs-6">
                                            <input name="subject" placeholder="Chủ đề">
                                        </div>
                                        <div class="col-xs-12">
                                            <textarea name="message" placeholder="Nội dung tin nhắn"></textarea>
                                        </div>
                                        <div class="col-xs-12">
                                            <button type="submit" class="primary-button button-md-contact full-width rounded-border center-holder mt-20">Gửi tin nhắn
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-10">
                    <div class="max-map" style="width: 100%!important; margin-bottom: 30px;">
                        <iframe style="width: 100%;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.6717079534023!2d105.84016491422481!3d21.00579288601084!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac77a240d211%3A0x6b47998bc097b3ef!2zbmjDoCBDOSDEkeG6oWkgaOG7jWMgQsOhY2ggS2hvYSBIw6AgTuG7mWk!5e0!3m2!1svi!2s!4v1543372007630" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
                <div class="col-lg-1"></div>
            </div>
        </div> 

@endsection