@extends('frontend.master.master')
@section('name','Tour')
@section('content')
	 <div class="n3-breadcrumb">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 ">
                            <div class="breadcrumb">
                                <span itemscope="" itemtype="">
                                    <a href="" itemprop="url">
                                        <span itemprop="title" style="color: #ff002d;">Du lịch</span>
                                    </a>
                                </span>
                                 » 
                                 <span itemscope="" itemtype="">
                                    <a href="" itemprop="url">
                                        <span itemprop="title">Thanh Toán</span>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="container n3-book-tour-1 mg-bot40">
            <div class="row">
                <div class="col-xs-12 mg-bot50">
                    <div class="step-by-step">
                        <ul class="stepper stepper-horizontal">
                            <li class="warnning">
                                <a>
                                <span class="circle">1</span>
                                <span class="label">Nhập thông tin</span>
                                </a>
                            </li>
                            <li class=" active">
                                <a>
                                <span class="circle">2</span>
                                <span class="label">FriendsTravel xác nhận</span>
                                </a>
                            </li>
                           {{--  <li class="warnning">
                                <a>
                                <span class="circle">3</span>
                                <span class="label">&nbsp;&nbsp;Thanh toán</span>
                                </a>
                            </li> --}}
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 mg-bot30">
                    <div class="mg-bot30" style="margin-left: 20px;">
						
						
							<table class="order-rable">
								<tbody>
									
									<tr style="margin-bottom:10px; height: 40px; font-weight: 600; font-size: 17px; color:#000;">
										<td>Ngày đăng kí</td>
										<td>:{{Carbon\carbon::parse($customer->created_at)->format('d-m-Y')}}</td>
									</tr>
									
									<tr style="margin-bottom:10px; height: 40px; font-weight: 600; font-size: 17px; color:#000;">
										<td>Phương thức thanh toán</td>
										<td>: Kí hợp đồng và nộp tiền tại công ty FriendTravels</td>
									</tr>
								</tbody>
							</table>
						
						
					</div>
                    
                </div>
                
                <div class="col-xs-12 mg-bot30">
                    <div class="title">THÔNG TIN TOUR</div>
                </div>
                @foreach ($inforCustomer as $element)
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <a href="" target="_blank" title=""><img src="{{$element->img}}" alt="{{$element->name}}" class="img-responsive pic-bt"></a>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            <div class="tour-name">
                                <a href="" target="_blank" title="" class="dot-dot cut-name ddd-truncated" style="overflow-wrap: break-word; display: block; overflow: hidden; white-space: nowrap; text-overflow: Ellipsis;">{{$element->name}}</a>
                            </div>
                            <div class="frame-info backgroud-none">
                                <div class="row">
                                    <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12 mg-bot10">
                                        <div class="f-left l"><img src="images/i-code.png" alt="code"></div>
                                        <div class="f-left r">{{$element->code}}</div>
                                        <div class="clear"></div>
                                    </div>
                                 
                                 
                                    <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
                                        <div class="f-left l"><img src="images/i-price.png" alt="price"></div>
                                        <div class="f-left r">Số lượng đăng kí: 
                                            <span class="font500">
                                            	{{$element->quantity}}
                                            </span>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                     <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
                                        <div class="f-left l"><img src="images/i-price.png" alt="price"></div>
                                        <div class="f-left r">Thành tiền: 
                                            <span class="font500">
                                            	{{number_format($element->price*$element->quantity,0,'.',',')}} đ
                                            </span>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                     
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="total-wrap">
                            <div class="row">
                                <div class="col-md-8">

                                </div>
                                <div class="col-md-3 col-md-push-1 text-center" style="width: 300px;">
                                    <div class="total">
                                       
                                        <div class="grand-total">
                                            <p><span><strong style="color:#000">Tổng cộng:</strong></span> <span style="color:#ff002d; font-size: 17px;">{{number_format($customer->total,0,'.',',')}} đ</span></p>
                                            
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                
                
                
               <div class="col-xs-12 mg-bot30">
                    <div class="title">Cảm ơn bạn đã tin tưởng chúng tôi !! Chúc bạn có trải nghiệm thú vị tại FriendTravels .</div>
                    <div style="text-align: center; margin-top: 10px">
                    	<i>Chúng tôi đã gửi mail đến cho bạn. Vui lòng check email và trong thời gian sớm nhất nhân viên bên FriendTravels sẽ liên lạc cho bạn</i>
                    </div>
                    
                </div>
            </div>
        </div>
        
        
@endsection
