@extends('frontend.master.master')
@section('name','Tour')
@section('content')
	 <div class="n3-breadcrumb">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 ">
                            <div class="breadcrumb">
                                <span itemscope="" itemtype="">
                                    <a href="" itemprop="url">
                                        <span itemprop="title" style="color: #ff002d;">Du lịch</span>
                                    </a>
                                </span>
                                 » 
                                 <span itemscope="" itemtype="">
                                    <a href="" itemprop="url">
                                        <span itemprop="title">Thanh Toán</span>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <div class="container n3-book-tour-1 mg-bot40">
            <div class="row">
                <div class="col-xs-12 mg-bot50">
                    <div class="step-by-step">
                        <ul class="stepper stepper-horizontal">
                            <li class="active">
                                <a>
                                <span class="circle">1</span>
                                <span class="label">Nhập thông tin</span>
                                </a>
                            </li>
                            <li class="warnning">
                                <a>
                                <span class="circle">2</span>
                                <span class="label">FriendsTravel xác nhận</span>
                                </a>
                            </li>
                           {{--  <li class="warnning">
                                <a>
                                <span class="circle">3</span>
                                <span class="label">&nbsp;&nbsp;Thanh toán</span>
                                </a>
                            </li> --}}
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 mg-bot30">
                    <div class="title">THÔNG TIN TOUR</div>
                </div>
                @foreach ($cart as $element)
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <a href="" target="_blank" title=""><img src="{{$element->options->img}}" alt="{{$element->name}}" class="img-responsive pic-bt"></a>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            <div class="tour-name">
                                <a href="" target="_blank" title="" class="dot-dot cut-name ddd-truncated" style="overflow-wrap: break-word; display: block; overflow: hidden; white-space: nowrap; text-overflow: Ellipsis;">{{$element->name}}</a>
                            </div>
                            <div class="frame-info backgroud-none">
                                <div class="row">
                                    <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12 mg-bot10">
                                        <div class="f-left l"><img src="images/i-code.png" alt="code"></div>
                                        <div class="f-left r">{{$element->id}}</div>
                                        <div class="clear"></div>
                                    </div>
                                   
                                    <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12 mg-bot10">
                                        <div class="f-left l"><img src="images/i-date.png" alt="date"></div>
                                        <div class="f-left r">Ngày khởi hành: <span class="font500">{{$element->options->ngay_khoi_hanh}}</span></div>
                                        <div class="clear"></div>
                                    </div>
                                 
                                    <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12 mg-bot10">
                                        <div class="f-left l"><img src="images/i-clock.png" alt="clock"></div>
                                        <div class="f-left r">Số ngày: <span class="font500">{{$element->options->so_ngay}}</span></div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
                                        <div class="f-left l"><img src="images/i-price.png" alt="price"></div>
                                        <div class="f-left r">Giá: <span class="font500 price">{{number_format($element->price,0,'',',')}} đ</span></div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
                                        <div class="f-left l"><img src="images/i-price.png" alt="price"></div>
                                        <div class="f-left r">Số lượng đăng kí: 
                                            <span class="font500"><input onchange="updateItem('{{$element->rowId}}',this.value)" type="number" id="quantity" name="quantity"
                                        class="form-control input-number text-center" style="width: 70px; display: inherit!important;" value="{{$element->qty}}">
                                            </span>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                     <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12">
                                        
                                        <a onclick="delItem('{{$element->rowId}}','{{$element->name}}')" class="closed" style="padding-left: 10px; font-size: 30px; color: #ff002d; text-shadow: 1px 1px 2px #ffba8a; "><i class="fas fa-times"></i></a>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="total-wrap">
                            <div class="row">
                                <div class="col-md-8">

                                </div>
                                <div class="col-md-3 col-md-push-1 text-center" style="width: 300px;">
                                    <div class="total">
                                       
                                        <div class="grand-total">
                                            <p><span><strong style="color:#000">Tổng cộng:</strong></span> <span style="color:#ff002d; font-size: 17px;">{{$total}} đ</span></p>
                                            
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                
                <div class="col-xs-12 mg-bot30">
                    <div class="chuy">
                        Khách nữ từ 55 tuổi trở lên, khách nam từ 60 tuổi trở lên đi tour một mình và khách mang thai trên 4 tháng (16 tuần) vui lòng đăng ký tour trực tiếp tại văn phòng của FriendTravels. Không áp dụng đăng ký tour online đối với khách từ 70 tuổi trở lên
                    </div>
                </div>
                
                <form action="/cart/checkout" method="post">
                    @csrf
                       
                    <div class="col-xs-12 mg-bot30">
                        <div class="title">THÔNG TIN LIÊN LẠC</div>
                    </div>
                    <div class="col-xs-12 form-input mg-bot30 color-chu-infor-customer">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Họ tên (<span class="star">*</span>)</label>
                                    <div>
                                        <input class="form-control" id="contact_name" name="full" required="required" type="text" value="{{old('full')}}">
                                         {{ showErrors($errors,'full') }}
                                    </div>

                                </div>
                                
                                <div class="form-group">
                                    <label>Địa chỉ</label>
                                    <div>
                                        <input class="form-control" id="address" name="address" type="text" value="{{old('address')}}">
                                         {{ showErrors($errors,'address') }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label>Email (<span class="star">*</span>)</label>
                                    <div>
                                        <input class="form-control" id="email" name="email" required="required" type="email" value="{{old('email')}}">
                                         {{ showErrors($errors,'email') }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Điện thoại</label>
                                    <div>
                                        <input class="form-control" id="phone" name="phone"  type="text" value="{{old('phone')}}">
                                         {{ showErrors($errors,'phone') }}
                                    </div>
                                </div>

                            </div>
                           

                        </div>
                    </div>
            
                    <div class="col-xs-12 mg-bot30">
                        <div class="title">ĐIỀU KHOẢN BẮT BUỘC KHI ĐĂNG KÝ ONLINE</div>
                    </div>
                    <div class="col-xs-12 mg-bot20">
                        <div class="frame-rule">
                            <div class="rule-content">
                                <title></title>
                                <p>
                                    <strong>ĐIỀU KIỆN BÁN VÉ CÁC CHƯƠNG TRÌNH DU LỊCH TRONG NƯỚC</strong>
                                </p>
                                <p>
                                    <strong>I.&nbsp;&nbsp; GIÁ VÉ DU LỊCH</strong>
                                </p>
                                <p>
                                    Giá vé du lịch được tính theo tiền Đồng (Việt Nam - VNĐ). Trường hợp khách thanh toán bằng USD sẽ được quy đổi ra VNĐ theo tỉ giá của ngân hàng Đầu Tư và Phát Triển Việt Nam - Chi nhánh TP.HCM tại thời điểm thanh toán.
                                </p>
                                <p>
                                    Giá vé chỉ bao gồm những khoản được liệt kê một cách rõ ràng trong phần “Bao gồm” trong các chương trình du lịch. FriendTravels không có nghĩa vụ thanh toán bất cứ chi phí nào không nằm trong phần “Bao gồm”.
                                </p>
                                <p>
                                    <strong>II.&nbsp;&nbsp; GIÁ DÀNH CHO TRẺ EM</strong>
                                </p>
                                <p>
                                    - Trẻ em dưới 5 tuổi:&nbsp; không thu phí dịch vụ, bố mẹ tự lo cho bé và thanh toán các chi phí phát sinh (đối với các dịch vụ tính phí theo chiều cao…). Hai người lớn chỉ được kèm 1 trẻ em dưới 5 tuổi, trẻ em thứ 2 sẽ đóng phí theo qui định dành cho độ tuổi từ 5 đến dưới 12 tuổi và phụ thu phòng đơn. Vé máy bay, tàu hỏa, phương tiện vận chuyển công cộng mua vé theo qui định của các đơn vị vận chuyển (nếu có)
                                </p>
                                <p>
                                    - Trẻ em từ 5 tuổi đến dưới 12 tuổi:&nbsp; 50% giá tour người lớn đối với tuyến xe, 75% giá tour người lớn đối với tuyến có vé máy bay (không có chế độ giường riêng). Hai người lớn chỉ được kèm 1 trẻ em từ 5 - dưới 12 tuổi, em thứ hai trở lên phải mua 1 suất giường đơn.
                                </p>
                                <p>
                                    - Trẻ em từ 12 tuổi trở lên: mua một vé như người lớn.
                                </p>
                                <p>
                                    <strong>III. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; THANH TOÁN</strong>
                                </p>
                                <p>
                                    Khi thanh toán, Quý khách vui lòng cung cấp đầy đủ thông tin và đặt cọc ít nhất 50% tổng số tiền tour để giữ chỗ, số tiền còn lại Quý khách sẽ thanh toán trước ngày khởi hành 05 ngày làm việc (tour ngày thường) và trước 10 ngày làm việc (tour dịp Lễ, Tết)”.
                                </p>
                                <p>
                                    Thanh toán bằng tiền mặt hoặc chuyển khoản tới tài khoản ngân hàng của FriendTravels.
                                </p>
                                <p>
                                    Tên tài khoản: VIETRAVEL
                                </p>
                                <p>
                                    Ngân hàng:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ngoại Thương Việt Nam – Chi nhánh TPHCM
                                </p>
                                <p>
                                    Địa chỉ:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 29 Bến Chương Dương, Quận 1, TPHCM
                                </p>
                                <p>
                                    Tài khoản:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; USD: 007 137 008 9095
                                </p>
                                <p style="margin-left:.5in;">
                                    VNĐ: 007 100 001 2584
                                </p>
                                <p>
                                    Việc thanh toán được xem là hoàn tất khi FriendTravels nhận được đủ tiền vé du lịch trước lúc khởi hành hoặc theo hợp đồng thỏa thuận giữa hai bên. Bất kỳ mọi sự thanh toán chậm trễ dẫn đến việc hủy dịch vụ không thuộc trách nhiệm của FriendTravels.
                                </p>
                                <p>
                                    Khách hàng có nhu cầu xuất hóa đơn, vui lòng cung cấp thông tin xuất hóa đơn ngay tại thời điểm đăng ký. FriendTravels có trách nhiệm xuất hóa đơn cho khách hàng trong vòng 7 ngày sau khi tour kết thúc.
                                </p>
                                <p>
                                    <strong>DÀNH CHO KHÁCH HÀNG ĐĂNG KÝ TRÊN TRANG WWW.TRAVEL.COM.VN THANH TOÁN CHUYỂN KHOẢN:</strong>
                                </p>
                                <p>
                                    Khi thực hiện việc chuyển khoản, Quý khách <strong>vui lòng ghi rõ Tên họ, Số điện thoại và Nội dung chuyển</strong> cho chương trình du lịch cụ thể đã được Quý khách chọn đăng ký. Sau khi thực hiện việc chuyển khoản, Quý khách vui lòng gửi Ủy Nhiệm Chi về công ty FriendTravels theo địa chỉ email sales@vietravel.com và liên hệ với nhân viên phụ trách tuyến để nhận được Vé du lịch chính thức từ công ty FriendTravels. FriendTravels sẽ không giải quyết các trường hợp hệ thống tự động hủy phiếu đăng ký nếu Quý khách không thực hiện đúng qui định trên.
                                </p>
                                <p>
                                    <strong>IV.&nbsp;&nbsp; HỦY VÉ VÀ PHÍ HỦY VÉ DU LỊCH</strong>
                                </p>
                                <p>
                                    <strong>1.&nbsp;&nbsp; Trường hợp bị hủy bỏ do FriendTravels:</strong>
                                </p>
                                <p>
                                    Nếu FriendTravels không thực hiện được chuyến du lịch, FriendTravels phải báo ngay cho khách hàng biết và thanh toán lại cho khách hàng toàn bộ số tiền khách hàng đã đóng trong vòng 3 ngày kể từ lúc việc thông báo hủy chuyến du lịch bằng tiền mặt hoặc chuyển khoản.
                                </p>
                                <p>
                                    <strong>2.&nbsp;&nbsp; Trường hợp bị hủy bỏ do khách hàng:</strong>
                                </p>
                                <p>
                                    Sau khi đóng tiền, nếu Quý khách muốn chuyển/hủy tour xin vui lòng mang Vé Du Lịch đến văn phòng đăng ký tour để làm thủ tục chuyển/hủy tour và chịu mất phí theo quy định của FriendTravels. Không giải quyết các trường hợp liên hệ chuyển/hủy tour qua điện thoại.
                                </p>
                                <p>
                                    <strong>•&nbsp;&nbsp;&nbsp;&nbsp; Đối với ngày thường:</strong>
                                </p>
                                <p>
                                    -&nbsp;&nbsp; Được chuyển sang các tuyến du lịch khác trước ngày khởi&nbsp; hành 20 ngày : Không mất chi phí.
                                </p>
                                <p>
                                    -&nbsp;&nbsp; Hủy hoặc chuyển sang các chuyến du lịch khác ngay sau khi đăng ký đến từ 15 - 19 ngày trước ngày khởi hành: Chi phí chuyển/hủy tour: 50% tiền cọc tour.
                                </p>
                                <p>
                                    <strong>•&nbsp;&nbsp;&nbsp;&nbsp; Đối với ngày lễ, Tết:&nbsp; </strong>
                                </p>
                                <p>
                                    -&nbsp;&nbsp; Được chuyển sang các tuyến du lịch khác trước ngày khởi &nbsp;hành 30 ngày : Không mất chi phí.
                                </p>
                                <p>
                                    -&nbsp;&nbsp; Hủy hoặc chuyển sang các chuyến du lịch khác ngay sau khi đăng ký đến từ 25 - 29 ngày trước ngày khởi hành: Chi phí chuyển/hủy tour: 50% tiền cọc tour.
                                </p>
                                <p>
                                    <strong><em>* Các tour ngày Lễ, Tết là các tour có thời gian diễn ra rơi vào một trong các ngày lễ, tết theo qui định.</em></strong>
                                </p>
                                <p>
                                    <strong><em>* Thời gian hủy tour được tính cho ngày làm việc, không tính thứ 7, Chủ Nhật và các ngày Lễ, Tết.</em></strong>
                                </p>
                                <p>
                                    <strong>DÀNH CHO KHÁCH HÀNG ĐĂNG KÝ TRÊN TRANG WWW.TRAVEL.COM.VN THANH TOÁN TRỰC TUYẾN:</strong>
                                </p>
                                <p>
                                    Khách hàng hủy Vé du lịch trong thời điểm ngày Thường và Lễ Tết theo đúng những qui định trên, trong trường hợp khách thanh toán trực tuyến, nếu hủy Vé du lịch khách hàng sẽ chịu toàn bộ phí ngân hàng cho việc thanh toán tiền Vé du lịch. Việc hoàn trả tiền cho khách sẽ được FriendTravels thực hiện ngay sau khi ngân hàng thông báo tiền đã vào tài khoản của FriendTravels.
                                </p>
                                <p>
                                    <strong>3.&nbsp;&nbsp; Trường hợp bất khả kháng:</strong>
                                </p>
                                <p>
                                    Nếu chương trình du lịch bị hủy bỏ hoặc thay đổi bởi một trong hai bên vì một lý do bất khả kháng (hỏa hoạn, thời tiết, tai nạn, thiên tai, chiến tranh, dịch bệnh, hoãn, dời, hủy chuyến hoặc thay đổi khác của các phương tiện vận chuyển công cộng hoặc các sự kiến bất khả kháng khác theo quy định pháp luật…), thì hai bên sẽ không chịu bất kỳ nghĩa vụ bồi hoàn các tổn thất đã xảy ra và không chịu bất kỳ trách nhiệm pháp lý nào. Tuy nhiên mỗi bên có trách nhiệm cố gắng tối đa để giúp đỡ bên bị thiệt hại nhằm giảm thiểu các tổn thất gây ra vì lý do bất khả kháng.
                                </p>
                                <p>
                                    <strong>4. &nbsp;&nbsp;&nbsp;Thay đổi lộ trình:&nbsp; </strong>
                                </p>
                                <p>
                                    Tùy theo tình hình thực tế, FriendTravels giữ quyền thay đổi lộ trình, sắp xếp lại thứ tự các điểm tham quan hoặc hủy bỏ chuyến đi du lịch bất cứ lúc nào mà FriendTravels thấy cần thiết vì sự thuận tiện hoặc an toàn cho khách hàng.
                                </p>
                                <p>
                                    <strong>V.&nbsp;&nbsp; NHỮNG YÊU CẦU ĐẶC BIỆT TRONG CHUYẾN DU LỊCH</strong>
                                </p>
                                <p>
                                    Các yêu cầu đặc biệt của Quý khách phải được báo trước cho FriendTravels ngay tại thời điểm đăng ký. FriendTravels sẽ cố gắng đáp ứng những yêu cầu này trong khả năng của mình song sẽ không chịu trách nhiệm về bất kỳ sự từ chối cung cấp dịch vụ từ phía các nhà vận chuyển, khách sạn, nhà hàng và các nhà cung cấp dịch vụ độc lập khác.
                                </p>
                                <p>
                                    <strong>VI.&nbsp;&nbsp; KHÁCH SẠN</strong>
                                </p>
                                <p>
                                    Khách sạn được cung cấp trên cơ sở những phòng có hai giường đơn (TWN) hoặc một giường đôi (DBL) tùy theo cơ cấu phòng của các khách sạn. Khách sạn do FriendTravels đặt cho các chương trình tham quan có tiêu chuẩn tương ứng với các mức giá vé mà khách chọn khi đăng ký đi du lịch. Nếu cần thiết thay đổi về bất kỳ lý do nào, khách sạn thay thế sẽ tương đương với tiêu chuẩn của khách sạn ban đầu và sẽ được báo cho du khách trước khi khởi hành. Những yêu cầu đặc biệt của khách hàng nếu thông báo trước cho FriendTravels sẽ được đáp ứng tùy theo khả năng cung cấp của khách sạn và khách hàng phải trả thêm tiền đối với các yêu cầu này (nếu có). FriendTravels có quyền không đáp ứng những yêu cầu này nếu khách sạn từ chối cung cấp dịch vụ. Thời gian nhận phòng theo qui định tại các khách sạn là sau 14:00 và phải trả phòng trước 12:00. Đối với các trường hợp khách lưu trú tại hệ thống khách sạn/Resort 5 sao (Vinpearl, FLC, Grand Ho Tram Strip…) tuân thủ theo điều kiện hủy phạt của khách sạn/Resort cho từng thời điểm.
                                </p>
                                <p>
                                    <strong>VII.&nbsp;&nbsp; VẬN CHUYỂN</strong>
                                </p>
                                <p>
                                    Phương tiện vận chuyển tùy thuộc theo từng chương trình du lịch.
                                </p>
                                <p>
                                    Với chương trình đi bằng xe: xe máy lạnh (4, 7, 15, 25, 35, 45 chỗ) sẽ được FriendTravels sắp xếp tùy theo số lượng khách từng đoàn, phục vụ suốt chương trình tham quan.
                                </p>
                                <p>
                                    Với chương trình đi bằng xe lửa - máy bay - tàu cánh ngầm (phương tiện vận chuyển công cộng), trong một số chương trình các nhà cung cấp dịch vụ có thể thay đổi giờ khởi hành mà không báo trước, việc thay đổi này sẽ được FriendTravels thông báo cho khách hàng nếu thời gian cho phép.
                                </p>
                                <p>
                                    FriendTravels không chịu trách nhiệm bồi hoàn và trách nhiệm pháp lý với những thiệt hại về vật chất lẫn tinh thần do việc chậm trễ, thay đổi giờ giấc khởi hành của các phương tiện vận chuyển công cộng hoặc sự chậm trễ do chính hành khách gây ra. FriendTravels chỉ thực hiện hành vi giúp đỡ để giảm bớt tổn thất cho hành khách.
                                </p>
                                <p>
                                    <strong>VIII.&nbsp;&nbsp; HÀNH LÝ</strong>
                                </p>
                                <p>
                                    Hành lý gọn nhẹ, với các chương trình sử dụng dịch vụ hàng không, hành lý miễn cước sẽ do các hãng hàng không qui định. FriendTravels không chịu trách nhiệm về sự thất lạc, hư hỏng hành lý hoặc bất kỳ vật dụng gì của du khách trong suốt chuyến đi, du khách tự bảo quản hành lý của mình. Nếu khách hàng bị mất hay thất lạc hành lý thì FriendTravels sẽ giúp hành khách liên lạc và khai báo với các bộ phận liên quan truy tìm hành lý bị mất hay thất lạc. Việc bồi thường hành lý bị mất hay thất lạc sẽ theo qui định của các đơn vị cung cấp dịch vụ hoặc các đơn vị bảo hiểm (nếu có).
                                </p>
                                <p>
                                    <strong>IX.&nbsp;&nbsp; BẢO HIỂM DU LỊCH</strong>
                                </p>
                                <p>
                                    Giá dịch vụ du lịch trọn gói đã bao gồm bảo hiểm du lịch trong nước với mức đền bù cao nhất là 120.000.000đ/khách Việt Nam/vụ cho nhân mạng và 12.000.000 VNĐ/khách Việt Nam/vụ cho hành lý .
                                </p>
                                <p>
                                    Điều kiện, điều khoản bảo hiểm: Theo quy tắc bảo hiểm khách du lịch trong nước QĐ số: 001321/2006 – BM/BHCN.
                                </p>
                                <p>
                                    Số hotline tư vấn về các điều kiện, điều khoản bảo hiểm 0938 30 1234
                                </p>
                                <p>
                                    <strong>X.&nbsp;&nbsp;&nbsp; </strong>Trong quá trình thực hiện, nếu xảy ra tranh chấp sẽ được giải quyết trên cơ sở thương lượng trong thời hạn 30 ngày kể từ ngày một trong hai bên đưa tranh chấp ra thương lượng. Hết thời hạn này nếu tranh chấp không được giải quyết hoặc một trong hai bên không đồng ý với kết quả thương lượng thì có quyền đưa tranh chấp ra giải quyết tại Tòa án thẩm quyền.
                                </p>
                                <p>
                                    <strong><em>* Vé du lịch được xem như Hợp đồng lữ hành và được lập thành 2 bản, mỗi bên giữ một bản, có giá trị như nhau.</em></strong>
                                </p>
                                <p>
                                    &nbsp;
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 mg-bot30">
                        <div>
                            <div class="f-left mg-bot10 wl">
                                <input type="checkbox" id="chkDieuKhoan" name="chkDieuKhoan">
                            </div>
                            <div class="f-left wr">
                                Tôi đồng ý với các điều kiện trên
                            </div>

                            <div class="clear"></div>

                        </div>
                        
                    </div>
                    @if(session('abc'))
                            <div class="alert bg-danger" role="alert" style="color:#000; font-size: 20px; font-weight: 600">
                                
                               Bạn chắc chắn rằng đã đồng ý với các điều khoản chưa
                            </div>
                        @endif
                    
                    <div class="col-xs-12 mg-bot30 text-center">
                        <button type="submit" class="btn btn-md btn-book">Đặt tour&nbsp;&nbsp;<i class="fas fa-arrow-right"></i></button>
                    </div>
                </form>
            </div>
        </div>
        
        
@endsection
@section('data-cart-update')

<script>
        function updateItem(rowId,qty)
        {
             $.ajax({
                    url : '/cart/update-item/'+rowId+'/'+qty, // gửi ajax đến file result.php
                    type : "get", // chọn phương thức gửi là post
                    
                    data : { // Danh sách các thuộc tính sẽ gửi đi
                        rowId:rowId,
                        qty:qty,
                    },
                    success : function (result){
                        // Sau khi gửi và kết quả trả về thành công thì gán nội dung trả về
                        // đó vào thẻ div có id = result
                        location.reload();
                    }
                });
            
        }
        function delItem(rowId,name)
        {
            if (confirm('Ban muon xoa tour:'+name+'trong gio')) {
                $.get('/cart/del-item/'+rowId,
                function(data){
                    // alert(data);
                    if (data ='success') 
                    {
                        location.reload();
                    }
                });
            }
            
        }


    </script>
@stop 