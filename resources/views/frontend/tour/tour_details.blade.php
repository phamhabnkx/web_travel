@extends('frontend.master.master')
@section('name','Tour chi tiết')
@section('content')
<style>

    .list_start{
        cursor: pointer;
    }
    .list_start i:hover{
        cursor: pointer;
    }
    .list_text{
        display: inline-block;
        margin-left: 10px;
        position: relative;
        background: #52b858;
        color: #fff;
        padding: 2px 8px;
        box-sizing: border-box;
        font-size: 12px;
        border-radius: 2px;
        display: none;
    }
    .list_text::after{
        right: 100%;
        top: 50%;
        border: solid transparent;
        content: " ";
        height: 0;
        width: 0;
        position: absolute;
        pointer-events: none;
        border-color: rgba(82,184,88,0);
        border-right-color: #52b858;
        border-width: 6px;
        margin-top: -6px;
    }
    .list_start .rating_active{
        color: #ff9705
    }
    
</style>
      <div class="n3-breadcrumb">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 ">
                            <div class="breadcrumb">
                                <span itemscope="" itemtype="">
                                    <a href="" itemprop="url">
                                        <span itemprop="title" style="color: #ff002d;">Du lịch</span>
                                    </a>
                                </span>
                                 » 
                                @if ($tour->loai_tour == 1)
                                    <span itemscope="" itemtype="">
                                        <a href="" itemprop="url">
                                            <span itemprop="title">
                                                Du lịch trong nước
                                            </span>
                                            
                                        </a>
                                    </span>
                                @else
                                    <span itemscope="" itemtype="">
                                        <a href="" itemprop="url">
                                            <span itemprop="title">
                                                Du lịch nước ngoài
                                            </span>
                                            
                                        </a>
                                    </span>
                                @endif
                                
                                
                                 
                                 » 
                                 <span itemscope="" itemtype="">
                                    <a href="" itemprop="url">
                                        <span itemprop="title">{{$tour->address->name}}: {{$tour->name}}
                                        </span>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container n3-tour-description mg-bot40" itemscope="" itemtype="http://data-vocabulary.org/Product">
                <div class="row">
                    <div class="col-xs-12 mg-bot15">
                        <h1 class="tour-name" itemprop="name" title="{{$tour->name}}">
                            <a>{{$tour->address->name}}: {{$tour->name}}</a>
                        </h1>
                        <span itemprop="brand" style="display: none;">FriendTravels</span>
                        <img itemprop="image" style="display: none;" src="images/i-code.png" alt="">
                        <div class="tour-code">
                            <i class="fas fa-barcode"></i>
                            {{$tour->code}}
                        </div>
                    </div>                    
                </div>


            </div>
            <div class="container" style="margin-bottom: 40px;" id="content_tour" data-id="{{$tour->id}}">
                <div class="row">

                        <div class="slideshow-pt col-lg-8 col-md-12 col-sm-12 col-xs-12 pos-relative">
                            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    
                                    @for($i=0; $i<count($img); $i++)
                                        <li data-target="#myCarousel" data-slide-to="{{$i}}" class=""></li>
                                    @endfor
                                    
                                </ol>
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <div class="item active">
                                            <img src="/upload_avatar/{{$img[0]}}" alt="T" class="img-responsive pic-ss-pt">
                                        </div>
                                    @for ($i = 1; $i < count($img); $i++)
                                       <div class="item">
                                            <img src="/upload_avatar/{{$img[$i]}}" alt="T" class="img-responsive pic-ss-pt">
                                        </div>
                                    @endfor
                                    
                                </div>
                                <!-- Left and right controls -->
                                <a class="left carousel-control hidden" href="#myCarousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control hidden" href="#myCarousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                                </a>
                            </div>

                        </div>
                        <div class="info col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <div class="frame-info pos-relative" style="height: 500px">
                                <div class="sec1">
                                    <div class="f-left frame-rating">
                                        <div style="float:left;margin:8px 0px" data-tourid="97687771-f9b7-4114-b28e-fa6ec180bd4b" class="rateit" data-rateit-value="4" data-rateit-resetable="false">
                                            <button id="rateit-reset-2" data-role="none" class="rateit-reset" aria-label="reset rating" aria-controls="rateit-range-2" style="display: none;"></button>
                                            <div id="rateit-range-2" class="rateit-range" tabindex="0" role="slider" aria-label="rating" aria-owns="rateit-reset-2" aria-valuemin="0" aria-valuemax="5" aria-valuenow="4" aria-readonly="false" style="width: 80px; height: 16px;">
                                                <div class="rateit-selected" style="height: 16px; width: 74.56px; display: block;"></div>
                                                <div class="rateit-hover" style="height: 16px; width: 0px; display: none;"></div>
                                            </div>
                                        </div>
                                        <div style="float: left; margin-top:8px" class="hidden-xs margin-danh-gia">
                                            <span class="hreview-aggregate">
                                            <span class="item">
                                            <span class="fn">
                                            <span class="rating">
                                                <span class="average"><strong>4</strong></span>
                                                <strong>/<span class="best">5 </span></strong>
                                                trong 
                                                <span class="votes"><strong>364</strong></span>
                                                 Đánh giá
                                                <span class="summary"></span></span>
                                            </span>
                                            </span>
                                            </span>
                                        </div>
                                    </div>
                                  
                                </div>
                                <div class="sec2">
                                    <div class="row mg-bot10">
                                        <div class="col-lg-4 col-md-2 col-sm-3 col-xs-6" style="color:#000;">Khởi hành:</div>
                                        <div class="col-lg-8 col-md-10 col-sm-9 col-xs-6">
                                            <div class="mg-bot-date" style="color:#000;">
                                                {{$tour->ngay_khoi_hanh}} 
                                                <span class="hidden-xs">
                                                <i class="far fa-calendar-alt"></i>
                                                <a href="" class="b">Ngày khác</a>
                                                </span>
                                            </div>
                                            <div class="hidden-lg hidden-md hidden-sm">
                                                <i class="far fa-calendar-alt" style="color:#000;"></i><a href="" class="b">Ngày khác</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mg-bot10">
                                        <div class="col-lg-4 col-md-2 col-sm-3 col-xs-6" style="color:#000;">Thời gian:</div>
                                        <div class="col-lg-8 col-md-10 col-sm-9 col-xs-6" style="color:#000;">
                                            {{$tour->so_ngay}}
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4 col-md-2 col-sm-3 col-xs-6" style="padding-right: 0px !important;color:#000;" >Nơi khởi hành:</div>
                                        <div class="col-lg-8 col-md-10 col-sm-9 col-xs-6" style="color:#000;">
                                             @if ($tour->DepartureId ==0)
                                                        
                                                    @endif
                                                    @if ($tour->DepartureId ==1)
                                                        Hồ Chí Minh
                                                    @endif
                                                    @if ($tour->DepartureId ==32)
                                                        Bạc Liêu
                                                    @endif
                                                    @if ($tour->DepartureId ==33)
                                                        Bảo Lộc
                                                    @endif
                                                    @if ($tour->DepartureId ==7)
                                                        Bình Dương
                                                    @endif
                                                    @if ($tour->DepartureId ==18)
                                                        Buôn Ma Thuột
                                                    @endif
                                                    @if ($tour->DepartureId ==20)
                                                        Cà Mau
                                                    @endif
                                                    @if ($tour->DepartureId ==5)
                                                        Cần Thơ
                                                    @endif
                                                    @if ($tour->DepartureId ==24)
                                                        Đà Lạt
                                                    @endif
                                                    @if ($tour->DepartureId ==4)
                                                       Đà Nẵng
                                                    @endif
                                                    @if ($tour->DepartureId ==12)
                                                        Đồng Nai
                                                    @endif
                                                    @if ($tour->DepartureId ==34)
                                                        Đồng Tháp
                                                    @endif
                                                    @if ($tour->DepartureId ==3)
                                                        Hà Nội
                                                    @endif
                                                    @if ($tour->DepartureId ==6)
                                                        Hải Phòng
                                                    @endif
                                                    @if ($tour->DepartureId ==10)
                                                        Huế                                                    
                                                    @endif
                                                    @if ($tour->DepartureId ==39)
                                                       Long An                                                   
                                                    @endif
                                                    @if ($tour->DepartureId ==14)
                                                        Long Xuyên                                                 
                                                    @endif
                                                    @if ($tour->DepartureId ==17)
                                                        Quảng Ninh                                                 
                                                    @endif
                                                    @if ($tour->DepartureId ==8)
                                                        Nha Trang                                                 
                                                    @endif
                                                    @if ($tour->DepartureId ==13)
                                                        Phú Quốc                                                
                                                    @endif
                                                    @if ($tour->DepartureId ==30)
                                                        Quảng Bình                                                
                                                    @endif
                                                    @if ($tour->DepartureId ==15)
                                                        Quảng Ngãi                                               
                                                    @endif
                                                    @if ($tour->DepartureId ==11)
                                                        Quy Nhơn                                              
                                                    @endif
                                                    @if ($tour->DepartureId ==22)
                                                        Rạch Giá                                             
                                                    @endif
                                                    @if ($tour->DepartureId ==35)
                                                        Sóc Trăng                                            
                                                    @endif
                                                    @if ($tour->DepartureId ==40)
                                                        Thái Nguyên                                            
                                                    @endif
                                                    @if ($tour->DepartureId ==16)
                                                        Vũng Tàu                                           
                                                    @endif
                                        </div>
                                    </div>
                                </div>
                                <form action="/cart/add-item" method="get" accept-charset="utf-8">
                                        <div class="sec3">
                                            <div class="row">
                                                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                                                    <div style="margin-bottom: 7px;" itemscope="" itemtype="">
                                                        <span class="price" itemprop="price" content="4690000">{{number_format($tour->price,0,'.',',')}} </span><span class="unit" itemprop="priceCurrency" content="VND">đ</span>
                                                    </div>
                                                    <div style="color:#000;">
                                                        Số chỗ còn nhận:
                                                        <span class="sit">{{$tour->so_luong_tour}}</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-right">
                                                    <button class="btn btn-book1 btn-md">Đặt ngay</button>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="tourId" value="{{$tour->id}}">
                                        <div class="row">
                                             <div style="color:#000; padding-left: 15px;">
                                                Số chỗ đăng kí:
                                                
                                            </div>
                                            <div class="col-md-5">
                                                 <input id="demo3" type="text" style="text-align: center;" value="1" name="quantity">
                                            </div>
                                            
                                      
                                        </div>
                                
                                </form>
                         
                                <div class="sec5">
                                    <div style="margin-bottom: 8px; color: #000;">Bạn cần hỗ trợ?</div>
                                    <div>
                                        
                                        <div class="f-left btn-s2" data-toggle="modal" data-target="#divTuVan" style="cursor:pointer;">
                                            <img src="images/btn-call2.png" alt="phone">
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="bg-phone hidden-md hidden-sm hidden-xs">
                                    <img src="images/bg-phone.png" alt="phone">
                                </div>
                            </div>
                        </div>
                   
                    
                </div>
                
            </div>
            <!-- chi tiết tour  -->
            <div class="container n3-tour-detail">
               <div class="row">
                <div class="group-tabs">
                   <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                     <div class="menu-left mg-bot30">
                        <div class="panel panel-default panel-side-menu">
                           <div class="panel-body panel-body-nav">
                              <div class="side-menu ">
                                 <ul class="nav nav-pills nav-stacked list-unstyled ">
                                    <li class="tab-chuongtrinhtour active">
                                       <a href="#chuongtrinhtour"  data-toggle="pill">
                                           <i class="fas fa-spinner"></i>
                                          
                                           <span>Chương trình tour</span>
                                       </a>
                                    </li>
                                    <li class="tab-chitiettour">
                                       <a href="#tabChiTiet"  data-toggle="pill">
                                           <i class="fas fa-list"></i>
                                          
                                           <span>Chi tiết tour</span>
                                       </a>
                                    </li>
                                    <li class="tab-luuy">
                                       <a  href="#luuy"   data-toggle="pill">
                                           <i class="fas fa-exclamation-triangle"></i>
                                          
                                           <span>Lưu ý</span>
                                       </a>
                                    </li>

                                    <li class="tab-lienhe">
                                       <a  href="#tabLienHe"  data-toggle="pill">
                                           <i class="fas fa-podcast"></i>
                                          
                                           <span>Liên hệ</span>
                                       </a>
                                    </li>
                                    <li class="tab-comment">
                                       <a  href="#tabcomment"  data-toggle="pill">
                                          <i class="fas fa-comment-alt"></i>
                                          
                                           <span>Đánh Giá</span>
                                       </a>
                                    </li>
                                 </ul>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="services">
                        <div class="title-lg">Dịch vụ đi kèm</div>
                        <div class="frame-service">
                           <ul class="list-service">
                              <li>Bữa ăn theo chương trình</li>
                              <li>Bảo hiểm</li>
                              <li>Hướng dẫn viên</li>
                              <li>Vé tham quan</li>
                              <li>Vận chuyển</li>
                           </ul>
                        </div>
                     </div>
                   
                  </div>
                  <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12 ">
                     <div class="sec-info tab-content">
                        <div class="chuongtrinhtour mg-bot30 tab-pane active"   id="chuongtrinhtour" >
                            
                            <div class="title-lg">
                                <h2>Chương trình tour</h2>
                            </div>
                            <div style="color:#000; padding: 10px; line-height: 1.7">
                                {{$tour->describe}}
                            </div>
                            
                            <div class="sec-content itinerary">
                                <div class="list">
                                    @for ($i = 0; $i <$tour->quantity ; $i++)
                                        <div class="list__item">
                                            <div class="list__time">
                                                <div class="num">{{$i+1}}.</div>
                                                <div class="day">{{$chuong_trinh_tour_ngay[$i]}}</div>
                                            </div>
                                            <div class="list__border"></div>
                                            <div class="list__desc">
                                                <h3 class="font500 name"><img src="images/i-marker.png" alt="marker">{{$ten_chuong_trinh[$i]}}</h3>
                                                <div class="d1 detail">
                                                   {!!$content[$i]!!}
                                                </div>
                                                <div class="an-hien ah1">
                                                    <div class="hienra">
                                                        <a href="#"> Xem thêm <i class="fas fa-arrow-down"></i></a>
                                                    </div>
                                                    <div class="andi" style="display: none;">
                                                        <a href="#"> Ẩn đi <i class="fas fa-arrow-up"></i></a>
                                                    </div>
                                                </div>
                                                <div class="border"></div>
                                            </div>
                                        </div>
                                    @endfor
                                    
                                    

                                </div>
                               


                            
                            </div>

                        </div>
                        <div class="chitiettour mg-bot30 tab-pane" id="tabChiTiet">
                            <div class="title-lg"><h2>Chi tiết tour</h2></div>
                            <div class="sec-content tour-detail">
                                <div id="divThongTinVanChuyen" style="padding: 20px;">
                                    <div class="tour-guide">
                                        <div class="title">
                                            <span class="hidden-xs">
                                            <img src="images/i-plane.png" alt="plane">
                                            </span>
                                            Thông tin vận chuyển
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr class="bold">
                                                            <td>Tên phương tiện</td>
                                                            <td>Dòng xe</td>
                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        
                                                            @foreach ($phuongTien as $element)
                                                                @if(check_value($tour,$element->id))
                                                                    <tr>
                                                                        <td data-title="Tên Khách sạn:">{{$element->pt_name}}</td>
                                                                        <td data-title="Địa chỉ:">{{$element->pt_loai}}</td>
                                                                        
                                                                    </tr>
                                                                @endif 
                                                            @endforeach
                                                            
                                                       
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="divThongTinKhachSan" style="padding: 0px 20px 20px 20px;">
                                    <div class="hotel">
                                        <div class="title">
                                            <span class="hidden-xs"><img src="images/i-hotel.png" alt="hotel"></span>
                                            Thông tin khách sạn
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr class="bold">
                                                            <td>Tên Khách sạn</td>
                                                            <td>Địa chỉ</td>
                                                            <td>Tiêu chuẩn</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        
                                                            @foreach ($hotel as $element)
                                                                @if(check_value_hotel($tour,$element->id))
                                                                    <tr>
                                                                        <td data-title="Tên Khách sạn:">{{$element->ks_name}}</td>
                                                                        <td data-title="Địa chỉ:">{{$element->ks_address}}</td>
                                                                        <td data-title="Tiêu chuẩn:">{{$element->tieu_chuan}}</td>
                                                                    </tr>
                                                                @endif 
                                                            @endforeach
                                                            
                                                       
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="divThongTinHDV" style="padding: 0px 20px 20px 20px;">
                                    <div class="tour-guide">
                                        <div class="title">
                                            <span class="hidden-xs">
                                            <img src="images/i-hdv.png" alt="hdv">
                                            </span>
                                            Thông tin hướng dẫn viên
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr class="bold">
                                                            <td>Họ tên</td>
                                                            <td>Địa chỉ</td>
                                                            <td>Điện thoại</td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            @foreach ($guide as $element)
                                                                @if(check_value_nv($tour,$element->id))
                                                                     <td data-title="Họ tên:">{{$element->full}}</td>
                                                                    <td data-title="Địa chỉ:">{{$element->address}}</td>
                                                                    <td data-title="Điện thoại:">{{$element->phone}}</td>
                                                                @endif 
                                                            @endforeach
                                                            
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="info" style="padding: 0px 20px 20px 20px;">
                                    <div class="title">
                                        <span class="hidden-xs"><img src="images/i-info.png" alt="info"></span>
                                        Thông tin tập trung
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <table class="table table-bordered">
                                                <tbody>
                                                    <tr>
                                                        <td>Ngày giờ tập trung</td>
                                                        <td>
                                                            {{$tour->time_tap_trung}}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Nơi tập trung
                                                        </td>
                                                        <td>
                                                            <span>{{$tour->dia_diem_tap_trung}}</span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="tour-price" style="padding: 0px 20px 20px 20px;">
                                    <div class="title">
                                        <span class="hidden-xs"><img src="images/i-tourprice.png" alt="tourprice"></span>
                                        Giá tour &amp; Phụ thu phòng đơn
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr class="bold">
                                                        <td>Loại khách</td>
                                                        <td>Giá tour</td>
                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td data-title="Loại khách">Người lớn (Từ 12 tuổi trở lên)</td>
                                                        <td data-title="Giá tour">{{$gia_rieng_le[0]}}<span> đ</span></td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td data-title="Loại khách">Trẻ em (Từ 5 tuổi đến dưới 12 tuổi)</td>
                                                        <td data-title="Giá tour">{{$gia_rieng_le[1]}}<span> đ</span></td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td data-title="Loại khách">Trẻ nhỏ (Từ 2 tuổi đến dưới 5 tuổi)</td>
                                                        <td data-title="Giá tour">{{$gia_rieng_le[2]}}<span> đ</span></td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td data-title="Loại khách">Em bé (Dưới 2 tuổi)</td>
                                                        <td data-title="Giá tour">{{$gia_rieng_le[3]}} đ</td>
                                                       
                                                    </tr>
                                                   
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="note" style="padding: 0px 20px 20px 20px;">
                                    <div class="title">
                                        <span class="hidden-xs">
                                        <img src="images/i-note.png" alt="note">
                                        </span>
                                        Ghi chú
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <p class="note-content">
                                                {{$tour->ghi_chu}}                                  
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="luuy mg-bot30 tab-pane"  id="luuy">
                            <div class="luuy mg-bot30" style="display: block;">
                                <div class="title-lg">
                                <h2>Lưu ý</h2>
                                </div>
                                <div class="sec-content attention" style="padding: 20px; color:#000;">
                                    {!!$tour->luu_y!!}
                                </div>
                                
                                
                            </div>
                        </div>
                        <div class="lienhe mg-bot30 tab-pane"  id="tabLienHe">
                            <div class="title-lg"><h2>Liên hệ</h2></div>

                            <div class="sec-content contact" style="padding:20px;">
                                <div class="row vpc">
                                    <div class="col-xs-12 title">
                                        Trụ sở chính
                                    </div>
                                    @foreach ($chi_nhanh as $element)
                                        @if ($element->featured == 1)
                                            
                                           
                                            <div class="col-sm-6 col-xs-12 mg-bot30">
                                                <p class="address mg-bot10"><strong>Địa chỉ: </strong> {{$element->address}}</p>
                                                <p class="mg-bot10"><strong>Điện thoại:</strong> {{$element->phone}}</p>
                                                <p class="mg-bot10"><strong>Fax:</strong> {{$element->fax}}</p>
                                                <p><strong>Email:</strong> {{$element->email}}</p>
                                            </div>
                                            
                                        @endif
                                    @endforeach
                                </div>
                               
                                <div class="row cn">

                                    <div class="mien" id="divMienNam" >
                                       <div class="col-xs-12 title">
                                            Chi Nhánh
                                        </div>

                                         @foreach ($chi_nhanh as $element)
                                            @if ($element->featured == 0)
                                                
                                               
                                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 mg-bot30" style="height:143px">
                                                    <div>
                                                        <p class="name">
                                                           {{$element->ten_chi_nhanh}}
                                                        </p>
                                                        <p class="mg-bot10">
                                                            <span class="address font500">Địa chỉ:</span> {{$element->address}}
                                                        </p>
                                                        <p class="mg-bot10">
                                                            <span class="address font500">Điện thoại:</span> {{$element->phone}}
                                                        </p>
                                                        <p class="mg-bot10">
                                                            <span class="address font500">Fax:</span> {{$element->fax}}
                                                        </p>
                                                        <p class="mg-bot10">
                                                            <span class="address font500">Email:</span> {{$element->email}}
                                                        </p>
                                                    </div>
                                                </div>
                                                
                                            @endif
                                        @endforeach
                                        
                                        
                                       
                                    </div>
                                 
                                </div>
                            </div>
                        </div>
                         <div class=" mg-bot30 tab-pane"  id="tabcomment">
                            <div class="title-lg"><h2>Đánh Giá</h2></div>
                           

                            <div class="sec-content contact" style="padding:20px;  border: 1px solid #dedede; border-radius: 5px">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="rating_item" style="position: relative; margin: auto;" >
                                            
                                                <span class="fa fa-star" style="font-size: 100px; color: #ff9705; display: block;margin: auto;text-align: center;"></span>
                                                <b style="position: absolute;top: 17%; left: 31%; transform: translateX(50%) translateY(50%);color: white; font-size: 25px">2.5</b>
                                           
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="list_rating">
                                            @for ($i = 1; $i <=5 ; $i++)
                                                <div class="item_rating"  style="display: flex;align-items: center;">
                                                    
                                                        <div style="width:10%;font-size: 14px">
                                                           {{$i}} <span style="" class="fa fa-star"></span>
                                                        </div>
                                                        <div style="width:60%; margin:0 20px;">
                                                            <span style="width: 100%; height: 8px; display: block;border: 1px solid #dedede; border-radius:5px; background: #dedede"><b style="width: 30%; background: #f25800; display: block;height: 100%; border-radius: 5px"></b></span>
                                                        </div>
                                                        <div style="width: 30%">
                                                            <a href="" style="text-decoration: none;color: black">(290)</a>
                                                        </div>
                                                    

                                                    
                                                </div>
                                            @endfor
                                        </div>
                                    </div>
                                    <div class="col-md-2" style="margin-top: 40px">
                                        @if (Auth::check())
                                           <a href="" id="demo" style="text-decoration: none;width:200px; padding: 10px;background: #ff002d; color: white; border: 1px solid #ff002d; border-radius:5px " class="js_rating_action">Gửi đánh giá </a>
                                        @else
                                            
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style="text-decoration: none;width:100px; padding: 10px;background: #ff002d; color: white; border: 1px solid #ff002d; border-radius:5px " class="comment_login_before">
                                              Đánh Giá
                                            </button>
                                        @endif
                                        
                                    </div>
                                    
                                </div>
                                @php
                                    $listRatingText = [
                                        1 =>'không thích',
                                        2=>'tạm đc',
                                        3=>'bình thường',
                                        4=>'tốt',
                                        5=>'rất tốt'
                                    ];

                                @endphp
                                <div class="form_rating hide">
                                    <div style="display: flex; margin: 15px; font-size: 15px">
                                        <p> Chọn đánh giá của bạn</p>
                                        <span style="margin: 0 15px" class="list_start">
                                            @for ($i = 1; $i <=5 ; $i++)
                                                <i class="fa fa-star" data-key="{{$i}}"></i>
                                            @endfor
                                        </span>
                                        <span class="list_text"> Tốt</span>
                                        <input type="hidden" name="" value="" class="number_rating" placeholder="">
                                    </div>
                                    <div style="margin-top: 15px">
                                        <textarea name="" id="editor" class="form-control content_rating" cols="30" rows="30"></textarea>
                                        
                                    </div>
                                    <div style="margin: 15px" >
                                        
                                            <a href="/ajax/danh-gia/{{$tour->id}}" class="js_rating_product" style="width:200px; padding: 10px;background: #ff002d; color: white; border: 1px solid #ff002d; border-radius:5px ">Gửi đánh giá</a>
                                        {{-- route('post.rating.product',$tour) == ajax/danh-gia/{$tour->id} --}}
                                    </div>
                                </div>
                            
                            </div>
                             
                        </div>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="line-ct"></div>
                  </div>
                </div>



               </div>
            </div>
            <!-- end chi tiết tour -->
          <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                   
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body" style="color: black; font-size: 20px">
                    Bạn hãy đăng nhập để có thể bình luận 
                  </div>
                
                </div>
              </div>
            </div>
            

@endsection
@section('script-rating')
    <script>
       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function(){
            let listStart = $(".list_start .fa");
              listRatingText ={
                    1:'không thích',
                    2:'Tạm được',
                    3:'Bình thường',
                    4:'Tốt',
                    5:'Rất tốt'
                }
            listStart.mouseover(function() {
                
                let $this = $(this);
                let number = $this.attr('data-key');
                listStart.removeClass('rating_active');

                $('.number_rating').val(number);

                $.each(listStart, function(key, Value){
                    if(key +1 <=number){// do vongf lapwj tuw 0 maf ddang gia tuw 1 dden 5
                        $this.addClass('rating_active')
                       // console.log($this);
                    }
                });
                

                $(".list_text").text('').text(listRatingText[$this.attr('data-key')]).show();
                // console.log($this.attr('data-key'));
            });
             $('.js_rating_action').click(function (event) {
                event.preventDefault();// ko refresh

                if($(".form_rating").hasClass('hide')){
                    $(".form_rating").removeClass('hide').addClass('active');
                    document.getElementById("demo").innerHTML = "Đóng lại";

                    
                }else{
                     $(".form_rating").removeClass('active').addClass('hide');
                     document.getElementById("demo").innerHTML = "Gửi đánh giá";
                    
                }
            });
            $('.js_rating_product').click(function(event){
                event.preventDefault();
                let content =CKEDITOR.instances.editor.getData();
                let number = $(".number_rating").val();
                let url = $(this).attr('href');
                // console.log(url);
                if(content && number)
                {
                    $.ajax({
                            url: url,
                            type:'POST',
                            data:{
                                number:number,
                                ra_content:content

                            }
                        }).done(function(result) {
                          
                          //console.log(result);
                          if(result.code == 1){
                            alert("Success");
                            location.reload();

                          }
                        }); 
                    
                }
             });

            // lưu id sp
            let idTours = $("#content_tour").attr('data-id');
            // lấy giá trị storega // lấy giá trị về mảng id đã lưu
            let tours = localStorage.getItem('tours');
           

             if(tours == null){
                arrayTour = new Array();
                arrayTour.push(idTours);
                // do storage luu dang string neen phair doi json sang string r ms push vaof
                localStorage.setItem('tours',JSON.stringify(arrayTour))
             }else{
                
                // chuyển về mảng
                tours = $.parseJSON(tours);
                // indexOf trả về vị trí trong mảng vs key chính là id của sản phẩm .tránh bị tăng lượt đã xem
                if (tours.indexOf(idTours)==-1){
                    tours.push(idTours);
                    localStorage.setItem('tours',JSON.stringify(tours));
                }
                console.log(tours);
                
             }
           
        });
         
    </script>
@stop