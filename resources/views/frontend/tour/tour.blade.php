@extends('frontend.master.master')
@section('name','Tour')
@section('content')
<div class="main-body">

<div class="n3-breadcrumb">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 ">
                <div class="breadcrumb">
                    <span itemscope="" itemtype="">
                    <a href="" itemprop="url">
                    <span itemprop="title" style="color: #ff002d;">Du lịch</span>
                    </a>
                    </span>
                    » 
                    <span itemscope="" itemtype="">
                    <a href="" itemprop="url">
                    <span itemprop="title">Các tour du lịch</span>
                    </a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container n3-list-tour mg-bot40">
    <div class="row">
        <div class="col-lg-3 col-md-3 mg-bot30">
            <form action="" method="get" accept-charset="utf-8">
                <div class="search-sidebar">
                    <div class="title">
                        Tìm Kiếm
                    </div>
                    <div class="frame-search">
                        <div class="noikhoihanh mg-bot15">
                            <label for="">Nơi khởi hành</label>
                            <select class="form-control" id="departureIDFilter" name="DepartureId">
                                <option value="" selected="">Nơi khởi hành</option>
                                <option value="1">Hồ Chí Minh</option>
                                <option value="32">Bạc Liêu</option>
                                <option value="33">Bảo Lộc</option>
                                <option value="7">Bình Dương</option>
                                <option value="18">Buôn Ma Thuột</option>
                                <option value="20">Cà Mau</option>
                                <option value="5">Cần Thơ</option>
                                <option value="24">Đà Lạt</option>
                                <option value="4">Đà Nẵng</option>
                                <option value="12">Đồng Nai</option>
                                <option value="34">Đồng Tháp</option>
                                <option value="3">Hà Nội</option>
                                <option value="6">Hải Phòng</option>
                                <option value="10">Huế</option>
                                <option value="39">Long An</option>
                                <option value="14">Long Xuyên</option>
                                <option value="17">Quảng Ninh</option>
                                <option value="8">Nha Trang</option>
                                <option value="13">Phú Quốc</option>
                                <option value="30">Quảng Bình</option>
                                <option value="15">Quảng Ngãi</option>
                                <option value="11">Quy Nhơn</option>
                                <option value="22">Rạch Giá</option>
                                <option value="35">Sóc Trăng</option>
                                <option value="40">Thái Nguyên</option>
                                <option value="16">Vũng Tàu</option>
                            </select>
                        </div>
                        <div class="loaitour mg-bot15">
                            <label>Loại tour</label>
                            <select class="form-control" id="TourTypeIdFilter" name="loai_tour">
                                <option value="" selected="">Loại Tour</option>
                                <option value="1">Trong nước</option>
                                <option value="2">Nước ngoài</option>
                            </select>
                        </div>
                        <div class="loaitour mg-bot15">
                            <label>Dòng tour</label>
                            <select class="form-control" id="TourTypeIdFilter" name="dong_tour">
                                <option value="" selected="">Dòng tour</option>
                                <option value="1" >Tiết kiệm</option>
                                <option value="2">Tiêu chuẩn</option>
                                <option value="3">Cao cấp</option>
                            </select>
                        </div>
                        <div class="noiden mg-bot15">
                            <label>Nơi đến</label>
                            <select class="form-control" id="group_idFilter" name="address_id">
                                <option selected="" value="">Nơi đến</option>

                                @foreach ($address as $element)
                                    @if ($element->parent != 0)
                                        <option  value="{{$element->id}}">{{$element->name}}</option>
                                    @endif
                                   
                                @endforeach
                            </select>
                        </div>
                        <div class="ngaykhoihanh mg-bot15">
                            <label>Ngày khởi hành</label>
                            <div class="pos-relative">
                                <input type="text" id="departureDateFilter"  name="ngay_khoi_hanh" class="form-control input-md hasDatepicker" autocomplete="off" value="" placeholder="Ngày khởi hành">
                                <span class="i-calendar">
                                <i class="far fa-calendar-alt fa-lg"></i>
                                </span>
                            </div>
                        </div>
                        <div class="songay mg-bot20">
                            <label>Giá</label>
                            <select class="form-control" id="priceIDFilter" name="PriceId">
                                <option selected="" value="">Giá</option>
                                <option value="0-1000000">Dưới 1 Triệu</option>
                                <option value="1000000-2000000">  1- 2  Triệu</option>
                                <option value="2000000-4000000">  2-4 Triệu</option>
                                <option value="4000000-6000000">  4-6 Triệu</option>
                                <option value="6000000-10000000">  6-10 Triệu</option>
                                <option value="10000000-20000000">Trên 10 Triệu</option>
                            </select>
                        </div>
                        <div class="but mg-bot15">
                            <button type="submit" class="btn btn-md btn-search" onclick="">Tìm kiếm</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <div class="list-tour" itemscope="" itemtype="">
                <div class="des-tour">
                    <h1 itemprop="name"><a href="">Du lịch </a></h1>
                </div>
            </div>
            <div class="rating-view">
                <div class='rating-stars '>
                    <ul id='stars'>
                        <li class='star' title='Poor' data-value='1'>
                            <i class='fa fa-star fa-fw'></i>
                        </li>
                        <li class='star' title='Fair' data-value='2'>
                            <i class='fa fa-star fa-fw'></i>
                        </li>
                        <li class='star' title='Good' data-value='3'>
                            <i class='fa fa-star fa-fw'></i>
                        </li>
                        <li class='star' title='Excellent' data-value='4'>
                            <i class='fa fa-star fa-fw'></i>
                        </li>
                        <li class='star' title='WOW!!!' data-value='5'>
                            <i class='fa fa-star fa-fw'></i>
                        </li>
                        <li class='star'><strong style="color: #000;">4.6/5</strong> trong <strong style="color: #000">4654</strong> Đánh giá</li>
                    </ul>
                </div>
            </div>
            <div class="des-content">
                <div class="mg-bot10">
                    <div itemprop="description" class="content-tour" style="margin-bottom: 10px;">
                        Du lịch luôn là lựa chọn tuyệt vời. Đường bờ biển dài , những khu bảo tồn thiên nhiên tuyệt vời, những thành phố nhộn nhịp, những di tích lịch sử hào hùng, nền văn hóa độc đáo và hấp dẫn, cùng một danh sách dài những món ăn ngon nhất thế giới, Việt Nam có tất cả những điều đó. Với lịch trình dày, khởi hành đúng thời gian cam kết, FriendTravels là công ty lữ hành uy , luôn sẵn sàng phục vụ du khách mọi lúc, mọi nơi, đảm bảo tính chuyên nghiệp và chất lượng dịch vụ tốt nhất thị trường
                    </div>
                </div>
            </div>
            <div class="col-md-12 text-center" style="padding-bottom:25px;padding-top:20px">
                <h2 class="title-tour" style="text-shadow:none;">
                    <a href="">Danh sách tour du lịch</a>
                </h2>
            </div>
          
                
           
            @foreach ($tour as $element)
                <div class="content-tour-view">
                    
                    @if ($element->ngay_khoi_hanh[0] !=null)
                        <div class="col-lg-1 col-lg-tour-padd col-md-1 hidden-sm hidden-xs">
                            <div class="frame-day-scroll" id="tagMax334">
                                <div class="text-center">

                                    <div class="f-day">
                                        {{$element->ngay_khoi_hanh[0]}}
                                       {{--  {{dd($element)}} --}}
                                    </div>
                                    <div class="f-date">{{$element->ngay_khoi_hanh[1]}}/{{$element->ngay_khoi_hanh[2]}}</div>
                                </div>
                            </div>
                        </div>
                    @else

                         <div class="col-lg-1 col-lg-tour-padd col-md-1 hidden-sm hidden-xs">
                            <div class="frame-day-scroll" id="tagMax334">
                                <div class="text-center">

                                    <div class="f-day">
                                        
                                       {{--  {{dd($element)}} --}}
                                    </div>
                                    <div class="f-date"></div>
                                </div>
                            </div>
                        </div>
                    @endif

                    
                    <div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
                        <div class="item mg-bot30">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="tour-name">
                                        <a href="/tour/tour-details/{{$element->slug}}-{{$element->id}}.html" title="{{$element->name}}">
                                            <h3>{{$element->name}}</h3>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                    <div class="pos-relative">
                                        <div class="badge-promo">
                                            <div class="badge-promo-content">
                                                <div><i class="fas fa-spinner"></i>&nbsp;&nbsp;
                                                    @if ($element->dong_tour==1)
                                                        Tiết kiệm
                                                    @endif
                                                    @if ($element->dong_tour==2)
                                                        Tiêu chuẩn
                                                    @endif
                                                    @if ($element->dong_tour==3)
                                                        Cao cấp
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <a href="" title=""><img src="{{$element->avatar}}" class="img-responsive pic-lt" alt=""></a>
                                    </div>
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                    <div class="frame-info">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12 mg-bot10">
                                                <div class="f-left group-listtour" style="width:100%">
                                                    <div class="rating-view rating-view-tour-detail">
                                                        <div class='rating-stars '>
                                                            <ul id='stars'>
                                                                <li class='star' title='Poor' data-value='1'>
                                                                    <i class='fa fa-star fa-fw'></i>
                                                                </li>
                                                                <li class='star' title='Fair' data-value='2'>
                                                                    <i class='fa fa-star fa-fw'></i>
                                                                </li>
                                                                <li class='star' title='Good' data-value='3'>
                                                                    <i class='fa fa-star fa-fw'></i>
                                                                </li>
                                                                <li class='star' title='Excellent' data-value='4'>
                                                                    <i class='fa fa-star fa-fw'></i>
                                                                </li>
                                                                <li class='star' title='WOW!!!' data-value='5'>
                                                                    <i class='fa fa-star fa-fw'></i>
                                                                </li>
                                                                <li class='star' style="color: #000"><strong style="color: #000;">4.6/5</strong> trong <strong style="color: #000">4654</strong> Đánh giá</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="col-md-7 col-sm-7 col-xs-12 mg-bot10">
                                                <div class="f-left l"><img src="images/i-code.png" alt="code"></div>
                                                <div class="f-left r">{{$element->code}}</div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="col-md-5 col-sm-5 col-xs-12 mg-bot10">
                                                <div class="f-left l"><img src="images/i-chair.png" alt="chair"></div>
                                                <div class="f-left r">
                                                    Số chỗ còn nhận: <span class="font500">{{$element->so_luong_tour}}</span>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="col-md-7 col-sm-7 col-xs-12 mg-bot10">
                                                <div class="f-left l"><img src="images/i-date.png" alt="date"></div>
                                                <div class="f-left r">Ngày đi: <span class="font500">{{implode( '-', $element->ngay_khoi_hanh)}}</span></div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="col-md-5 col-sm-5 col-xs-12 mg-bot10">
                                                <div class="f-left l"><img src="images/i-clock.png" alt="clock"></div>
                                                <div class="f-left r">Giờ đi: <span class="font500">{{$element->time_tap_trung}}</span></div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="col-md-7 col-sm-7 col-xs-12 mg-bot10">
                                                <div class="f-left l"><img src="images/i-price.png" alt="price"></div>
                                                <div class="f-left r">
                                                    Giá:
                                                    <span class="font500 price">{{number_format($element->price,0,'.',',')}}đ</span>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="col-md-5 col-sm-5 col-xs-12">
                                                <div class="f-left l"><img src="images/i-calendar.png" alt="date"></div>
                                                <div class="f-left r">Số ngày: <span class="font500">{{$element->so_ngay}}</span></div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 mg-bot10">
                                                <div class="f-left l"><img src="images/i-dep.png"></div>
                                                <div class="f-left r">Nơi khởi hành: <span class="font500"> 
                                                    @if ($element->DepartureId ==0)
                                                        
                                                    @endif
                                                    @if ($element->DepartureId ==1)
                                                        Hồ Chí Minh
                                                    @endif
                                                    @if ($element->DepartureId ==32)
                                                        Bạc Liêu
                                                    @endif
                                                    @if ($element->DepartureId ==33)
                                                        Bảo Lộc
                                                    @endif
                                                    @if ($element->DepartureId ==7)
                                                        Bình Dương
                                                    @endif
                                                    @if ($element->DepartureId ==18)
                                                        Buôn Ma Thuột
                                                    @endif
                                                    @if ($element->DepartureId ==20)
                                                        Cà Mau
                                                    @endif
                                                    @if ($element->DepartureId ==5)
                                                        Cần Thơ
                                                    @endif
                                                    @if ($element->DepartureId ==24)
                                                        Đà Lạt
                                                    @endif
                                                    @if ($element->DepartureId ==4)
                                                       Đà Nẵng
                                                    @endif
                                                    @if ($element->DepartureId ==12)
                                                        Đồng Nai
                                                    @endif
                                                    @if ($element->DepartureId ==34)
                                                        Đồng Tháp
                                                    @endif
                                                    @if ($element->DepartureId ==3)
                                                        Hà Nội
                                                    @endif
                                                    @if ($element->DepartureId ==6)
                                                        Hải Phòng
                                                    @endif
                                                    @if ($element->DepartureId ==10)
                                                        Huế                                                    
                                                    @endif
                                                    @if ($element->DepartureId ==39)
                                                       Long An                                                   
                                                    @endif
                                                    @if ($element->DepartureId ==14)
                                                        Long Xuyên                                                 
                                                    @endif
                                                    @if ($element->DepartureId ==17)
                                                        Quảng Ninh                                                 
                                                    @endif
                                                    @if ($element->DepartureId ==8)
                                                        Nha Trang                                                 
                                                    @endif
                                                    @if ($element->DepartureId ==13)
                                                        Phú Quốc                                                
                                                    @endif
                                                    @if ($element->DepartureId ==30)
                                                        Quảng Bình                                                
                                                    @endif
                                                    @if ($element->DepartureId ==15)
                                                        Quảng Ngãi                                               
                                                    @endif
                                                    @if ($element->DepartureId ==11)
                                                        Quy Nhơn                                              
                                                    @endif
                                                    @if ($element->DepartureId ==22)
                                                        Rạch Giá                                             
                                                    @endif
                                                    @if ($element->DepartureId ==35)
                                                        Sóc Trăng                                            
                                                    @endif
                                                    @if ($element->DepartureId ==40)
                                                        Thái Nguyên                                            
                                                    @endif
                                                    @if ($element->DepartureId ==16)
                                                        Vũng Tàu                                           
                                                    @endif
                                                </span></div>
                                                <div class="clear"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <div align='right'>
                <nav aria-label="Page navigation example">
                    {{$tour->links('vendor.pagination.default')}}
                </nav>
            </div>
           
        </div>
    </div>
</div>
@endsection
