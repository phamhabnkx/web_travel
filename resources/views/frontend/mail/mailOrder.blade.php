<P>Xác nhận đơn hàng</P>
xin chao <strong>{{$customer->full}}</strong>, mời bạn kiểm tra lại đơn hàng của mình
<div class="cart-content-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="cart-content-inner">
                    <!-- cart content inner -->
                    <div class="top-content">
                        <!-- top content -->
                        <table class="table table-responsive" style=" border: 1px solid black;
                            border-collapse: collapse;width: 100%">
                            <thead>
                                <tr>
                                    <th style=" border: 1px solid black;
                                        border-collapse: collapse;padding: 15px">Image</th>
                                    <th style=" border: 1px solid black;
                                        border-collapse: collapse;padding: 15px">Tour</th>
                                    <th style=" border: 1px solid black;
                                        border-collapse: collapse;padding: 15px">Price</th>
                                    <th style=" border: 1px solid black;
                                        border-collapse: collapse;padding: 15px">Quantity</th>
                                    <th style=" border: 1px solid black;
                                        border-collapse: collapse;padding: 15px">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($informationOrder as $infor)
                                <tr>
                                    <td style=" border: 1px solid black;
                                        border-collapse: collapse; padding: 15px">
                                        <div class="product-details">
                                            <!-- product details -->
                                            <div class="content" style="text-align: center;">
                                                <img src="{{$message->embed(public_path().$infor->img)}}"  alt=""  style="width: 150px; height: 150px">
                                                {{--  @php
                                                dd(URL::asset('/backend/img/'.$infor->img));
                                                @endphp --}}
                                            </div>
                                        </div>
                                        <!-- //. product detials -->
                                    </td>
                                    <td style=" border: 1px solid black;
                                        border-collapse: collapse; padding: 15px">
                                        <div class="product-details">
                                            <!-- product details -->
                                            <div class="content" style="text-align: center;">
                                                <h4 class="title">{{$infor->name}}</h4>
                                            </div>
                                        </div>
                                        <!-- //. product detials -->
                                    </td>
                                    <td style=" border: 1px solid black;
                                        border-collapse: collapse; padding: 15px">
                                        <div class="price" style="text-align: center;">{{number_format($infor->price,0,'','.') }}</div>
                                    </td>
                                    <td style=" border: 1px solid black;
                                        border-collapse: collapse; padding: 15px">
                                        <div class="qty" style="text-align: center;">
                                            <span class="qttotal" >{{ $infor->quantity }}</span>
                                        </div>
                                    </td>
                                    <td style=" border: 1px solid black;
                                        border-collapse: collapse; padding: 15px">
                                        <div class="price" style="text-align: center;">{{number_format($customer->total,0,'','.')}}</div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- //. top content -->
                    <div class="bottom-content">
                        <!-- bottom content -->
                        <div class="right-content-area">
                            <div class="cart-total">
                                <h3 class="title">Cart Totals</h3>
                                <ul class="cart-list">
                                    <li>Subtotal <span class="right">{{number_format($customer->total,0,'','.')}}</span></li>
                                    <li>Shipping </li>
                                    <li>Calculate shipping </li>
                                    <li>Tax (estimated for Bangladesh)   <span class="right">$0.00</span></li>
                                    <li class="total">Total <span class="right"><strong>{{number_format($customer->total,0,'','.')}}</strong></span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<p>chúng tôi sẽ liên lạc lại vs quí khách nhanh nhất,xin cảm ơn quý khách đã mua hàng</p>