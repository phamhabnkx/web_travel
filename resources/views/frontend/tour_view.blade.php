            <div class="destinations" id="dia_diem">
                <div class="container">
                    <div class="row">
                        <div class="col text-center" >
                            
                            <div class="section_title">
                                <h2>Địa điểm vừa xem</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row destinations_row">
                        <div class="col">
                            <div class="destinations_container item_grid">
                                <!-- Destination -->
                                @foreach ($tourView as $element)
                                    <div class="destination item col-lg-3" title="{{$element->name}}">
                                        <div class="destination-item-view" style="padding-bottom:0px ">
                                            <div class="destination_image">
                                            
                                                <div class="view-eye">
                                                  <a href="/tour/tour-details/{{$element->slug}}-{{$element->id}}.html" >
                                                    <img src="{{$element->avatar}}" alt="">
                                                  </a>
                                                    
                                                    
                                                </div>
                                                <div class="spec_offer text-center"><p class="tag"><span class="new" style="font-size: 11px; background: #FF9600; color: #fff;padding: .3em .5em;">New</span></p></div>

                                            </div>
                                           
                                        </div>
                                        

                                    </div>
                                @endforeach
                               
                               
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>