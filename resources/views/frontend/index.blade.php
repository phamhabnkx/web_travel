@extends('frontend.master.master')
@section('name','Trang chủ')
@section('content')
@if (get_data_user('web','active') == 1)
                        <div class="alert alert-success" role="alert">
                           Tài khoản của bạn chưa xác thực
                        </div>
                    @endif
		  <div class="main-body">
            <div class="video-area" id="video-area">
               
                <div class="player" id="video-play" data-property="{videoURL:'@if (isset($data)){{$data->link_video_background}}@endif', containment:'#video-area', showControls:false, autoPlay:true, zoom:0, loop:true, mute:true, startAt:1, opacity:1, quality:'low',}"></div>
                <div class="video-table">
                    <div class="video-table-cell">
                        <div class="container center-holder">
                            <div class="video-effect">
                                <div class="video-effect-box">
                                    <div class="video-effect-content">
                                        <h2 data-animate-scroll='{"x": "-20","y": "-20","rotation": "-3","alpha": "0", "duration": ".8"}'>Hãy để chúng tôi đưa bạn đi</h2>
                                        <div class="home_search">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="home_search_container">
                                                            <div class="home_search_title">Tìm kiếm chuyến đi</div>
                                                            <div class="home_search_content">
                                                                <form action="/search" class="home_search_form" method="get" id="home_search_form">
                                                                    <div class="d-flex flex-lg-row flex-column align-items-start justify-content-lg-between justify-content-start">
                                                                        <input type="text" class="search_input search_input_1" placeholder="vd : Đà lạt" name="thanh_pho" required="required">
                                                                        
                                                                        <input type="text" class="search_input search_input_4" placeholder="Chi phí tối thiểu : 1000000 " name="min" required="required">
                                                                        <input type="text" class="search_input search_input_4" placeholder="Chi phí tối đa 1000000000000" name="max" required="required">
                                                                        <button class="home_search_button">Tìm kiếm</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="home_page_nav">
                                            <ul class="d-flex flex-column align-items-end justify-content-end">
                                                <li><a href="#" data-scroll-to="#dia_diem" style="font-family: sans-serif;">Địa điểm phổ biến<span>01</span></a></li>
                                                <li><a href="#" data-scroll-to="#nhan_xet" style="font-family: sans-serif;">Nhận xét & đánh giá<span>02</span></a></li>
                                                <li><a href="#" data-scroll-to="#tin_tuc_new" style="font-family: sans-serif;">Tin tức<span>03</span></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="intro" >
                <div class="intro_background" style="background-image:url(images/intro.png)"></div>
                <div class="container" >
                    <div class="row">
                        <div class="col">
                            <div class="intro_container" >
                                <div class="row">
                                    <!-- Intro Item -->
                                    <div class="col-lg-4 intro_col">
                                        <div class="intro_item d-flex flex-row align-items-end justify-content-start">
                                            <div class="intro_icon"><img src="images/beach.svg" alt=""></div>
                                            <div class="intro_content">
                                                <div class="intro_title">Top điểm đến</div>
                                                <div class="intro_subtitle">
                                                    <p>Điểm đến đã được lựa chọn và khảo sát</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Intro Item -->
                                    <div class="col-lg-4 intro_col">
                                        <div class="intro_item d-flex flex-row align-items-end justify-content-start">
                                            <div class="intro_icon"><img src="images/wallet.svg" alt=""></div>
                                            <div class="intro_content">
                                                <div class="intro_title">Giá tour tốt nhất</div>
                                                <div class="intro_subtitle">
                                                    <p>Phù hợp với mọi đối tượng</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Intro Item -->
                                    <div class="col-lg-4 intro_col">
                                        <div class="intro_item d-flex flex-row align-items-end justify-content-start">
                                            <div class="intro_icon"><img src="images/suitcase.svg" alt=""></div>
                                            <div class="intro_content">
                                                <div class="intro_title">Dịch vụ tuyệt vời</div>
                                                <div class="intro_subtitle">
                                                    <p>Chúng tôi cung cấp dịch vụ cần thiết</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="destinations" id="dia_diem">
                <div class="container">
                    <div class="row">
                        <div class="col text-center" >
                            <div class="section_subtitle">Đơn giản là những nơi tuyệt vời</div>
                            <div class="section_title">
                                <h2>Địa điểm phổ biến</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row destinations_row">
                        <div class="col">
                            <div class="destinations_container item_grid">
                                <!-- Destination -->
                                @foreach ($details as $element)
                                    <div class="destination item col-lg-3" title="{{$element->name}}">
                                        <div class="destination-item-view">
                                            <div class="destination_image">
                                            
                                                <div class="view-eye">
                                                  
                                                    <img src="{{$element->avatar}}" alt="">
                                                    
                                                </div>
                                                <div class="spec_offer text-center"><p class="tag"><span class="new" style="font-size: 11px; background: #FF9600; color: #fff;padding: .3em .5em;">New</span></p></div>

                                            </div>
                                            <div class="destination_content" style="margin-bottom: 40px;">
                                                <div class="destination_title"><a href="" title="{{$element->address->name}}" style="font-family:initial; font-size: 18px; ">{{$element->address->name}}</a></div>
                                                <div class="destination_subtitle">
                                                    <p>{{$element->name}}</p>
                                                </div>
                                                <div class="destination_price">{{number_format($element->price,0,'.','.')}} đ</div>
                                               
                                            </div>
                                            <div class="mt-30 mb-15 float-item-destination">
                                                <a href="/tour/tour-details/{{$element->slug}}-{{$element->id}}.html" class="dark-button button-md">Khám phá</a>
                                                {{-- <a href="/tour/tour-details/{{$element->slug}}-{{$element->id}}.html" class="dark-button button-md" style="background:#FF9600;">Đặt tour</a>  --}} 
                                            </div>  
                                        </div>
                                        

                                    </div>
                                @endforeach
                               
                               
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <!-- Milestones -->
            <div class="milestones" >
                <div class="row milestones_row">
                    <!-- Milestone -->
                    <div class="col-lg-3 milestone_col">
                        <div class="milestone text-center">
                            <div class="milestone_icon"><img src="/frontend/images/mountain.svg" alt=""></div>
                            <div class="milestone_counter" data-end-value="17">17</div>
                           
                        </div>
                    </div>
                    <!-- Milestone -->
                    <div class="col-lg-3 milestone_col">
                        <div class="milestone text-center">
                            <div class="milestone_icon"><img src="/frontend/images/island.svg" alt=""></div>
                            <div class="milestone_counter" data-end-value="213">213</div>
                            
                        </div>
                    </div>
                    <!-- Milestone -->
                    <div class="col-lg-3 milestone_col">
                        <div class="milestone text-center">
                            <div class="milestone_icon"><img src="/frontend/images/camera.svg" alt=""></div>
                            <div class="milestone_counter" data-end-value="11923">11923</div>
                           
                        </div>
                    </div>
                    <!-- Milestone -->
                    <div class="col-lg-3 milestone_col">
                        <div class="milestone text-center">
                            <div class="milestone_icon"><img src="/frontend/images/boat.svg" alt=""></div>
                            <div class="milestone_counter" data-end-value="15">15</div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="section-block-parallax" style="background-image: url('/frontend/images/home_mobile_bg.jpg');" id="nhan_xet">
                <div class="container">
                    <div class="owl-carousel owl-theme" id="testmonials">
                        <div class="testmonial-block mt-40">
                            <div class="radius-img-comment">
                                <img src="/frontend/images/user.jpg" alt="">
                            </div>
                            <h4>Nguyễn Văn A</h4>
                            9:00 AM - 2/8/2019
                            <h6>Director</h6>
                            <p>Tour du lịch tại FriendTravels mang lại cảm thú vị, tuyệt vời. Tìm hiểu được các kinh nghiệm đời sống.....</p>
                        </div>
                        <div class="testmonial-block mt-40">
                            <div class="radius-img-comment">
                                <img src="/frontend/images/user.jpg" alt="">
                            </div>
                            <h4>Nguyễn Văn B</h4>
                            9:00 AM - 2/8/2019
                            <h6>Trần Văn B</h6>
                            <p>Tour du lịch tại FriendTravels mang lại cảm thú vị, tuyệt vời. Tìm hiểu được các kinh nghiệm đời sống.....</p>
                        </div>
                        <div class="testmonial-block mt-40">
                            <div class="radius-img-comment">
                                <img src="/frontend/images/user.jpg" alt="">
                            </div>
                            <h4>Nguyễn Văn C</h4>
                            9:00 AM - 2/8/2019 
                            <h6>Marketing</h6>
                            <p>Tour du lịch tại FriendTravels mang lại cảm thú vị, tuyệt vời. Tìm hiểu được các kinh nghiệm đời sống.....</p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="adress">
                <div class="container">
                    <div class="row">
                        <h2 class="hot-address">ĐIỂM ĐẾN HOT NHẤT MÙA HÈ 2019</h2>
                        <div class="slick marquee">
                            @foreach ($details as $element)
                                <div class="slick-slide">
                                <div class="inner">
                                    <a href="/tour/tour-details/{{$element->slug}}-{{$element->id}}.html" title="{{$element->name}}">
                                    <img src="{{$element->avatar}}" class="img-zoom" style="width:160px!important; height: 150px!important; border-radius: 50%;" />
                                    </a>
                                </div>
                            </div>
                            @endforeach
                            
                            
                        </div>
                    </div>
                </div> 
            </div>

            <div class="layout layout6" id="tin_tuc_new">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-xs-12 video-box">
                            <div class="title-thi title-video-hot">
                                <h2>
                                    <a href="" title="video">Video</a>
                                </h2>
                            </div>
                            <div class="slider-video-index">
                                @if (isset($data))
                                    <div class="col-vi-content">
                                        <div class="box-video">
                                            <div class="box-video-content">
                                                <a href="javascript:void(0)" onclick="buttonVideo(this)" title=""> </a>
                                                <iframe width="100%" height="100%" frameborder="0" src="https://www.youtube-nocookie.com/embed/{{$ID_video[0]}}" allowfullscreen="1" allowtransparency="true"></iframe>
                                            </div>
                                        </div>
                                        <div class="box-list-video">
                                            @for($i =0 ; $i <count($ID_video); $i++)
                                                 <div class="box-image">
                                                    <a onclick="changeVideo(this)" name="https://www.youtube.com/embed/{{$ID_video[$i]}}" class="item-list" title="" href="javascript:void(0)">
                                                    <img src="/upload_avatar/{{$avatar[$i]}}" >
                                                    </a>
                                                </div>
                                            @endfor
                                           
                                            {{-- <div class="box-image">
                                                <a onclick="changeVideo(this)" name="https://www.youtube.com/embed/JQ9bET0lq4w" class="item-list" title="" href="javascript:void(0)">
                                                <img src="images/video.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="box-image">
                                                <a onclick="changeVideo(this)" name="https://www.youtube.com/embed/IFZul62m54s" class="item-list " title="" href="javascript:void(0)">
                                                <img src="images/video3.jpg" alt="">
                                                </a>
                                            </div>
                                            <div class="box-image">
                                                <a onclick="changeVideo(this)" name="https://www.youtube.com/embed/bNfvj0BDcEw" class="item-list " title="" href="javascript:void(0)">
                                                <img src="images/video4.jpg" alt="">
                                                </a>
                                            </div> --}}
                                        </div>
                                    </div>
                                @endif
                               
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12 new-box">
                            <div class="title-thi title-new-box">
                                <h2>
                                    <a href="" title="#">Tin tức</a>
                                </h2>
                            </div>
                            <div class="list-new-box">
                                @foreach ($blog as $element)
                                    <div class="col-new item-new-box">
                                        <div class="box-img">
                                            <a href="index.html" title="">
                                                <img class="img-zoom img-responsive" src="{{$element->bai_viet_img}}" alt="">
                                            </a>
                                        </div>
                                        <div class="box-info">
                                            <div class="name-new">
                                                <h4>
                                                    <a href="/tin-tuc/tin-tuc-details/{{str_slug($element->name)}}-{{$element->id}}.html" title="{{$element->name}}">
                                                        {{$element->name}}
                                                    </a>
                                                </h4>
                                            </div>
                                            <div class="post-content">
                                                <span class="post-date">
                                                    @php
                                                        $time=Carbon\Carbon::parse($element->updated_at);
                                                    @endphp
                                                    <i class="far fa-calendar-alt" aria-hidden="true"></i>
                                                    {{date('H:i d-m-Y', strtotime($time))}}                     
                                                </span>

                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                               
                           
                                <div class="view-all-new col-100">
                                    <a href="/tin-tuc" title="">
                                    Xem thêm 
                                    <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="tour_view" >
                
            </div>
        
        </div>
       {{--  <div class="popup-dialog" style="display: block;">
            <div class="cover-popup">
                <div class="popup-form" style=" background: url(images/travelling-background.jpg); max-width:600px;">
                    <h2>HÈ NÀY BẠN MUỐN ĐI ĐÂU?</h2>
                    <div class="text-center">
                        <p style="text-align: center;"><span style="color:#ff0000;"><span style="font-size: 18px;"><b>Chúng tôi sẵn sàng tư vấn</b></span></span></p>
                        <p style="text-align: center;"><span style="color:#000000;"><em>(Ưu đãi cực Sốc khi đi theo nhóm. Đăng ký ngay!)</em></span></p>
                    </div>
                    <div class="row">
                        <form enctype="multipart/form-data" class="w3f-form" role="form" id="form-popup" action="" method="post">
                            <div class="form-group w3-form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input class="input-text form-control" placeholder="Họ tên" type="text" value="" name="" id="">        
                                </div>
                            </div>
                            <div class="form-group w3-form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input class="input-text form-control" placeholder="Email" type="text" value="" name="" id="">     
                                </div>
                            </div>
                            <div class="form-group w3-form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input class="input-text form-control" placeholder="Điện thoại" type="text" value="" name="" id="">        
                                </div>
                            </div>
                            <div class="form-group w3-form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input class="input-text form-control" placeholder="Tour quan tâm" type="text" value="" name="" id="">       
                                </div>
                            </div>
                            <div class="w3-form-group">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                                    <button type="submit" class="">Đăng ký</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="shadow-background"></div>
        </div> --}}
@endsection
@section('script-index')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function(){
            let routeRenderTour = '/ajax/view-product';
            checkRender= false;
            $(document).on('scroll',function(){
                if($(window).scrollTop()>400 && checkRender == false){
                    //console.log('scroll');
                    checkRender = true;
                    let tours = localStorage.getItem('tours');
                    
                    tours = $.parseJSON(tours);
                    
                    //  console.log( Object.keys(tours).length);
                    // console.log(tours.lenght);
                    if(Object.keys(tours).length > 0 ){
                        $.ajax({
                            url: routeRenderTour,
                            method:"POST",
                            data:
                            {
                                id:tours
                            },
                            success:function(result){
                                //console.log(data);
                                $("#tour_view").html('').append(result.data);
                            }
                        });
                    }
                }
            });
        });
    </script>
@stop


