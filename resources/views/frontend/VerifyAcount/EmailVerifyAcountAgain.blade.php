@extends('frontend.master.master')
@section('name','Xác thực')
@section('content')
	   <div class="login-page-content-area">
    <div class="container">

        <div class="row">
            <div class="col-lg-12 col-md-12">

               @if(session('danger'))
                        <div class="alert alert-danger" role="alert">
                           vui lòng kiểm tra lại tài khoản email bạn vừa cung cấp. Có vẻ nó không đúng???

                        </div>
                    @endif
                    @if(session('success'))
                        <div class="alert alert-success" role="alert">
                           vui lòng kiểm tra email

                        </div>
                    @endif
                <div class="login-page-wrapper"><!-- login page wrapper -->
                    <div class="or hidden-xs hidden-sm">
                        <span>or</span>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                            <div class="left-content-area">
                                <div class="top-content">
                                    <h4 class="title">Chào mừng quay trở lại</h4>
                                </div>
                                <div class="bottom-content">
                                    <div class="left-content">
                                        <div class="thumb">
                                            <img src="asset/image/login-image.jpg" alt="login image">
                                        </div>
                                    </div>
                                    <div class="right-content">
                                        <ul>
                                            <li class="active">
                                                <a href="#">Đăng nhập lại</a>
                                            </li>
                                            <li>
                                                <a href="#">Xóa tài Khoản</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg- col-md-6 col-xs-12 col-sm-12">
                            <div class="right-contnet-area">
                                <div class="top-content">
                                    <h4 class="title">Xác thực tài khoản</h4>
                                </div>
                                <div class="bottom-content">
                                    <form action="" method="POST" class="login-form">
                                      @csrf
                                        <div class="form-element">
                                            <input type="email" name="email" class="input-field" placeholder="Nhập email của bạn để xác thực lại">
                                             {{showErrors($errors,'email')}}
                                        </div>

                                        <div class="btn-wrapper">
                                            <button type="submit" class="submit-btn-login">Xác nhận</button>
                                           
                                            
                                            
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
@endsection
