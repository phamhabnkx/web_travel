@extends('backend.master.master')

@section('name','Địa điểm du lịch')
@section('address','class=active')


@section('content')
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home">
							<use xlink:href="#stroked-home"></use>
						</svg></a></li>
				<li class="active">Icons</li>
			</ol>
		</div>
		<!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Quản lý danh mục</h1>
			</div>
		</div>
		<!--/.row-->


		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-body">
						<div class="row">
							<div class="col-md-5">
								<form  method="post">
									@csrf
									<div class="form-group">
									<label for="">Danh mục cha:</label>
									<select class="form-control" name="idParent" id="">
										<option value="0">----ROOT----</option>
										{{getAddress($address,0,'')}}
									</select>
								</div>
								<div class="form-group">
									<label for="">Tên Danh mục</label>
									<input type="text" class="form-control" name="name" id="" placeholder="Tên danh mục mới">
									 {{ showErrors($errors,'name') }}
									
								</div>
								<button type="submit" class="btn btn-primary">Thêm địa điểm</button>
								</form>
								
							</div>
							<div class="col-md-7">
								 @if(session('thongbao'))
		                            <div class="alert bg-success" role="alert">
											<svg class="glyph stroked checkmark">
												<use xlink:href="#stroked-checkmark"></use>
											</svg>Đã thêm thành công<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
										</div>
		                        @endif

		                         @if(session('thongbaoabc'))
		                            <div class="alert bg-success" role="alert">
											<svg class="glyph stroked checkmark">
												<use xlink:href="#stroked-checkmark"></use>
											</svg>Đã xóa thành công<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
										</div>
		                        @endif
								<h3 style="margin: 0;"><strong>Phân cấp Menu</strong></h3>
								<div class="vertical-menu">
									<div class="item-menu active">Danh mục </div>
									<div class="vertical-menu">
									<form action="" method="get" accept-charset="utf-8">
										{{getAddressdanhmuc($address,0,'')}}
									</form>

								</div>
							</div>
						</div>
					</div>
				</div>



			</div>
			<!--/.col-->


		</div>
		<!--/.row-->
	</div>
	<div class="modal fade" id="delete-modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Product</h4>
        </div>
        <div class="modal-body">
          <form action="" method="Post" accept-charset="utf-8">
              @csrf;
            <input type="hidden" value="delete" name="_method">
            <h4>Bạn có chắc chắn xóa không</h4>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
              <button type="submit" class="btn bg-red">Có</button>
            </div>
          </form>
        </div>
        
      </div>
      
    </div>
  </div>
<script>
    function onDelete(id) {
        $('#delete-modal form').attr('action','/admin/dia-diem/'+id);
        $('#delete-modal').modal('show');
    }
</script>
@endsection


