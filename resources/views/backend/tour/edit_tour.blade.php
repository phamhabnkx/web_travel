@extends('backend.master.master')
@section('name','Tour')
@section('tour','class=active')
@section('content')
{{-- @php
dd($tour->Tour_and_Phuong_tien())
@endphp --}}
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header" title="{{$tour->name}}">Sửa tour {{$tour->name}}</h1>
        </div>
    </div>
    <!--/.row-->
    <div class="row">
        <div class="col-xs-6 col-md-12 col-lg-12">
            @if(session('thongbao'))
            <div class="alert bg-success" role="alert">
                <svg class="glyph stroked checkmark">
                    <use xlink:href="#stroked-checkmark"></use>
                </svg>
                Đã sửathành công<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
            </div>
            @endif
            @if(session('abc'))
            <div class="alert bg-danger" role="alert">
                <svg class="glyph stroked checkmark">
                    <use xlink:href="#stroked-checkmark"></use>
                </svg>
                Hãy thêm địa điểm du lịch , nơi nghỉ, nhân viên du lịch hoặc phương tiện vận chuyển .Có thể bạn đã thiếu dữ liệu<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
            </div>
            @endif
            <div class="panel panel-primary">
                <form action="/admin/tour/{{$tour->id}}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="panel-body">
                        <div class="row" style="margin-bottom:40px">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <label>Mã tour</label>
                                    <input type="text" name="code" value="{{$tour->code}}" class="form-control">
                                    {{ showErrors($errors,'code') }}
                                </div>
                                <div class="form-group">
                                    <label>Tên tour</label>
                                    <input type="text" name="name" value="{{$tour->name}}" class="form-control">
                                    {{ showErrors($errors,'name') }}
                                </div>
                                <div class="form-group">
                                    <label>Ngày khởi hành</label>
                                    <input type="text" id="departureDateFilter"  name="ngay_khoi_hanh" value="{{$tour->ngay_khoi_hanh}}" class="form-control input-md hasDatepicker" autocomplete="off" value="" placeholder="Ngày khởi hành">
                                </div>
                                <div class="form-group">
                                    <label>Nơi khởi Hành</label>
                                    <select class="form-control" id="departureIDFilter" name="DepartureId">
                                        <option value="0">Nơi khởi hành</option>
                                        <option value="1">Hồ Chí Minh</option>
                                        <option value="32">Bạc Liêu</option>
                                        <option value="33">Bảo Lộc</option>
                                        <option value="7">Bình Dương</option>
                                        <option value="18">Buôn Ma Thuột</option>
                                        <option value="20">Cà Mau</option>
                                        <option value="5">Cần Thơ</option>
                                        <option value="24">Đà Lạt</option>
                                        <option value="4">Đà Nẵng</option>
                                        <option value="12">Đồng Nai</option>
                                        <option value="34">Đồng Tháp</option>
                                        <option value="3">Hà Nội</option>
                                        <option value="6">Hải Phòng</option>
                                        <option value="10">Huế</option>
                                        <option value="39">Long An</option>
                                        <option value="14">Long Xuyên</option>
                                        <option value="17">Quảng Ninh</option>
                                        <option value="8">Nha Trang</option>
                                        <option value="13">Phú Quốc</option>
                                        <option value="30">Quảng Bình</option>
                                        <option value="15">Quảng Ngãi</option>
                                        <option value="11">Quy Nhơn</option>
                                        <option value="22">Rạch Giá</option>
                                        <option value="35">Sóc Trăng</option>
                                        <option value="40">Thái Nguyên</option>
                                        <option value="16">Vũng Tàu</option>
                                    </select>
                                </div>
                                <label>Địa điểm du lịch</label>
                                <div style="margin-bottom: 10px;" class="form-group">
                                    <select class="form-control" id="" name="address_id">
                                    {{getEditAddresstour($address,0,'',$tour->address_id)}}
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Số chỗ nhận</label>
                                    <input type="number"  name="so_luong_tour" value="{{$tour->so_luong_tour}}" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>số ngày-đêm</label>
                                    <input type="text"  name="so_ngay" class="form-control" value="{{$tour->so_ngay}}" placeholder="vd: 3 ngay 4 đêm" >
                                </div>
                                <div class="form-group">
                                    <label>Hướng dẫn viên</label>
                                    <select class="form-control" id="" name="nhan_vien">
                                    @foreach ($guide as $element)
                                    <option @if(check_value_nv($tour,$element->id)) selected @endif  value="{{$element->id}}">{{$element->full}}</option>
                                    @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Dòng tour</label>
                                    <select class="form-control" name="dong_tour" id="">
                                    <option @if ($tour->dong_tour == 1) selected @endif value="1">----ROOT----</option>
                                    <option @if ($tour->dong_tour == 2) selected @endif value="2"> Dòng tiết kiệm</option>
                                    <option @if ($tour->dong_tour == 3) selected @endif value="3">Dòng tiêu chuẩn</option>
                                    <option @if ($tour->dong_tour == 4) selected @endif value="4">Dòng cao cấp</option>
                                    </select>
                                    <div class="form-group">
                                        <label>Trạng Thái</label>
                                        <select class="form-control" name="trang_thai" id="">
                                        <option @if ($tour->trang_thai == 1) selected @endif value="1">Đang tổ chức</option>
                                        <option @if ($tour->trang_thai == 2) selected @endif value="2"> Đã hết hạn</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Loại tour</label>
                                        <select class="form-control" name="loai_tour" id="">
                                        <option @if ($tour->loai_tour == 1) selected @endif value="1">Trong nước</option>
                                        <option @if ($tour->loai_tour == 2) selected @endif value="2"> Ngoài nước</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Giá tour</label>
                                        <input type="number" name="price" value="{{$tour->price}}" class="form-control">
                                        {{ showErrors($errors,'price') }}
                                    </div>
                                    <label>Giá tour riêng lẻ</label>
                                    <div>
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr class="bold">
                                                    <td style="color: #000;">Loại khách</td>
                                                    <td style="color: #000;">Giá tour</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td data-title="Loại khách">Người lớn (Từ 12 tuổi trở lên)</td>
                                                    <td data-title="Giá tour"><input class="form-control" type="text" name="gia_rieng_le[]" value="{{$gia_rieng_le[0]}}" placeholder=""></td>
                                                </tr>
                                                <tr>
                                                    <td data-title="Loại khách">Trẻ em (Từ 5 tuổi đến dưới 12 tuổi)</td>
                                                    <td data-title="Giá tour"><input class="form-control" type="text" name="gia_rieng_le[]" value="{{$gia_rieng_le[1]}}" placeholder=""></td>
                                                </tr>
                                                <tr>
                                                    <td data-title="Loại khách">Trẻ nhỏ (Từ 2 tuổi đến dưới 5 tuổi)</td>
                                                    <td data-title="Giá tour"><input class="form-control" type="text" name="gia_rieng_le[]" value="{{$gia_rieng_le[2]}}" placeholder=""></td>
                                                </tr>
                                                <tr>
                                                    <td data-title="Loại khách">Em bé (Dưới 2 tuổi)</td>
                                                    <td data-title="Giá tour"><input class="form-control" type="text" name="gia_rieng_le[]" value="{{$gia_rieng_le[3]}}" placeholder=""></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="form-group">
                                        <label>Thời gian tập trung</label>
                                        <input size="16" type="text" value="2020-12-17 14:45" name="time_tap_trung" value="{{$tour->time_tap_trung}}" readonly class="form_datetime form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Địa điểm tập trung</label>
                                        <input type="text" name="dia_diem_tap_trung" value="{{$tour->dia_diem_tap_trung}}" class="form-control">
                                    </div>
                                    <label>Thông tin vận chuyển</label>
                                    <div style="margin-bottom: 10px;" class="form-group">
                                        @foreach ($phuongTien as $element)
                                        <div class="xe_phuong_tien"><input @if(check_value($tour,$element->id)) checked @endif type="checkbox" value="{{$element->id}}" name="phuong_tien_id[]" > {{$element->pt_name}} + {{$element->pt_cho}} chỗ</div>
                                        @endforeach
                                    </div>
                                    <label>Thông tin nơi nghỉ</label>
                                    <div style="margin-bottom: 10px;" class="form-group">
                                        @foreach ($hotel as $element)
                                        <div class="xe_phuong_tien">
                                            <input @if(check_value_hotel($tour,$element->id)) checked @endif type="checkbox" name="noi_nghi_id[]" value="{{$element->id}}"  >{{$element->ks_name}}
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                               <div class="form-group">
                                    <label>Ảnh nổi bật</label>
                                    <input id="img" type="file" name="avatar"  class="form-control hidden"
                                        onchange="changeImg(this)">
                                    <img id="avatar" class="thumbnail" width="100%" height="350px" src="{{$tour->avatar}}">
                                    {{ showErrors($errors,'avatar') }}
                                </div>
                                <div class="form-group">
                                    <label>Ảnh slide</label>
                                    <input type="file" name="img[]" multiple class="form-control">
                                   
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Miêu tả</label>
                                    <textarea  name="describe" style="width: 100%;height: 100px;">{{$tour->describe}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Ghi chú</label>
                                    <textarea  name="ghi_chu" style="width: 100%;height: 100px;">{{$tour->ghi_chu}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label>Lưu ý</label>
                                    <textarea id="editor" name="luu_y" style="width: 100%;height: 100px;">{{$tour->luu_y}}</textarea>
                                </div>
                                <div style="margin-bottom: 10px;" style="color: #000">
                                    <input type="checkbox" @if($tour->featured ==1 ) checked @endif value="1"   name="featured" >  Tour này có phải là tour nổi bật
                                </div>
                                <div>
                                    <h2 style="color: #000;">Chương trình tour</h2>
                                    <div class="giu_or_xoa">
                                        @if ($tour->quantity !=0)
                                        @for($i=0; $i<$tour->quantity;$i++)
                                        <div class="form-group">
                                            <label>Tên chương trình ngày {{$i+1}}</label>
                                            <input type="text" name="ten_chuong_trinh[]" value="{{$ten_chuong_trinh[$i]}}" class="form-control" placeholder="">   
                                        </div>
                                        <div class="form-group">
                                            <label>Ngày-tháng-năm</label>
                                            <input type="text"  name="chuong_trinh_tour_ngay[]" value="{{$chuong_trinh_tour_ngay[$i]}}" class="form-control input-md hasDatepicker chuong_trinh_tour" autocomplete="off" value="" placeholder="17/12/2019">
                                        </div>
                                        <div class="form-group">
                                            <label>Mô tả chi tiết</label>
                                            <textarea  name="content[]" id="editore{{$i}}" class="abcd" style="width: 100%;height: 100px;">{!!$content[$i]!!}</textarea>
                                        </div>
                                        @endfor
                                        @endif
                                    </div>
                                    <input type="text" class="form-control" onchange="Quantum(this)" name="quantum" value="" placeholder="số ngày">
                                    <div style="padding-top: 10px" class="banner">
                                    </div>
                                </div>
                                <button class="btn btn-success"  type="submit">Cập nhật</button>
                                <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--/.row-->
</div>
@endsection
@section('chart-app-data')
<script>
    function Quantum(obj) {
        $('.giu_or_xoa').remove();
        
        var i = obj.value;  
        var data = new Array();     
        for(var j=1; j<=i; j++){
            data[j] = '<div class="form-group"><label>Tên chương trình ngày '+j+
                       ' </label><input type="text" name="ten_chuong_trinh[]" value="" class="form-control" placeholder=""></div><div class="form-group"><label>Ngày-tháng-năm</label><input type=""  name="chuong_trinh_tour_ngay[]" id="" class="chuong_trinh_tour form-control input-md hasDatepicker " autocomplete="off" value="" placeholder="17/12/2019"></div><divclass="form-group"><label>Mô tả chi tiết '+j+'</label><textarea id="editor'+j+'"  name="content[]" class="abc" style="width: 100%;height: 100px;"></textarea></div>';
    
        }  
        $('.banner').html(
            data); 
        for(var j=1; j<=i; j++){
            $("textarea.abc").each(function(){
                
                 CKEDITOR.replace('editor'+j+'' , {
                   filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',
                 });
               }); 
             }
             $('.chuong_trinh_tour').datepicker({
                  'format': 'dd-mm-yyyy',
                  'autoclose': true
              });
        }
    
    
    $(document).ready(function() {
       $('#departureDateFilter').datepicker({
          'format': 'dd-mm-yyyy',
          'autoclose': true
      });
       
        $('.thoi_gian_tap_trung').datepicker({
          'format': 'dd-mm-yyyy',
          'autoclose': true
      });
        $('.chuong_trinh_tour').datepicker({
          'format': 'dd-mm-yyyy',
          'autoclose': true
      });
        $(".form_datetime").datetimepicker({
            format: 'yyyy-mm-dd hh:ii',
             
        });
        
       
    });  
    
             
             $('.chuong_trinh_tour').datepicker({
                  'format': 'dd-mm-yyyy',
                  'autoclose': true
              });
    
        
    
    
    
    
    
</script>
@stop