<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
		<form role="search">
		</form>
        <ul class="nav menu">
			<li @yield('user')><a href="/admin"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg> Tổng quan</a></li>
			<li @yield('address')><a href="/admin/dia-diem"><img src="img/i-marker.png" style="margin-right: 8px" alt=""> Địa Điểm</a></li>
			
			<li @yield('hotel')><a href="/admin/hotel"><img src="img/i-hotel.png" style="margin-right: 8px" alt=""> Nơi nghỉ</a></li>
			
			<li @yield('guide')><a href="/admin/guide"><img src="img/i-hdv.png" style="margin-right: 8px" alt=""> Hướng dẫn Viên</a></li>
			<li @yield('phuong-tien')><a href="/admin/phuong-tien"><img src="img/i-plane.png" style="margin-right: 8px" alt=""> Phương tiện</a></li>
			<li @yield('tour')><a href="/admin/tour"><img src="img/i-tourprice.png" style="margin-right: 8px" alt="">  Tour</a></li>
			<li @yield('order')><a href="/admin/order"><img src="img/i-note.png" style="margin-right: 8px" alt=""> Danh sách khách hàng</a></li>
			<li @yield('chi-nhanh')><a href="/admin/chi-nhanh"><img src="img/i-marker.png" style="margin-right: 8px" alt="">  Chi nhánh</a></li>
			<li @yield('option')><a href="/admin/option"><svg class="glyph stroked notepad " style="color: red"><use xlink:href="#stroked-notepad" /></svg> Tùy chọn</a></li>
			<li @yield('blog')><a href="/admin/blog"><img src="img/i-note.png" style="margin-right: 8px" alt=""> Blog</a></li>
			<li @yield('blog-details')><a href="/admin/blog-details"><svg class="glyph stroked notepad "><use xlink:href="#stroked-notepad" /></svg> Bài viết chi tiết</a></li>
			<li role="presentation" class="divider"></li>
			@if (Auth::user()->level == 1)
				<li @yield('user')><a href="/admin/user"><img src="img/i-hdv.png" style="margin-right: 8px" alt=""> Quản lý thành viên</a></li>
			@endif
			
		
		</ul>

	</div>