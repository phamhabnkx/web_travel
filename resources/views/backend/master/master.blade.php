<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('name')</title>
	<!-- css -->
	<base href="{{asset('backend')}}/" target="_blank, _self, _parent, _top">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="lib/bootstrap-datepicker.css" >
    <link rel="stylesheet" type="text/css" href="datetimepicker/css/bootstrap-datetimepicker.css" >
	<link href="css/styles.css" rel="stylesheet">
	<!--Icons-->
	<script src="js/lumino.glyphs.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>
<body>
	<!-- header -->
	@include('backend.master.nav')
	<!-- header -->
	<!-- sidebar left-->
	@include('backend.master.sidebar')
	<!--/. end sidebar left-->

	<!--main-->
	@yield('content')
	<!--end main-->

	<!-- javascript -->
	
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/chart-data.js"></script>
	<script type="text/javascript" src="lib/bootstrap-datepicker.js"></script>
    <script type="text/javascript" src="datetimepicker/js/bootstrap-datetimepicker.js"></script>
	<script src="js/app.js"></script>
	@yield('char-data')
	@yield('chart-app-data')
	<script src={{ url('ckeditor/ckeditor.js') }}></script>
    <script>
  
    CKEDITOR.replace( "editor", {
        filebrowserBrowseUrl: '{{ route('ckfinder_browser') }}',

    } );
    </script>
    @include('ckfinder::setup')
</body>

</html>