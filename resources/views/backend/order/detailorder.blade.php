@extends('backend.master.master')

@section('name','detailOrder')
@section('order','class=active')


@section('content')
		<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home">
							<use xlink:href="#stroked-home"></use>
						</svg></a></li>
				<li class="active">Khách hàng / Chi tiết </li>
			</ol>
		</div>
		<!--/.row-->
		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">

				<div class="panel panel-primary">
					
					<div class="panel-body">
						<div class="bootstrap-table">
							<div class="table-responsive">
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<div class="panel panel-blue" >
												<div class="panel-heading dark-overlay">Thông tin khách hàng</div>
												<div class="panel-body">
													<strong><span class="glyphicon glyphicon-user" aria-hidden="true" style="margin-right: 10px"></span> : {{$data->full}}</strong> <br>
													<strong><span class="glyphicon glyphicon-phone" aria-hidden="true" style="margin-right: 10px"></span> : Số điện thoại: {{$data->phone}}</strong>
													<br>
													<strong><span class="glyphicon glyphicon-send" aria-hidden="true" style="margin-right: 10px"></span> :{{$data->address}}</strong>
												</div>
											</div>
										</div>
									</div>


								</div>
								<table class="table table-bordered" style="margin-top:20px;">
									<thead>
										<tr class="bg-primary">
											<th>ID</th>
											<th>Thông tin tour</th>
											<th>Giá tour</th>
											<th>Thành tiền</th>

										</tr>
									</thead>
									<tbody>
										@foreach($order as $orders)
										<tr>
											<td>{{$orders->id}}</td>
											<td>
												<div class="row">
													<div class="col-md-4">
														<img width="100px" src="{{$orders->img}}" class="thumbnail">
													</div>
													<div class="col-md-8">
														<p><b>Mã sản phẩm</b>: {{$orders->code}}</p>
														<p><b>Tên Sản phẩm</b>: {{$orders->name}}</p>
														<p><b>Số lương</b> : {{$orders->quantity}}</p>
													</div>
												</div>
											</td>
											<td>{{number_format($orders->price,0,"",".")}}VNĐ</td>
											<td>{{number_format($orders->price*$orders->quantity,0,"",".")}} VNĐ</td>

										</tr>
										@endforeach

									
									</tbody>

								</table>
								<table class="table">
									<thead>
										<tr>
											<th width='70%'>
												<h4 align='right'>Tổng Tiền :</h4>
											</th>
											<th>
												<h4 align='right' style="color: brown;">{{number_format($data->total,0,"",".")}} VNĐ</h4>
											</th>

										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
								<div class="alert alert-primary" role="alert" align='right'>
									<a name="" id="" class="btn btn-success"  onclick="onDelete({{$data->id}})" href="javascript:void(0)" role="button">Xác nhận</a>

									<a name="" id="" class="btn btn-danger"  onclick="onDeleteHuy({{$data->id}},{{$orders->id}})" href="javascript:void(0)" role="button">Hủy đơn</a>
								</div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<!--/.row-->


	</div>
<div class="modal fade" id="delete-modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Xác nhận đơn hàng</h4>
        </div>
        <div class="modal-body">
          <form action="" method="get" accept-charset="utf-8">
              @csrf
           
            <h4>Are you sure</h4>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-danger">Confirm</button>
            </div>
          </form>
        </div>
        
      </div>
      
    </div>
 </div>

 <div class="modal fade" id="huy-don" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Hủy đơn hàng</h4>
        </div>
        <div class="modal-body">
          <form action="" method="get" accept-charset="utf-8">
              @csrf
           
            <h4>Bạn đã xác nhận với khách hàng và họ muốn hủy đơn</h4>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-danger">Confirm</button>
            </div>
          </form>
        </div>
        
      </div>
      
    </div>
 </div>
<script>
    function onDelete(id) {
        $('#delete-modal form').attr('action','/admin/order/pay/'+id);
        $('#delete-modal').modal('show');
    }

    function onDeleteHuy(id,x) {
        $('#huy-don form').attr('action','/admin/order/delete/'+id+'/'+x);
        $('#huy-don').modal('show');
    }
</script>

@endsection
