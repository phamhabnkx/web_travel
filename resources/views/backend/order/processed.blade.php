@extends('backend.master.master')

@section('name','processed')
@section('order','class=active')


@section('content')
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home">
							<use xlink:href="#stroked-home"></use>
						</svg></a></li>
				<li class="active">Khách hàng</li>
			</ol>
		</div>
		<!--/.row-->
		<div class="row">
			<div class="col-xs-12 col-md-12 col-lg-12">

				<div class="panel panel-primary">
					
					<div class="panel-body">
						<div class="bootstrap-table">
							<div class="table-responsive">
								<a href="/admin/order" class="btn btn-warning"><span class="glyphicon glyphicon-gift"></span>Khách hàng Chưa xử lý</a>
								<table class="table table-bordered" style="margin-top:20px;">				
                                    <thead>
                                        <tr class="bg-primary">
                                            <th>ID</th>
                                            <th>Tên khách hàng</th>
                                            <th>Email</th>
                                            <th>Sđt</th>
                                            <th>Địa chỉ</th>
                                            <th>Thời gian</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         @foreach($orders as $order)
                                            <tr>
                                                <td>{{$order->id}}</td>
                                                <td>{{$order->full}}</td>
                                                <td>{{$order->email}}</td>
                                                <td>{{$order->phone}}</td>
                                                <td>{{$order->address}}</td>
                                                @php
                                                    $time=Carbon\Carbon::parse($order->updated_at);
                                                @endphp
                                                <td>{{date('H:i d-m-Y', strtotime($time))}}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
							</div>
						</div>
                        <div align='right'>
                                    <nav aria-label="Page navigation example">
                                        {{$orders->links('vendor.pagination.default')}}
                                    </nav>
                                </div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
		<!--/.row-->


	</div>
@endsection














