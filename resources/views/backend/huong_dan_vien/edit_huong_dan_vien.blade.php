@extends('backend.master.master')

@section('name','Hướng dẫn viên')
@section('guide','class=active')
	
			

@section('content')
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Sửa thông tin {{$guide->full}}</h1>
            </div>
        </div>
        <!--/.row-->
    <div class="row">
        @if(session('thongbao'))
                    <div class="alert bg-success" role="alert">
                            <svg class="glyph stroked checkmark">
                                <use xlink:href="#stroked-checkmark"></use>
                            </svg>Đã sửa thành công<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        </div>
                @endif
        <div class="col-xs-12 col-md-12 col-lg-12">

            <form action="/admin/guide/{{$guide->id}}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                 <div class="panel panel-primary">
                   
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">

                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                             
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" name="email" value="{{$guide->email}}" class="form-control">
                                   {{showErrors($errors,'email')}}
                                  
                                </div>
                               
                                <div class="form-group">
                                    <label>Full name</label>
                                    <input type="full" name="full" value="{{$guide->full}}" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input type="address" name="address" value="{{$guide->address}}" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="phone" name="phone" value="{{$guide->phone}}" class="form-control">
                                </div>
                              
                               
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right">
                                  
                                    <button class="btn btn-success"  type="submit">Cập nhật</button>
                                    <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                </div>
                            </div>
                           

                        </div>
                    
                        <div class="clearfix"></div>
                    </div>
                </div>
            </form>
               

        </div>
    </div>

        <!--/.row-->
    </div>
		
@endsection


