<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Login</title>
	<base href="{{ asset('backend') }}/" target="_blank, _self, _parent, _top">
	<link href="css/bootstrap.min.css" rel="stylesheet">

	
	<link href="css/styles.css" rel="stylesheet">
</head>

<body>

	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">Log in</div>
				 @if(session('success'))
                    <div class="alert bg-success" role="alert">
                            <svg class="glyph stroked checkmark">
                                <use xlink:href="#stroked-checkmark"></use>
                            </svg>Bạn đã thay đổi mật khẩu thành công<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        </div>
                @endif
				@if(session('danger'))
                    <div class="alert bg-success" role="alert">
                            <svg class="glyph stroked checkmark">
                                <use xlink:href="#stroked-checkmark"></use>
                            </svg>Vui lòng thử lại<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        </div>
                @endif
				<div class="panel-body">
					<form role="form" method="post">
						@csrf
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="E-mail" name="email" type="email" value="{{old('email')}}" autofocus="">
								{{showErrors($errors,'email')}}
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password" name="password" type="password" value="">
								{{showErrors($errors,'password')}}
							</div>
							<div class="checkbox">
								<label>

									<input name="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}>{{ __('Remember Me') }}

								</label>
								<a href="/admin/forgotPassword" style="color: red; float: right;">Quên mật khẩu ?</a>
							</div>
							<button type="submit" class="btn btn-primary">submit</button>
						</fieldset>
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->
</body>

</html>