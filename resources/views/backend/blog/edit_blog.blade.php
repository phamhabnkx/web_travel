@extends('backend.master.master')

@section('name','Tin tức')
@section('blog','class=active')


@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"><svg class="glyph stroked home">
                            <use xlink:href="#stroked-home"></use>
                        </svg></a></li>
                <li class="active">Icons</li>
            </ol>
        </div>
        <!--/.row-->

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Sửa danh mục blog</h1>
            </div>
        </div>
        <!--/.row-->


        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                             @if(session('thongbao'))
                                    <div class="alert bg-success" role="alert">
                                            <svg class="glyph stroked checkmark">
                                                <use xlink:href="#stroked-checkmark"></use>
                                            </svg>Đã cập nhật thành công<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                        </div>
                                @endif
                            <form action="/admin/blog/{{$data->id}}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                                @csrf
                                @method('PUT')
                                 <div class="col-md-5">

                                    <div class="form-group">
                                        <label for="">Tên Danh mục Blog</label>
                                        <input type="text" class="form-control" name="name" id="" placeholder="Tên danh mục mới" value="{{$data->name}}">
                                        {{ showErrors($errors,'name') }}
                                    </div>
                                    <button type="submit" class="btn btn-primary">Cập nhật </button>
                                </div>
                            </form>
                           
                           
                        </div>
                    </div>
                </div>



            </div>
            <!--/.col-->


        </div>
        <!--/.row-->
    </div>
	 
@endsection