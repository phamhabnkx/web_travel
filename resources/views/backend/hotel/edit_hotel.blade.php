@extends('backend.master.master')

@section('name','hotel')
@section('hotel','class=active')


@section('content')
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <div class="col-lg-12">
                
            </div>
        </div>
        <!--/.row-->
        <div class="row">
            <div class="col-xs-6 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">sửa Khách sạn</div>


                    @if(session('thongbao'))
                    <div class="alert bg-success" role="alert">
                            <svg class="glyph stroked checkmark">
                                <use xlink:href="#stroked-checkmark"></use>
                            </svg>Đã sửa thành công<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        </div>
                @endif
                    <form action="/admin/hotel/edit/{{$hotel->id}}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        @csrf
                        
                        <div class="panel-body">
                        <div class="row" style="margin-bottom:40px">
                            <div class="col-xs-8">
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label>Tiêu chuẩn Khách sạn</label>
                                            <select name="tieu_chuan" class="form-control">
                                               
                                                <option @if($hotel->tieu_chuan == 1) selected @endif value="1">1 sao</option>
                                                <option @if($hotel->tieu_chuan == 2) selected @endif value="2">2 sao</option>
                                                <option @if($hotel->tieu_chuan == 3) selected @endif value="3">3 sao</option>
                                                <option @if($hotel->tieu_chuan == 4) selected @endif value="4">4 sao</option>
                                                <option @if($hotel->tieu_chuan == 5) selected @endif value="5">5 sao</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Mã Nơi nghỉ ngơi</label>
                                            
                                            <input  type="text" value="{{$hotel->ks_code}}" name="ks_code" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Tên Nơi nghỉ ngơi</label>
                                            <input  type="text" value="{{$hotel->ks_name}}" name="ks_name" class="form-control">
                                        </div>
                                         <div class="form-group">
                                            <label>Tình trạng</label>
                                            <input  type="text" value="{{$hotel->ks_tinh_trang}}" name="ks_tinh_trang" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Giá sản phẩm (Giá chung)</label>
                                            <input  type="number" value="{{$hotel->ks_price}}" name="ks_price" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Địa chỉ</label>
                                            <input  type="text" value="{{$hotel->ks_address}}" name="ks_address" class="form-control">
                                        </div>
                                     
                                    </div>
                                    <div class="col-md-5">
                                         <div class="form-group">
                                        <label>Ảnh </label>
                                        <input id="img" type="file" name="avatar" value="{{$hotel->avatar}}" class="form-control hidden"
                                            onchange="changeImg(this)">
                                        <img id="avatar" class="thumbnail" width="40%" height="200px" src="{{$hotel->avatar}}">
                                         
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Thông tin</label>
                                    <textarea  value="{{$hotel->info}}" name="info" style="width: 100%;height: 100px;"></textarea>
                                </div>

                            </div>
                           
                        </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Miêu tả</label>
                                        <textarea   value="{{$hotel->description}}" name="description" style="width: 100%;height: 100px;"></textarea>
                                    </div>
                                    <button class="btn btn-success"  type="submit">cập nhật</button>
                                    <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                </div>
                            </div>
                        <div class="clearfix"></div>
                    </div>
                    </form>
                    
                </div>

            </div>
        </div>

        <!--/.row-->
    </div>
         
     
@endsection