@extends('backend.master.master')

@section('name','hotel')
@section('hotel','class=active')


@section('content')
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Thêm Khách sạn</h1>
            </div>
        </div>
        <!--/.row-->
        <div class="row">
            <div class="col-xs-6 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">Thêm Khách sạn</div>
                    @if(session('thongbao'))
                    <div class="alert bg-success" role="alert">
                            <svg class="glyph stroked checkmark">
                                <use xlink:href="#stroked-checkmark"></use>
                            </svg>Đã thêm thành công<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        </div>
                @endif
                    <form action="" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        @csrf
                        <div class="panel-body">
                        <div class="row" style="margin-bottom:40px">
                            <div class="col-xs-8">
                                <div class="row">
                                    <div class="col-md-7">
                                        <div class="form-group">
                                            <label>Tiêu chuẩn Khách sạn</label>
                                            <select name="tieu_chuan" class="form-control">
                                                <option value='1' selected>1 sao</option>
                                                <option value='2'>2 sao</option>
                                                <option value='3'>3 sao</option>
                                                <option value='4'>4 sao</option>
                                                <option value='4'>5 sao</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Mã Nơi nghỉ ngơi</label>
                                            <input  type="text" name="ks_code" class="form-control">
                                            {{ showErrors($errors,'ks_code') }}
                                        </div>
                                        <div class="form-group">
                                            <label>Tên Nơi nghỉ ngơi</label>
                                            <input  type="text" name="ks_name" class="form-control">
                                            {{ showErrors($errors,'ks_name') }}
                                        </div>
                                         <div class="form-group">
                                            <label>Tình trạng</label>
                                            <input  type="text" name="ks_tinh_trang" class="form-control">
                                            {{ showErrors($errors,'ks_tinh_trang') }}
                                        </div>
                                        <div class="form-group">
                                            <label>Giá sản phẩm (Giá chung)</label>
                                            <input  type="number" name="ks_price" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Địa chỉ</label>
                                            <input  type="text" name="ks_address" class="form-control">
                                            {{ showErrors($errors,'ks_address') }}
                                        </div>
                                     
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>Ảnh</label>
                                            <input id="img" type="file" name="avatar" class="form-control hidden"
                                                onchange="changeImg(this)">
                                            <img id="avatar" class="thumbnail" width="100%" height="350px" src="img/import-img.png">
                                            {{ showErrors($errors,'avatar') }}
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Thông tin</label>
                                    <textarea  name="info" style="width: 100%;height: 100px;"></textarea>
                                </div>

                            </div>
                           
                        </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Miêu tả</label>
                                        <textarea   name="description" style="width: 100%;height: 100px;"></textarea>
                                    </div>
                                    <button class="btn btn-success" type="submit">Thêm</button>
                                    <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                </div>
                            </div>
                        <div class="clearfix"></div>
                    </div>
                    </form>
                    
                </div>

            </div>
        </div>

        <!--/.row-->
    </div>
         
	 
@endsection