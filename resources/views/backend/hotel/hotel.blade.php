@extends('backend.master.master')

@section('name','hotel')
@section('hotel','class=active')


@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"><svg class="glyph stroked home">
                            <use xlink:href="#stroked-home"></use>
                        </svg></a></li>
                <li class="active">Danh sách khách sạn</li>
            </ol>
        </div>
        <!--/.row-->

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Danh sách khách sạn</h1>
            </div>
        </div>
        <!--/.row-->

        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">

                <div class="panel panel-primary">

                    <div class="panel-body">
                        <div class="bootstrap-table">
                            <div class="table-responsive">
                                @if(session('thongbaoabc'))
                                    <div class="alert bg-success" role="alert">
                                            <svg class="glyph stroked checkmark">
                                                <use xlink:href="#stroked-checkmark"></use>
                                            </svg>Đã xóa thành công<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                        </div>
                                @endif
                                <a href="/admin/hotel/add" class="btn btn-primary">Thêm khách sạn</a>
                                <table class="table table-bordered" style="margin-top:20px;">

                                    <thead>
                                        <tr class="bg-primary">
                                            <th>ID</th>
                                            <th>Thông tin khách sạn</th>
                                            <th>Tình trạng</th>
                                            
                                            
                                            
                                            <th>Tùy chọn</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        
                                        
                                        @foreach ($hotel as $element)
                                            <tr>
                                            <td>1</td>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-3"><img src="{{$element->avatar}}" alt="{{$element->slug}}" width="100px" class="thumbnail"></div>
                                                    <div class="col-md-9">
                                                        <p><strong>Mã khách sạn : {{$element->ks_code}}</strong></p>
                                                        <p>Tên khách sạn : {{$element->ks_name}}</p>
                                                        <p>Địa chỉ : {{$element->ks_address}}</p>
                                                       
                                                    

                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <a name="" id="" class="btn btn-success" href="#" role="button">{{$element->ks_tinh_trang}}</a>
                                            </td>
                                        
                                            
                                            <td>
                                                <a href="/admin/hotel/edit/{{$element->id}}" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>
                                                <a onclick="onDelete({{$element->id}})" href="javascript:void(0)"  class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                       
                                    


                                    </tbody>
                                </table>
                               <div align='right'>
                                    <nav aria-label="Page navigation example">
                                        {{$hotel->links('vendor.pagination.default')}}
                                    </nav>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                </div>
                <!--/.row-->


            </div>
            <!--end main-->

            <!-- javascript -->
            <div class="modal fade" id="delete-modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Product</h4>
        </div>
        <div class="modal-body">
          <form action="" method="get" accept-charset="utf-8">
              @csrf;
            @method('Delete')
            <h4>Bạn có chắc chắn xóa không</h4>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
              <button type="submit" class="btn bg-red">Có</button>
            </div>
          </form>
        </div>
        
      </div>
      
    </div>
  </div>
<script>
    function onDelete(id) {
        $('#delete-modal form').attr('action','/admin/hotel/del/'+id);
        $('#delete-modal').modal('show');
    }
</script>

         
	 
@endsection