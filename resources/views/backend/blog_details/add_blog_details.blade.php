@extends('backend.master.master')

@section('name','Bài viết chi tiết')
@section('blog-details','class=active')


@section('content')
	  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Thêm bài viết chi tiết</h1>
            </div>
        </div>
        <!--/.row-->
        <div class="row">
            <div class="col-xs-6 col-md-12 col-lg-12">
                  @if(session('thongbao'))
                                    <div class="alert bg-success" role="alert">
                                            <svg class="glyph stroked checkmark">
                                                <use xlink:href="#stroked-checkmark"></use>
                                            </svg>Đã thêm thành công<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                        </div>
                                @endif
                <div class="panel panel-primary">
                    <form action="/admin/blog-details" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        @csrf
                         <div class="panel-body">
                        <div class="row" style="margin-bottom:40px">
                            <div class="col-xs-8">
                                <div class="row">
                                    <div class="col-md-7">
                                        
                                        <div class="form-group">
                                            <label>Mã bài viết</label>
                                            <input required type="text" name="bai_viet_code" class="form-control">
                                            {{ showErrors($errors,'bai_viet_code') }}
                                        </div>
                                        <div class="form-group">
                                            <label>Tên bài viết</label>
                                            <input required type="text" name="name" class="form-control">
                                            {{ showErrors($errors,'name') }}
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Thể loại</label>
                                           <select name="the_loai" class="form-control">
                                            @foreach ($blog as $element)
                                                <option value='{{$element->id}}'> {{$element->name}}</option>
                                            @endforeach
                                                
                                              
                                                
                                            </select>
                                        </div>
                                         
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>Ảnh Bài viết</label>
                                            <input id="img" type="file" name="bai_viet_img" class="form-control hidden"
                                                onchange="changeImg(this)">
                                            <img id="avatar" class="thumbnail" width="100%" height="350px" src="img/import-img.png">
                                        </div>
                                    </div>
                                </div>
                               

                            </div>
                           
                        </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nội dung</label>
                                        <textarea id="editor" required name="content" style="width: 100%;height: 100px;"></textarea>
                                        {{ showErrors($errors,'content') }}
                                    </div>
                                    <button class="btn btn-success" type="submit">Thêm</button>
                                    <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                </div>
                            </div>
                        <div class="clearfix"></div>
                    </div>
                        
                    </form>
                   
                </div>

            </div>
        </div>

        <!--/.row-->
    </div>
           
@endsection