@extends('backend.master.master')

@section('name','Trụ sở chính và chí nhánh')
@section('chi-nhanh','class=active')


@section('content')
	  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"><svg class="glyph stroked home">
                            <use xlink:href="#stroked-home"></use>
                        </svg></a></li>
                <li class="active">Địa chỉ công ty và chi nhánh</li>
            </ol>
        </div>
        <!--/.row-->

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Danh sách địa chỉ công ty và chi nhánh</h1>
            </div>
        </div>
        <!--/.row-->

        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">

                <div class="panel panel-primary">

                    <div class="panel-body">
                        <div class="bootstrap-table">
                            <div class="table-responsive">
                                @if(session('thongbaoabc'))
                                    <div class="alert bg-success" role="alert">
                                            <svg class="glyph stroked checkmark">
                                                <use xlink:href="#stroked-checkmark"></use>
                                            </svg>Đã xóa thành công<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                        </div>
                                @endif
                                 <a href="/admin/chi-nhanh/create" class="btn btn-primary">Thêm chi nhánh</a>
                                <div class="tru_so_chinh">
                                   <div class=" tru_so_title">
                                        <h3>Trụ sở chính</h3>
                                    </div>
                                   
                                    @foreach ($chiNhanh as $element)

                                        @if ($element->featured == 1)
                                            <div class=" mg-bot30">
                                                <p class="address mg-bot10"><strong>Địa chỉ: </strong> {{$element->address}}</p>
                                                <p class="mg-bot10"><strong>Điện thoại:</strong> {{$element->phone}}</p>
                                                <p class="mg-bot10"><strong>Fax:</strong> {{$element->fax}}</p>
                                                <p><strong>Email:</strong> {{$element->email}}</p>
                                            </div> 
                                        @endif
                                    @endforeach
                                    
                                </div>
                                

                               
                                <table class="table table-bordered" style="margin-top:20px;">

                                    <thead>
                                        <tr class="bg-primary">
                                            <th>ID</th>
                                            <th>Thông tin chi nhánh</th>
                                            
                                            
                                            
                                            
                                            <th>Tùy chọn</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($chiNhanh as $element)
                                             <tr>
                                            <td>{{$element->id}}</td>
                                            <td>
                                                <div class="row">
                                                    
                                                    <div class="col-md-9">
                                                       
                                                        <p>Tên chi nhánh :{{$element->ten_chi_nhanh}}</p>
                                                        <p>Địa chỉ : {{$element->address}}</p>
                                                        <p>Fax :{{$element->fax}}</p>
                                                        <p>Fax :{{$element->phone}}</p>
                                                        <p>Email: {{$element->email}}</p>
                                                       
                                                    

                                                    </div>
                                                </div>
                                            </td>
                                          
                                        
                                            
                                            <td>
                                                <a href="/admin/chi-nhanh/{{$element->id}}/edit" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>
                                              
                                                <a onclick="onDelete({{$element->id}})" href="javascript:void(0)"  class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                       
                                    


                                    </tbody>
                                </table>
                                <div align='right'>
                                    <nav aria-label="Page navigation example">
                                        {{$chiNhanh->links('vendor.pagination.default')}}
                                    </nav>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                </div>
                <!--/.row-->


            </div>
            <!--end main-->

            <!-- javascript -->
            <div class="modal fade" id="delete-modal" role="dialog">
                <div class="modal-dialog">
                
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Delete Product</h4>
                    </div>
                    <div class="modal-body">
                      <form action="" method="Post" accept-charset="utf-8">
                          @csrf;
                        <input type="hidden" value="delete" name="_method">
                        <h4>Bạn có chắc chắn xóa không</h4>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
                          <button type="submit" class="btn bg-red">Có</button>
                        </div>
                      </form>
                    </div>
                    
                  </div>
                  
                </div>
              </div>
            <script>
                function onDelete(id) {
                    $('#delete-modal form').attr('action','/admin/chi-nhanh/'+id);
                    $('#delete-modal').modal('show');
                }
            </script>
@endsection