@extends('backend.master.master')

@section('name','Trụ sở chính và chí nhánh')
@section('chi-nhanh','class=active')


@section('content')
	 <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Thêm chi nhánh</h1>
            </div>
        </div>
        <!--/.row-->
    <div class="row">
        @if(session('thongbao'))
                    <div class="alert bg-success" role="alert">
                            <svg class="glyph stroked checkmark">
                                <use xlink:href="#stroked-checkmark"></use>
                            </svg>Đã thêm thành công<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        </div>
                @endif
        <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <form action="/admin/chi-nhanh" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        @csrf
                        <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">

                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                             
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" name="email" class="form-control">
                                     {{ showErrors($errors,'email') }}
                                </div>
                               
                                <div class="form-group">
                                    <label>Tên chi nhánh</label>
                                    <input type="full" name="ten_chi_nhanh" class="form-control">
                                    {{ showErrors($errors,'ten_chi_nhanh') }}
                                </div>
                                <div class="form-group">
                                    <label>Address</label>
                                    <input type="address" name="address" class="form-control">
                                    {{ showErrors($errors,'address') }}
                                </div>
                                <div class="form-group">
                                    <label>Phone</label>
                                    <input type="phone" name="phone" class="form-control">
                                </div>
                                 <div class="form-group">
                                    <label>Fax</label>
                                    <input type="phone" name="fax" class="form-control">
                                </div>
                                <input type="checkbox" name="featured" value="1" placeholder=""> Đây có là trụ sở chính
                               
                            </div>
                             
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right">
                                  
                                    <button class="btn btn-success"  type="submit">Thêm </button>
                                    <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                </div>
                            </div>
                           

                        </div>
                    
                        <div class="clearfix"></div>
                    </div>
                    </form>
                    
                </div>

        </div>
    </div>

        <!--/.row-->
    </div>
@endsection