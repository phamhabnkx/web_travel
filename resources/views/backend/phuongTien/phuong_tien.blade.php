@extends('backend.master.master')

@section('name','Phương tiện')
@section('phuong-tien','class=active')
	
			

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#"><svg class="glyph stroked home">
							<use xlink:href="#stroked-home"></use>
						</svg></a></li>
				<li class="active">Danh sách phương tiện</li>
			</ol>
		</div>
		<!--/.row-->

		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Danh sách phương tiện</h1>
			</div>
		</div>
		<!--/.row-->

		<div class="row">
			@if (session('alert-dange'))
		        <div class="alert alert-info">{{session('alert-dange')}}</div>
		    @endif
			<div class="col-xs-12 col-md-12 col-lg-12">

				<div class="panel panel-primary">

					<div class="panel-body">
						<div class="bootstrap-table">
							<div class="table-responsive">
								@if(session('thongbaoabc'))
		                            <div class="alert bg-success" role="alert">
											<svg class="glyph stroked checkmark">
												<use xlink:href="#stroked-checkmark"></use>
											</svg>Đã xóa thành công<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
										</div>
		                        @endif
								<a href="/admin/phuong-tien/create" class="btn btn-primary">Thêm phương tiện</a>
								<table class="table table-bordered" style="margin-top:20px;">

									<thead>
										<tr class="bg-primary">
											<th>ID</th>
											<th>Thông tin sản phẩm</th>
											<th>Giá</th>
											<th>Tình trạng</th>
											
											<th>Tùy chọn</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($pt as $element)
											<tr>
											<td>{{$element->id}}</td>
											<td>
												<div class="row">
													<div class="col-md-3"><img src="{{$element->pt_img}}" alt="Áo đẹp" width="100px" class="thumbnail"></div>
													<div class="col-md-9">
														<p><strong>Mã sản phẩm : {{$element->pt_code}}</strong></p>
														<p>Tên sản phẩm :{{$element->pt_name}}</p>
														<p>số lượng chỗ ngồi : {{$element->pt_cho}} chỗ</p>
														<p>Nơi hoạt động: {{$element->pt_address}}</p>
														<p>Dòng xe: {{$element->pt_loai}}</p>
														

													</div>
												</div>
											</td>
											<td>{{$element->pt_price}}</td>
											<td>
												@if ($element->tinh_trang==1)
													<a name="" id="" class="btn btn-success" href="#" role="button">Đã mua</a>

												@endif
												@if ($element->tinh_trang==2)
													<a name="" id="" class="btn btn-danger" href="#" role="button">Đi thuê</a>
												
												@endif
												
											</td>
											
											<td>
												<a href="/admin/phuong-tien/{{$element->id}}/edit" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>
												<a onclick="onDelete({{$element->id}})" href="javascript:void(0)" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a>
											</td>
										</tr>
										@endforeach
										
										


									</tbody>
								</table>
								<div align='right'>
			                        <nav aria-label="Page navigation example">
			                            {{$pt->links('vendor.pagination.default')}}
			                        </nav>
			                    </div>
							</div>
							<div class="clearfix"></div>
						</div>

					</div>
				</div>
				<!--/.row-->


			</div>
			<!--end main-->

			<!-- javascript -->
			<div class="modal fade" id="delete-modal" role="dialog">
			    <div class="modal-dialog">
			    
			      <!-- Modal content-->
			      <div class="modal-content">
			        <div class="modal-header">
			          <button type="button" class="close" data-dismiss="modal">&times;</button>
			          <h4 class="modal-title">Xóa phương tiện</h4>
			        </div>
			        <div class="modal-body">
			          <form action="" method="Post" accept-charset="utf-8">
			              @csrf;
			            <input type="hidden" value="delete" name="_method">
			            <h4>Bạn có chắc chắn xóa không</h4>
			            <div class="modal-footer">
			              <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
			              <button type="submit" class="btn bg-red">Có</button>
			            </div>
			          </form>
			        </div>
			        
			      </div>
			      
			    </div>
			  </div>
			<script>
			    function onDelete(id) {
			        $('#delete-modal form').attr('action','/admin/phuong-tien/'+id);
			        $('#delete-modal').modal('show');
			    }
			</script>
@endsection


