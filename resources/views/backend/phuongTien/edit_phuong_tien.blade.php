@extends('backend.master.master')

@section('name','Phương tiện')
@section('phuong-tien','class=active')


@section('content')

<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Sửa phương tiện</h1>
            </div>
        </div>
        <!--/.row-->
        <div class="row">
            <div class="col-xs-6 col-md-12 col-lg-12">
                @if (session('dange'))
                <div class="alert alert-info">Thất bại</div>
            @endif
                <div class="panel panel-primary">
                    @if(session('thongbao'))
                    <div class="alert bg-success" role="alert">
                            <svg class="glyph stroked checkmark">
                                <use xlink:href="#stroked-checkmark"></use>
                            </svg>Đã sửa thành công<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        </div>
                @endif
                    <form action="/admin/phuong-tien/{{$data->id}}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                         <div class="panel-body">
                        <div class="row" style="margin-bottom:40px">
                            <div class="col-xs-8">
                                <div class="row">
                                    <div class="col-md-7">
                                        
                                        <div class="form-group">
                                            <label>Mã phương tiện</label>
                                            <input  type="text" value="{{$data->pt_code}}" name="pt_code" class="form-control">
                                            {{ showErrors($errors,'pt_code') }}
                                        </div>
                                        <div class="form-group">
                                            <label>Tên phương tiện</label>
                                            <input  type="text" value="{{$data->pt_name}}" name="pt_name" class="form-control">
                                        </div>
                                       
                                        <div class="form-group">
                                            <label>Nơi hoạt động</label>
                                            <input  type="text" value="{{$data->pt_address}}" name="pt_address" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Dòng xe</label>
                                            <input  type="text" value="{{$data->pt_loai}}" name="pt_loai" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>số chỗ ngồi</label>
                                            <input  type="text" value="{{$data->pt_cho}}" name="pt_cho" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Tình trạng</label>
                                           <select value="{{$data->tinh_trang}}" name="tinh_trang" class="form-control">
                                                <option value='1' selected> Đã mua</option>
                                                <option value='2'>Đi thuê</option>
                                                
                                            </select>
                                        </div>
                                         <div class="form-group">
                                            <label>Giá </label>
                                            <input  type="number" value="{{number_format($data->pt_price,0,'','.')}}" name="pt_price" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label>Ảnh phương tiện</label>
                                            <input id="img" type="file" value="{{$data->pt_img}}" name="pt_img" class="form-control hidden"
                                                onchange="changeImg(this)">
                                            <img id="avatar" class="thumbnail" width="100%" height="350px" src="{{$data->pt_img}}">
                                        </div>
                                    </div>
                                </div>
                               

                            </div>
                           
                        </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Miêu tả</label>
                                        <textarea id="editor"  value="{{$data->description}}" name="description" style="width: 100%;height: 100px;"></textarea>
                                    </div>
                                    <button class="btn btn-success" type="submit">Cập nhật</button>
                                    <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                </div>
                            </div>
                        <div class="clearfix"></div>
                    </div>
                    </form>
                   
                </div>

            </div>
        </div>

        <!--/.row-->
    </div>
@endsection


