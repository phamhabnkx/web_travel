@extends('backend.master.master')

@section('name','Thành viên')
@section('user','class=active')


@section('content')
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Thêm Thành viên</h1>
            </div>
        </div>
        <!--/.row-->

    <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    @if(session('thongbao'))
                    <div class="alert bg-success" role="alert">
                            <svg class="glyph stroked checkmark">
                                <use xlink:href="#stroked-checkmark"></use>
                            </svg>Đã thêm thành công<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        </div>
                @endif
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <form action="/admin/user" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                                @csrf
                                <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                 
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" name="email" class="form-control">
                                      {{showErrors($errors,'email')}}
                                    </div>
                                    <div class="form-group">
                                        <label>password</label>
                                        <input type="password" name="password" class="form-control">
                                        {{showErrors($errors,'password')}}
                                    </div>
                                    <div class="form-group">
                                        <label>Full name</label>
                                        <input type="full" name="full" class="form-control">
                                        {{showErrors($errors,'full')}}
                                    </div>
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input type="address" name="address" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Phone</label>
                                        <input type="phone" name="phone" class="form-control">
                                        {{showErrors($errors,'phone')}}
                                    </div>
                                  
                                    <div class="form-group">
                                        <label>Level</label>
                                        <select name="level" class="form-control">
                                            <option value="1">admin</option>
                                            <option selected value="2">user</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Ảnh </label>
                                        <input id="img" type="file" name="avatar" class="form-control hidden"
                                            onchange="changeImg(this)">
                                        <img id="avatar" class="thumbnail" width="40%" height="200px" src="img/import-img.png">
                                         {{showErrors($errors,'avatar')}}
                                    </div>
                                </div>
                                 
                                <div class="row">
                                    <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right">
                                      
                                        <button class="btn btn-success"  type="submit">Thêm thành viên</button>
                                        <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                    </div>
                                </div>
                            </form>
                           

                        </div>
                    
                        <div class="clearfix"></div>
                    </div>
                </div>

        </div>
    </div>

    	
@endsection