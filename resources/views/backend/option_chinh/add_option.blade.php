@extends('backend.master.master')

@section('name','admin')
@section('option','class=active')


@section('content')
	 	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Tùy chỉnh video</h1>
            </div>
        </div>
        <!--/.row-->
	    <div class="row">
	    	 @if(session('thongbao'))
                    <div class="alert bg-success" role="alert">
                            <svg class="glyph stroked checkmark">
                                <use xlink:href="#stroked-checkmark"></use>
                            </svg>Đã thêm thành công<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        </div>
                @endif
	        <div class="col-xs-12 col-md-12 col-lg-12">
	        	<form action="/admin/option" method="post" accept-charset="utf-8" enctype="multipart/form-data">
	        		@csrf
	        		<div class="panel panel-primary">
	                   
	                    <div class="panel-body">
	                        <div class="row justify-content-center" style="margin-bottom:40px">

	                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
	                             
	                                <div class="form-group">
	                                    <label>Video Background</label>
	                                    <input type="text" name="name_video_background" class="form-control" placeholder="Tên Video Background" value="">
	                                    <br>
	                                    <input type="text" name="link_video_background" class="form-control" placeholder="https://www.youtube.com/watch?v=-BzjirDqwnQ">
	                                   
	                                </div>
	                                
	                                
	                                	@for ($i=0; $i < 4; $i++)
	                                		
	                                		<div class="form-group">
			                                    <label>ID video {{$i+1}}</label>
			                                    <input type="text" name="name_video[]" class="form-control" placeholder="Tên Video" value="">
			                                    <br>
			                                    
			                                    <input type="address" name="ID_video[]" class="form-control" value="" placeholder="ID_video">
			                                </div>
	                                	@endfor
	                                 <div class="form-group">
                                        <label>Ảnh </label>
                                        <input id="img" type="file" multiple name="avatar[]" class="form-control"
                                            onchange="changeImage(this)">
                                        
                                        
                                    </div>
	                                
	                              
	                               
	                            </div>
	                            <div class="row">
	                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right">
	                                  
	                                    <button class="btn btn-success"  type="submit">Thêm</button>
	                                    <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
	                                </div>
	                            </div>
	                           

	                        </div>
	                    
	                        <div class="clearfix"></div>
	                    </div>
	                </div>
	        	</form>
	                

	        </div>
	    </div>

        <!--/.row-->
    </div>
@endsection