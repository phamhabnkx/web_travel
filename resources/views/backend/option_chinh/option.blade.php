@extends('backend.master.master')

@section('name','admin')
@section('option','class=active')


@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
        <div class="row">
            <ol class="breadcrumb">
                <li><a href="#"><svg class="glyph stroked home">
                            <use xlink:href="#stroked-home"></use>
                        </svg></a></li>
                <li class="active">Danh sách video</li>
            </ol>
        </div>
        <!--/.row-->

        
        <!--/.row-->

        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">

                <div class="panel panel-primary">

                    <div class="panel-body">
                        <div class="bootstrap-table">
                            <div class="table-responsive">
                                 @if(session('thongbaoabc'))
                                    <div class="alert bg-success" role="alert">
                                            <svg class="glyph stroked checkmark">
                                                <use xlink:href="#stroked-checkmark"></use>
                                            </svg>Đã xóa thành công<a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                                        </div>
                                @endif
                                <a href="/admin/option/create" class="btn btn-primary">Thêm video</a>
                                <table class="table table-bordered" style="margin-top:20px;">

                                    <thead>
                                        <tr class="bg-primary">
                                            <th>ID</th>
                                            <th>link video nền website</th>
                                            <th>ID video chiếu trang chủ</th>

                                            
                                          
                                           
                                            <th width='18%'>Tùy chọn</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($video as $item )
                                              <tr>
                                            <td>{{$item->id}}</td>
                                            <td>
                                                <p>{{$item->name_video_background}}</p>
                                                <p>{{$item->link_video_background}}</p>
                                            </td>
                                            <td>
                                                @for($i=0;$i<count($name_video);$i++)
                                                    <p>ID video {{$i+1}}:{{$name_video[$i][$i]}} </p>
                                                    <p>{{$ID_video[$i][$i]}}</p>
                                                @endfor
                                              
                                            </td>
                                            
                                          
                                            <td>
                                                <a href="/admin/option/{{$item->id}}/edit" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>
                                                <a onclick="onDelete({{$item->id}})" href="javascript:void(0)"  class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a>
                                            </td>
                                        </tr>
                                     @endforeach
                                      
                                        
                                
                                    </tbody>
                                </table>
                               <div align='right'>
                                    <nav aria-label="Page navigation example">
                                        {{$video->links('vendor.pagination.default')}}
                                    </nav>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                </div>
                <!--/.row-->


            </div>
            <!--end main-->

            <!-- javascript -->
            <div class="modal fade" id="delete-modal" role="dialog">
                <div class="modal-dialog">
                
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Delete Product</h4>
                    </div>
                    <div class="modal-body">
                      <form action="" method="Post" accept-charset="utf-8">
                          @csrf;
                        <input type="hidden" value="delete" name="_method">
                        <h4>Bạn có chắc chắn xóa không</h4>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
                          <button type="submit" class="btn bg-red">Có</button>
                        </div>
                      </form>
                    </div>
                    
                  </div>
                  
                </div>
              </div>
            <script>
                function onDelete(id) {
                    $('#delete-modal form').attr('action','/admin/option/'+id);
                    $('#delete-modal').modal('show');
                }
            </script>
	 
@endsection