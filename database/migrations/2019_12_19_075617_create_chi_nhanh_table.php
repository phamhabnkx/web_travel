<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChiNhanhTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chi_nhanh', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email',50)->unique();
            $table->string('ten_chi_nhanh');
            $table->string('address');
            $table->string('phone')->nullable();
            $table->string('fax')->nullable();
            $table->tinyInteger('featured')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chi_nhanh');
    }
}
