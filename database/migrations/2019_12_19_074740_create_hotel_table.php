<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('tieu_chuan');
            $table->string('ks_code',50)->unique();
            $table->string('ks_name',50);
            $table->string('slug');
            $table->string('ks_tinh_trang',191);
            $table->string('ks_address',191);
            $table->string('avatar')->nullable();
            $table->string('info',191)->nullable();
            $table->string('description',191)->nullable();
            $table->decimal('ks_price', 18, 0)->default(0);




           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel');
    }
}
