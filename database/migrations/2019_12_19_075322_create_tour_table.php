<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTourTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code',50)->unique();
            $table->text('name');
            $table->text('slug');
            $table->string('ngay_khoi_hanh',191)->nullable();
            $table->integer('DepartureId')->nullable();



            $table->decimal('so_luong_tour', 18, 0)->default(0);
            $table->string('so_ngay')->nullable();

            $table->tinyInteger('dong_tour')->unsigned();
            $table->tinyInteger('trang_thai')->unsigned();
            $table->tinyInteger('loai_tour')->unsigned();
            $table->tinyInteger('featured')->unsigned();
            $table->decimal('price', 18, 0)->default(0);
            $table->longText('gia_rieng_le')->nullable();
            $table->string('time_tap_trung',191)->nullable();
            $table->string('dia_diem_tap_trung')->nullable();
            $table->integer('quantity')->nullable();
            
            $table->longText('img');
            $table->string('avatar')->nullable();
          
            $table->text('describe')->nullable();
            $table->longText('ghi_chu')->nullable();
            $table->longText('ten_chuong_trinh')->nullable();
            $table->string('chuong_trinh_tour_ngay',191)->nullable();
            $table->longText('content')->nullable();

            $table->longText('luu_y')->nullable();
            $table->bigInteger('address_id')->unsigned();
            $table->foreign('address_id')->references('id')->on('address')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tour');
    }
}
