<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePhuongTienTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phuong_tien', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('pt_code',50)->unique();
            $table->text('pt_name');
            $table->text('slug');
            
            $table->string('pt_address',191);
            $table->string('pt_loai',191);
            $table->integer('pt_cho');
            $table->string('pt_img');
            $table->integer('tinh_trang');
           
            $table->longtext('description')->nullable();
            $table->decimal('pt_price', 18, 0)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phuong_tien');
    }
}
