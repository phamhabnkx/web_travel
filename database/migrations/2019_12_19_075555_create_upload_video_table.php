<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadVideoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upload_video', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name_video_background')->nullable();
            
            $table->string('link_video_background')->nullable();
            $table->longText('name_video')->nullable();
            $table->longText('avatar')->nullable();
            $table->longText('ID_video')->nullable();
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upload_video');
    }
}
