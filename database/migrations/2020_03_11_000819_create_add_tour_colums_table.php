<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddTourColumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tour', function (Blueprint $table) {
            $table->tinyInteger('tour_active')->default(1)->unsigned()->index();
            $table->tinyInteger('tour_sale')->unsigned()->default(0);
            $table->integer('tour_view')->default(0);
           
            $table->integer('tour_author_id')->default(0)->index();
            $table->integer('tour_total_number')->default(0)->comment('tính tổng điểm');
            $table->integer('tour_total_rating')->default(0);

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tour', function (Blueprint $table) {
            $table->dropColumn(['tour_active','tour_sale','tour_view','tour_sale','tour_author_id','tour_total_number','tour_total_rating']);
           
        });
    }
}


