<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class blogDetails extends Model
{
    protected $table='blog_details';

    function blogDetails()
    {
    	return $this->belongsTo('App\Model\blog','blog_id','id');
    }
}
