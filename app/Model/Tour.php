<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tour extends Model
{
    protected $table='tour';

    function address()
    {
    	return $this->belongsTo('App\Model\Address','address_id','id');
    }

    public function Tour_and_Phuong_tien(){
        return $this->belongsToMany('App\Model\PhuongTien','tour_and_phuong_tien','tour_id','pt_id');
    }

    public function Tour_and_Guide(){
        return $this->belongsToMany('App\Model\Guide','tour_and_huong_dan_vien','tour_id','guide_id');
    }

    public function Tour_and_Hotel(){
        return $this->belongsToMany('App\Model\Hotel','tour_and_khach_san','tour_id','ks_id');
    }
}
