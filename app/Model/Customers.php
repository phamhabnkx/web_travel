<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Customers extends Model
{
    protected $table='customers';
    function customer_order()
    {
    	return $this->hasMany('App\Model\Order','customers_id','id');
    }
}
