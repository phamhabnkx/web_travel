<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Backend\loginFormRequest;
use Auth;

class loginController extends Controller
{
    function getLogin(){
    	return view('backend.login.login');
    }

     function postlogin(loginFormRequest $r){
		//dd($r->all());
		$credentials = $r->only('email', 'password');
        $remember = $r->has('remember')?true:false;    
        if(Auth::attempt($credentials,$remember))
    	{

    		 $user = auth()->user();

      //       Auth::login($user,true);
            // dd($user);
            // $remember = true;
            //Auth::login($user, $remember);
            return redirect('/admin');
    	}
    	else
    	{
    		return redirect()->back()->withErrors(['email'=>'Tài khoản hoặc mật khẩu không chính xác'])->withInput();
    	}	
    }
}
