<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Backend\{EditGuideFormRequest,guideFormRequest};
use App\Model\Guide;

class GuideControler extends Controller
{
    private $guide;
    const NUM_OF_PAGE= 5;

    public function __construct(Guide $guide){
        $this->guide = $guide;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guide = $this->guide->orderBy('id','DESC')->paginate(self::NUM_OF_PAGE);
        return view('backend.huong_dan_vien.huong_dan_vien',compact('guide'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.huong_dan_vien.add_huong_dan_vien');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(guideFormRequest $r)
    {
        $this->guide->email =$r->email;
        $this->guide->full= $r->full;
        $this->guide->address=$r->address ;
        $this->guide->phone=$r->phone ;
       

       
        //dd($this->guide);
        $this->guide->save();
     // $this->guide->create($data);
       return redirect()->back()->with('thongbao','success !!!');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $guide = $this->guide->findOrfail($id);
       
        return view('backend.huong_dan_vien.edit_huong_dan_vien',compact('guide'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditGuideFormRequest $r, $id)
    {
        $guides= $this->guide->find($id);
        $guides->email =$r->email;
        $guides->full= $r->full;
        $guides->address=$r->address ;
        $guides->phone=$r->phone ;
       

       
        //dd($guides);
        $guides->save();
     // $this->guide->create($data);
       return redirect()->back()->with('thongbao','success !!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         Guide::destroy($id);
        return redirect()->back()->with('thongbaoabc','success !!!'); 
    }
}
