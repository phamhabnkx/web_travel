<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Backend\{chiNhanhFormRequest,EditchiNhanhFormRequest};
use App\Model\chiNhanh;

class ChiNhanhController extends Controller
{
    private $chiNhanh;
    const NUM_OF_PAGE= 5;

    public function __construct(chiNhanh $chiNhanh){
        $this->chiNhanh = $chiNhanh;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $chiNhanh = $this->chiNhanh->orderBy('id','DESC')->paginate(self::NUM_OF_PAGE);
        return view('backend.chi_nhanh.list_chi_nhanh',compact('chiNhanh'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.chi_nhanh.add_chi_nhanh');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(chiNhanhFormRequest $r)
    {
        $this->chiNhanh->email =$r->email;
        
        $this->chiNhanh->ten_chi_nhanh= $r->ten_chi_nhanh;
        $this->chiNhanh->address=$r->address ;
        $this->chiNhanh->phone=$r->phone ;
        $this->chiNhanh->fax=$r->fax ;
        if ($r->featured== null) {
             $this->chiNhanh->featured=0;
        }else {
             $this->chiNhanh->featured=$r->featured ;
        }
       

         
        // dd($this->chiNhanh);
        $this->chiNhanh->save();
     // $this->chiNhanh->create($data);
       return redirect()->back()->with('thongbao','success !!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data= $this->chiNhanh->find($id);
        return view('backend.chi_nhanh.edit_chi_nhanh',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditchiNhanhFormRequest $r, $id)
    {
        $chiNhanh = $this->chiNhanh->find($id);
       $chiNhanh->email =$r->email;
        
       $chiNhanh->ten_chi_nhanh= $r->ten_chi_nhanh;
       $chiNhanh->address=$r->address ;
       $chiNhanh->phone=$r->phone ;
       $chiNhanh->fax=$r->fax ;
        if ($r->featured== null) {
            $chiNhanh->featured=0;
        }else {
            $chiNhanh->featured=$r->featured ;
        }
       

         
        // dd($this->chiNhanh);
       $chiNhanh->save();
     // $this->chiNhanh->create($data);
       return redirect()->back()->with('thongbao','success !!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        chiNhanh::destroy($id);
        return redirect()->back()->with('thongbaoabc','success !!!'); 
    }
}
