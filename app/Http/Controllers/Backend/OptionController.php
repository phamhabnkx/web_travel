<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\UploadVideo;

class OptionController extends Controller
{
    private $video;
    const NUM_OF_PAGE= 5;

    public function __construct(UploadVideo $video){
        $this->video = $video;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $video = $this->video->orderBy('id','DESC')->paginate(self::NUM_OF_PAGE);
        // dd($video->id);
        
        foreach ($video as $key=>$value) {
            $avatar[] =json_decode($value['avatar'],true);
            $name_video[] =json_decode($value['name_video'],true);
            $ID_video[] =json_decode($value['ID_video'],true);
        }
        //dd($name_video);
       
        // json_decode($video['id']->avatar)
        return view('backend.option_chinh.option',compact('video','avatar','name_video','ID_video'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.option_chinh.add_option');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd( json_encode($request->name_video));
        $this->video->name_video_background=$request->name_video_background;
        $this->video->link_video_background=$request->link_video_background;
        $this->video->name_video= json_encode($request->name_video);
        $this->video->ID_video= json_encode($request->ID_video);

       
        if ($request->hasFile('avatar')) {

            foreach ($request->file('avatar') as $file) {
                $name=$file->getClientOriginalName();
                $file->move(public_path().'/upload_avatar/', $name);  
                $data[] = $name;
            }
      
            
            $this->video->avatar=json_encode($data);
            //dd(json_decode($this->video->avatar));
          
            // $fileName =  str_slug($r->full).'.'.$file->getClientOriginalExtension();
                    
            // $file->move('upload_avatar', $fileName);
            // $this->users->avatar = '/upload_avatar/' . $fileName; 
        }else {
             $arrayName = array('no-img.jpg','no-img.jpg','no-img.jpg'); 
            $this->video->avatar = json_encode($arrayName);
        }

        $this->video->save();
         return redirect()->back()->with('thongbao','success !!!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data= $this->video->find($id);
        $avatar=json_decode($data->avatar,true);
        $name_video=json_decode($data->name_video,true);
        $ID_video=json_decode($data->ID_video,true);
        return view('backend.option_chinh.edit_option',compact('data','avatar','name_video','ID_video'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $video = $this->video->find($id);
        $video->name_video_background=$request->name_video_background;
        $video->link_video_background=$request->link_video_background;
        $video->name_video= json_encode($request->name_video);
        $video->ID_video= json_encode($request->ID_video);
        $avatar = json_decode($video->avatar,true);
        $x = count($avatar);

       
        if ($request->hasFile('avatar')) {
            for ($i=0; $i <$x ; $i++) { 
                if($avatar[$i] !='no-img.jpg'){
                        unlink('upload_avatar/'.$avatar[$i]);
                }
            }
            foreach ($request->file('avatar') as $file) {
                $name=str_slug($request->name_video_background).'.'.$file->getClientOriginalName();
                $file->move(public_path().'/upload_avatar/', $name);  
                $data[] = $name;
            }
      
            
            $video->avatar=json_encode($data);
            //dd(json_decode($video->avatar));
          
            // $fileName =  str_slug($r->full).'.'.$file->getClientOriginalExtension();
                    
            // $file->move('upload_avatar', $fileName);
            // $this->users->avatar = '/upload_avatar/' . $fileName; 
        }
//dd($video);
        $video->save();
         return redirect()->back()->with('thongbao','success !!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         UploadVideo::destroy($id);
        return redirect()->back()->with('thongbaoabc','success !!!'); 
    }
}
