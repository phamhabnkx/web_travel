<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\{Customers,Order};

class orderController extends Controller
{
	private $customer;
    const NUM_OF_PAGE = 3;
    public function __construct(Customers $customer){
       	$this->customer=$customer;
    } 

   function getlist() 
	{ 
		$customer = $this->customer->where('state',2)->orderby('id','desc')->paginate(self::NUM_OF_PAGE);
		return view('backend.order.order',compact('customer')); 
	}
    function getdetail($id) 
    { 
    	$data=$this->customer->find($id);
    	
    	$order = $data->customer_order;
    	return view('backend.order.detailorder',compact('data','order'));  
	}

 	function getprocessed() 
 	{ 
 		$orders = $this->customer->where('state',1)->orderby('updated_at','desc')->paginate(self::NUM_OF_PAGE);
 		return view('backend.order.processed',compact(['orders']));  
 	}

 	function getpay($id)
 	{
 		$orderPay = $this->customer->find($id);
 		$orderPay->state =1;
 		$orderPay->save();
 		return redirect('/admin/order/processed');
 	}
    function getdelete($id,$x){

        Order::destroy($x);
        Customers::destroy($id);
        return redirect('/admin/order');
    }
}
