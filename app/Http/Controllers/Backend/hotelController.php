<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Backend\{HotelFormRequest,EditHotelFormRequest};
use App\Model\Hotel;

class hotelController extends Controller
{
    private $hotel;
    const NUM_OF_PAGE= 5;
    public function __construct(Hotel $hotel){
        $this->hotel = $hotel;
    }

    function gethotel(){
        $hotel= $this->hotel->orderBy('id','DESC')->paginate(self::NUM_OF_PAGE);
        //dd($hotel);
    	return view('backend.hotel.hotel',compact(['hotel']));
    }

    function getAddHotel(){
    	return view('backend.hotel.add_hotel');
    }

    function postAddHotel(HotelFormRequest $r){
        $this->hotel->tieu_chuan = $r->tieu_chuan;
        $this->hotel->ks_code = $r->ks_code;
        $this->hotel->ks_name = $r->ks_name;
        $this->hotel->slug = str_slug($r->ks_name);
        $this->hotel->ks_tinh_trang = $r->ks_tinh_trang;
        $this->hotel->ks_price = $r->ks_price;
        $this->hotel->ks_address = $r->ks_address;
        $this->hotel->info = $r->info;
        $this->hotel->description = $r->description;
       //dd(isset($r->avatar));
        if ($r->hasFile('avatar')) {
             // dd(123);
            $file = $r->avatar;
          
            $fileName =  str_slug($r->ks_name).'.'.$file->getClientOriginalExtension();
                    
            $file->move('upload_avatar', $fileName);
            $this->hotel->avatar = '/upload_avatar/' . $fileName; 
        }else {
            $this->hotel->avatar = '/upload_avatar/' . 'no-img.jpg';
        }   
        //dd($this->hotel);
        $this->hotel->save();
        return redirect()->back()->with('thongbao','success !!!');
        
    }

    function getEditHotel($id){
        $data['hotel'] = $this->hotel->find($id);  
    	return view('backend.hotel.edit_hotel',$data);
    }


    function postEditHotel(EditHotelFormRequest $r ,$id){
        $hotels = $this->hotel::find($id);
        $hotels->tieu_chuan =$r->tieu_chuan;
        $hotels->ks_code = $r->ks_code;
        $hotels->ks_name = $r->ks_name;
        $hotels->slug = str_slug($r->ks_name);
        $hotels->ks_tinh_trang = $r->ks_tinh_trang;
        $hotels->ks_price = $r->ks_price;
        $hotels->ks_address = $r->ks_address;
        $hotels->info = $r->info;
        $hotels->description = $r->description;
        
        if($r->hasFile('avatar')){
            
            if(file_exists('/upload_avatar/'.str_slug($r->full))){
                if($hotels->avatar !='no-img.jpg'){
                    unlink('upload_avatar'.str_slug($r->full));
                }
            }
          
            $file = $r->avatar; 
           
            $fileName =  str_slug($r->full).'.'.$file->getClientOriginalExtension();

            $file->move('upload_avatar', $fileName);
            $hotels->avatar = '/upload_avatar/' . $fileName; 

           
        } 
        // dd($hotels);
        $hotels->save();
       return redirect()->back()->with('thongbao','success !!!');
    }

    function delHotel($id){
        $this->hotel::destroy($id);
       return redirect()->back()->with('thongbaoabc','success !!!');
    }

    
}
