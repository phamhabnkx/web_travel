<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Backend\{TourFormRequest,EditTourFormRequest};
use App\Model\{Tour,Address,Guide,Hotel,PhuongTien};
use Auth;

class TourController extends Controller
{
    private $tour, $address, $guide, $hotel, $phuongTien;
    const NUM_OF_PAGE= 5;

    public function __construct(Tour $tour,Address $address,Guide $guide, Hotel $hotel, PhuongTien $phuongTien){
        $this->tour = $tour;
        $this->address = $address;
        $this->guide = $guide;
        $this->hotel = $hotel;
        $this->phuongTien = $phuongTien;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tour = $this->tour->orderBy('id','DESC')->paginate(self::NUM_OF_PAGE);
       
         foreach ($tour as $key=>$value) {
            $gia_rieng_le[] =json_decode($value['gia_rieng_le'],true);
            $img[] =json_decode($value['img'],true);
            $ten_chuong_trinh[] =json_decode($value['ten_chuong_trinh'],true);
            $chuong_trinh_tour_ngay[] =json_decode($value['chuong_trinh_tour_ngay'],true);
            $content[] =json_decode($value['content'],true);
        }
         //d($img);
        return view('backend.tour.list_tour',compact('tour','gia_rieng_le','img','ten_chuong_trinh','chuong_trinh_tour_ngay','chuong_trinh_tour_ngay','content'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['address']=$this->address->get();
        $data['guide']=$this->guide->get();
        $data['hotel']=$this->hotel->get();
        $data['phuongTien']=$this->phuongTien->get();

         return view('backend.tour.add_tour',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TourFormRequest $r)
    {

        try {
            if($r->address_id == null && $r->phuong_tien_id == null && $r->nhan_vien == null && $r->noi_nghi_id==null){
                // dd(123);
                return redirect()->back()->with('abc','Bạn đã thiếu các dữ liệu');

            }else{
                $this->tour->code = $r->code;
                $this->tour->name = $r->name;
                $this->tour->slug = str_slug($r->name);
                $this->tour->ngay_khoi_hanh = $r->ngay_khoi_hanh;
                $this->tour->DepartureId = $r->DepartureId;
                $this->tour->address_id = $r->address_id;
                $this->tour->so_luong_tour = $r->so_luong_tour;
                $this->tour->so_ngay = $r->so_ngay;
                $this->tour->dong_tour = $r->dong_tour;
                $this->tour->trang_thai = $r->trang_thai;
                $this->tour->loai_tour = $r->loai_tour;
                $this->tour->price = $r->price;
                $this->tour->gia_rieng_le = json_encode($r->gia_rieng_le);
                $this->tour->time_tap_trung = $r->time_tap_trung;
                $this->tour->dia_diem_tap_trung = $r->dia_diem_tap_trung;
                $this->tour->describe = $r->describe;
                $this->tour->ghi_chu = $r->ghi_chu;
                $this->tour->luu_y = $r->luu_y;
                $this->tour->tour_author_id =Auth::user()->id;

                if ($r->quantum == null) {
                    $this->tour->quantity = 0;
                }else{
                     $this->tour->quantity = $r->quantum;
                }
               
                if ($r->featured==null) {

                    $this->tour->featured=0;
                }else {
                    $this->tour->featured=1;
                }
                $this->tour->ten_chuong_trinh =json_encode($r->ten_chuong_trinh);
                $this->tour->chuong_trinh_tour_ngay =json_encode($r->chuong_trinh_tour_ngay);
                $this->tour->content =json_encode($r->content);
                // dd($this->tour->quantity);
                //dd($r->hasFile('img'));
                if ($r->hasFile('avatar')) {
             // dd(123);
                    $fileavatar = $r->avatar;
                  
                    $fileNameavatar =  str_slug($r->code).'.'.$fileavatar->getClientOriginalExtension();
                            
                    $fileavatar->move('upload_avatar', $fileNameavatar);
                    $this->tour->avatar = '/upload_avatar/' . $fileNameavatar; 
                }else {
                    $this->tour->avatar = '/upload_avatar/' . 'no-img.jpg';
                }
                if ($r->hasFile('img')) {

                    foreach ($r->file('img') as $file) {
                        $name=str_slug($r->code).'.'.$file->getClientOriginalName();
                        $file->move(public_path().'/upload_avatar/', $name);  
                        $data[] = $name;
                    }
              
                    
                    $this->tour->img=json_encode($data);
                    //dd(json_decode($video->avatar));
                  
                    // $fileName =  str_slug($r->full).'.'.$file->getClientOriginalExtension();
                            
                    // $file->move('upload_avatar', $fileName);
                    // $this->users->avatar = '/upload_avatar/' . $fileName; 
                }else {
                    $arrayName = array('no-img.jpg','no-img.jpg','no-img.jpg'); 
                    $this->tour->img = json_encode($arrayName);
                }
                $this->tour->save();
             // dd($r->only('nhan_vien','phuong_tien_id','noi_nghi_id'));
               
                $this->tour->Tour_and_Phuong_tien()->Attach($r->phuong_tien_id);
                $this->tour->Tour_and_Guide()->Attach($r->nhan_vien);
                $this->tour->Tour_and_Hotel()->Attach($r->noi_nghi_id);
                return redirect()->back()->with('thongbao','success!!');
            }
           
            
        } catch (Exception $e) {
           
           return redirect()->back();
        }
        // dd($r->all());
       
       


         



    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['address']=$this->address->get();
        $data['guide']=$this->guide->get();
        $data['hotel']=$this->hotel->get();
        $data['phuongTien']=$this->phuongTien->get();
        $tour['tour']= $this->tour->findorFail($id);
       // dd($tour['tour']->gia_rieng_le);
        $tour['gia_rieng_le'] =json_decode($tour['tour']->gia_rieng_le,true);
        $tour['img'] =json_decode($tour['tour']->img,true);
        $tour['ten_chuong_trinh'] =json_decode($tour['tour']->ten_chuong_trinh,true);
        // $tour['quantity']=3;
        $tour['chuong_trinh_tour_ngay'] =json_decode($tour['tour']->chuong_trinh_tour_ngay,true);
        $tour['content'] =json_decode($tour['tour']->content,true);
       // dd($tour);
        
        return view('backend.tour.edit_tour',$data,$tour);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditTourFormRequest $r, $id)
    {
       
          try {
            if($r->address_id == null && $r->phuong_tien_id == null && $r->nhan_vien == null && $r->noi_nghi_id==null){
                // dd(123);
                return redirect()->back()->with('abc','Bạn đã thiếu các dữ liệu');

            }else{
                $data = $this->tour->findorFail($id);
                $quantity = $data->quantity;
                //dd($quantity);
                //dd($data);
                //dd($r->featured);
                if ($r->quantum == null) {
                   $data->quantity =$quantity;
                }else{
                    $data->quantity =$r->quantum; 
                }
                $data->code = $r->code;
                $data->name = $r->name;
                $data->slug = str_slug($r->name);
                $data->ngay_khoi_hanh = $r->ngay_khoi_hanh;
                $data->DepartureId = $r->DepartureId;
                $data->address_id = $r->address_id;
                $data->so_luong_tour = $r->so_luong_tour;
                $data->so_ngay = $r->so_ngay;
                $data->dong_tour = $r->dong_tour;
                $data->trang_thai = $r->trang_thai;
                $data->loai_tour = $r->loai_tour;
                $data->price = $r->price;
                $data->gia_rieng_le = json_encode($r->gia_rieng_le);
                $data->time_tap_trung = $r->time_tap_trung;
                $data->dia_diem_tap_trung = $r->dia_diem_tap_trung;
                $data->describe = $r->describe;
                $data->ghi_chu = $r->ghi_chu;
                $data->luu_y = $r->luu_y;
                $data->tour->tour_author_id =Auth::user()->id;
                if ($r->featured==null) {

                    $data->featured=0;
                }else {
                    $data->featured=1;
                }
                            
                $data->ten_chuong_trinh =json_encode($r->ten_chuong_trinh);
                $data->chuong_trinh_tour_ngay =json_encode($r->chuong_trinh_tour_ngay);
                $data->content =json_encode($r->content);
                $avatar = json_decode($data->img,true);
                $x = count($avatar);
                //dd($avatar[0]);

                if($r->hasFile('avatar')){
            
                    if(file_exists('/upload_avatar/'.str_slug($r->code))){
                        if($data->avatar !='no-img.jpg'){
                            unlink('upload_avatar'.str_slug($r->code));
                        }
                    }
                  
                    $fileavatar = $r->avatar; 
                   
                    $fileNameavatar =  str_slug($r->code).'.'.$fileavatar->getClientOriginalExtension();

                    $fileavatar->move('upload_avatar', $fileNameavatar);
                    $data->avatar = '/upload_avatar/' . $fileNameavatar; 

                   
                }
                 if ($r->hasFile('img')) {
                    for ($i=0; $i <$x ; $i++) { 
                        if($avatar[$i] !='no-img.jpg'){
                                unlink('upload_avatar/'.$avatar[$i]);
                        }
                    }
                    foreach ($r->file('img') as $file) {

                        $name=str_slug($r->code).'.'.$file->getClientOriginalName();
                        $file->move(public_path().'/upload_avatar/', $name);  
                        $img[] = $name;
                    }
                    //dd($img);
                    
                    $data->img=json_encode($img);
                    //dd($data->img);
                 }   
                

                $data->save();
         
               
                $data->Tour_and_Phuong_tien()->Sync($r->phuong_tien_id);
                $data->Tour_and_Guide()->Sync($r->nhan_vien);
                $data->Tour_and_Hotel()->Sync($r->noi_nghi_id);
                return redirect()->back()->with('thongbao','success!!');
            }
           
            
        } catch (Exception $e) {
           
           return redirect()->back();
        }

        //dd($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        Tour::destroy($id);
        return redirect()->back()->with('thongbaoabc','success !!!'); 
    }
}
