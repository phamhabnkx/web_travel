<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Backend\{PhuongTienFormRequest,EditPhuongTienFormRequest};
use App\Model\PhuongTien;

class PhuongTienControler extends Controller
{
    private $pt;
    const NUM_OF_PAGE= 5;

    public function __construct(PhuongTien $phuongTien){
        $this->pt = $phuongTien;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pt = $this->pt->orderBy('id','DESC')->paginate(self::NUM_OF_PAGE);
        return view('backend.phuongTien.phuong_tien',compact('pt'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.phuongTien.add_phuong_tien');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PhuongTienFormRequest $r)
    {
        // dd($r->all());
        $this->pt->pt_code =$r->pt_code;      
        $this->pt->pt_name= $r->pt_name;
        $this->pt->slug= str_slug($r->pt_name);
        $this->pt->pt_address=$r->pt_address ;
        $this->pt->pt_loai=$r->pt_loai ;
        $this->pt->pt_cho=$r->pt_cho ;
        $this->pt->pt_price=$r->pt_price ;
        $this->pt->tinh_trang=$r->tinh_trang ;
        $this->pt->description=$r->description ;

        if ($r->hasFile('pt_img')) {
            $file = $r->pt_img;
          
            $fileName =  str_slug($r->pt_name).'.'.$file->getClientOriginalExtension();
                    
            $file->move('upload_avatar', $fileName);
            $this->pt->pt_img = '/upload_avatar/' . $fileName; 
        }else {
            $this->pt->pt_img = '/upload_avatar/' . 'no-img.jpg';
        }   
        //dd($this->pt);
        $this->pt->save();
     // $this->pt->create($data);
       return redirect()->back()->with('thongbao','success !!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data= $this->pt->find($id);
        return view('backend.phuongTien.edit_phuong_tien',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditPhuongTienFormRequest $r, $id)
    {
        
         try{

            $phuongTien = $this->pt::find($id);
            $phuongTien->pt_code =$r->pt_code;      
            $phuongTien->pt_name= $r->pt_name;
            $phuongTien->slug= str_slug($r->pt_name);
            $phuongTien->pt_address=$r->pt_address ;
            $phuongTien->pt_loai=$r->pt_loai ;
            $phuongTien->pt_cho=$r->pt_cho ;
            $phuongTien->pt_price=$r->pt_price ;
            $phuongTien->tinh_trang=$r->tinh_trang ;
            $phuongTien->description=$r->description;
            
            if($r->hasFile('pt_img')){
                
                if(file_exists('/upload_avatar/'.str_slug($r->pt_name))){
                    if($pt->pt_img !='no-img.jpg'){
                        unlink('upload_avatar'.str_slug($r->pt_name));
                    }
                }
              
                $file = $r->pt_img; 
               
                $fileName =  str_slug($r->pt_name).'.'.$file->getClientOriginalExtension();

                $file->move('upload_avatar', $fileName);
                $phuongTien->pt_img = '/upload_avatar/' . $fileName; 

               
            } 
            $phuongTien->save();
            return redirect()->back()->with('thongbao','success !!!');
        }catch (\Exception $e){
            // dd(123);
            // $r->session()->flash('danger','Whops! Something went wrong!');
            return redirect()->back()->with('danger','Whops! Something went wrong!');
            
        }
      ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PhuongTien::destroy($id);
        return redirect()->back()->with('thongbaoabc','success !!!'); 
    }
}
