<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Backend\{usersFormRequest,editUsersFormRequest};
use App\User;

class UserController extends Controller
{
    private $users;
    const NUM_OF_PAGE= 5;

    public function __construct(User $user){
        $this->users = $user;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->users->orderBy('id','DESC')->paginate(self::NUM_OF_PAGE);
        return view('backend.user.user',compact(['users']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.user.add_user');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(usersFormRequest $r)
    {
        $this->users->email =$r->email;
        $this->users->password = bcrypt($r->password);
        $this->users->full= $r->full;
        $this->users->address=$r->address ;
        $this->users->phone=$r->phone ;
        $this->users->level=$r->level ;

        if ($r->hasFile('avatar')) {
            $file = $r->avatar;
          
            $fileName =  str_slug($r->full).'.'.$file->getClientOriginalExtension();
                    
            $file->move('upload_avatar', $fileName);
            $this->users->avatar = '/upload_avatar/' . $fileName; 
        }else {
            $this->users->avatar = '/upload_avatar/' . 'no-img.jpg';
        }   
        //dd($this->users);
        $this->users->save();
     // $this->users->create($data);
       return redirect()->back()->with('thongbao','success !!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] =User::find($id);
        return view('backend.user.edit_user',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(editUsersFormRequest $r, $id)
    {
        $user = $this->users::find($id);
        $user->email =$r->email;
        $user->password =  bcrypt($r->password);
        $user->full= $r->full;
        $user->address=$r->address ;
        $user->phone=$r->phone ;
        $user->level=$r->level ;
        
        if($r->hasFile('avatar')){
            
            if(file_exists('/upload_avatar/'.str_slug($r->full))){
                if($user->avatar !='no-img.jpg'){
                    unlink('upload_avatar'.str_slug($r->full));
                }
            }
          
            $file = $r->avatar; 
           
            $fileName =  str_slug($r->full).'.'.$file->getClientOriginalExtension();

            $file->move('upload_avatar', $fileName);
            $user->avatar = '/upload_avatar/' . $fileName; 

           
        } 
        $user->save();
       return redirect()->back()->with('thongbao','success !!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
       User::destroy($id);
        return redirect()->back()->with('thongbaoabc','success !!!'); 
    }
}
