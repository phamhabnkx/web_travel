<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use App\Model\Customers;

class indexController extends Controller
{
    function getindex(){

    	$yearNow=Carbon::now()->format('Y');
    	$monthNow=Carbon::now()->format('m');
    	// dd($monthNow);

    	for ($i=1; $i <=12 ; $i++) { 
    		$dl['Tháng '.$i] = Customers::where('state',1)->whereMonth('updated_at',$i)->whereYear('updated_at',$yearNow)->sum('total');

    	}
    	switch ($monthNow) {
            case '01':
                $monthNow = 1;
                break;

            case '02':
                $monthNow = 2;
                break;
            case '03':
                $monthNow = 3;
                break;
            case '04':
                $monthNow = 4;
                break;
           case '05':
                $monthNow = 5;
                break;
            case '06':
                $monthNow = 6;
                break;
            case '07':
                $monthNow = 7;
                break;
            case '08':
                $monthNow = 8;
                break;
            case '09':
                $monthNow = 9;
                break;
        }
        //dd($monthNow);
    	$data['dl']= $dl;
    	$data['so_dh']=Customers::where('state',2)->count();
    	return view('backend.index',$data,compact('monthNow')); 
    }
    function getlogout(){
    	Auth::logout();
    	return redirect('/admin/login');
    }
}
