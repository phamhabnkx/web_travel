<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Backend\{blogDetailsFormRequest,editBlogDetailsFormRequest};
use App\Model\{blog,blogDetails};

class BlogDetailsController extends Controller
{
    private $blogDetails, $blog;
    const NUM_OF_PAGE= 5;

    public function __construct(blogDetails $blogDetails, blog $blog){
        $this->blogDetails = $blogDetails;
        $this->blog = $blog;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogDetails= $this->blogDetails->orderBy('id','DESC')->paginate(self::NUM_OF_PAGE);
        return view('backend.blog_details.blog_details',compact('blogDetails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['blog']=$this->blog->get();
       
        return view('backend.blog_details.add_blog_details',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(blogDetailsFormRequest $r)
    {
        $this->blogDetails->bai_viet_code =$r->bai_viet_code;
        $this->blogDetails->name =$r->name;
       
        $this->blogDetails->content =$r->content;
        $this->blogDetails->blog_id =$r->the_loai;
      
      
        if ($r->hasFile('bai_viet_img')) {
            $file = $r->bai_viet_img;
          
            $fileName =  str_slug($r->bai_viet_code).'.'.$file->getClientOriginalExtension();
                    
            $file->move('upload_avatar', $fileName);
            $this->blogDetails->bai_viet_img = '/upload_avatar/' . $fileName; 
        }else {
            $this->blogDetails->bai_viet_img = '/upload_avatar/' . 'no-img.jpg';
        }   
        // dd( $this->blogDetails);
         $this->blogDetails->save();
     // $this->users->create($data);
       return redirect()->back()->with('thongbao','success !!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['blogDetails'] =$this->blogDetails->find($id);
        $data['blog']=$this->blog->get();
        return view('backend.blog_details.edit_blog_details',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(editBlogDetailsFormRequest $r, $id)
    {
        $blogs =$this->blogDetails->find($id);
        $blogs->bai_viet_code =$r->bai_viet_code;
        $blogs->name =$r->name;
       
        $blogs->content =$r->content;
        $blogs->blog_id =$r->the_loai;
        
        if($r->hasFile('bai_viet_img')){
            
            if(file_exists('/upload_avatar/'.str_slug($r->bai_viet_code))){
                if($blogs->bai_viet_img !='no-img.jpg'){
                    unlink('upload_avatar'.str_slug($r->bai_viet_code));
                }
            }
          
            $file = $r->bai_viet_img;
          
            $fileName =  str_slug($r->bai_viet_code).'.'.$file->getClientOriginalExtension();
                    
            $file->move('upload_avatar', $fileName);
            $blogs->bai_viet_img = '/upload_avatar/' . $fileName; 
         

           
        }
      
       
         $blogs->save();
     // $this->users->create($data);
       return redirect()->back()->with('thongbao','success !!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        blogDetails::destroy($id);
        return redirect()->back()->with('thongbaoabc','success');
    }
}
