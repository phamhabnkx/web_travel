<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Backend\{AddressFormRequest,EditAddressFormRequest};
use App\Model\Address;

class AddressControler extends Controller
{
    private $address;
    public function __construct(Address $address)
    {
        $this->address = $address;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['address'] =Address::all()->toarray();
        return view('backend.address.address',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddressFormRequest $r)
    {
        if(getLevel(Address::all()->toarray(), $r->idParent,1 )<3){
            $this->address->name= $r->name;
            $this->address->slug = str_slug($r->name);
            $this->address->parent = $r->idParent;
            // dd(str_slug($r->name));
            $this->address->save();
            return redirect()->back()->with('thongbao','success !!!');
        }else{

            return redirect()->back()->withErrors(['name'=>'giao diện không hỗ trợ danh mục lơn hơn 2 cấp '])->withInput();
            // thuộc tính old() để trả về giá trị đã nhập của form request .vì vậy khi sử dụng withErrors thì sẽ ko dùng old đc .Muốn sử dungj ddc cần gửi thêm withInput();
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['address'] =$this->address::find($id);
        $data['addressdanhmuc']=$this->address::all();

        return view('backend.address.edit_address',$data); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditAddressFormRequest $r, $id)
    {
        if(getLevel(Address::all()->toarray(), $r->idParent,1 )<3){
            $data= $this->address::find($id);
            $data->name= $r->name;
            $data->slug = str_slug($r->name);
            $data->parent = $r->idParent;
            // dd(str_slug($r->name));
            $data->save();
            return redirect()->back()->with('thongbao','success !!!');
        }else{

            return redirect()->back()->withErrors(['name'=>'giao diện không hỗ trợ danh mục lơn hơn 2 cấp '])->withInput();
            // thuộc tính old() để trả về giá trị đã nhập của form request .vì vậy khi sử dụng withErrors thì sẽ ko dùng old đc .Muốn sử dungj ddc cần gửi thêm withInput();
            
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // dd(123);
        $this->address::destroy($id);
        return redirect('admin/dia-diem')->with('thongbaoabc','success !!!'); 
    }
}
