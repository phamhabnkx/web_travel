<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\{UploadVideo,Tour,blogDetails};

class indexController extends Controller
{
	private $video, $tour, $blogDetails;

	public function __construct(UploadVideo $video, Tour $tour, blogDetails $blog)
	{
		$this->video = $video;
		$this->tour= $tour;
		$this->blogDetails=$blog;
	}
    function getIndex(){

    	   $data = $this->video->first();
           if(isset($data)){
                $avatar=json_decode($data->avatar,true);
                $ID_video=json_decode($data->ID_video,true);
           }
        
           
     
    	
    	//dd(count($ID_video));

    	$tour['details']=$this->tour->where('featured',1)->orderBy('id','ASC')->get();
    	

    	$blog = $this->blogDetails->orderBy('id','Desc')->take(5)->get();

    	return view('frontend.index',$tour,compact('data','avatar','ID_video','blog'));
    }

    function getContact(){
    	return view('frontend.contact');
    }
    function getsearch(Request $r){
        
        //dd($r->all());
        if ($r->thanh_pho != '' && $r->max != '' && $r->min !='') {
            $data = $this->video->first();
            //dd($data);
           $search = $this->tour->where('name','like', '%'.$r->thanh_pho.'%')->whereBetween('price',[$r->min,$r->max])->orderBy('id','ASC')->get();
           //dd($search);
           return view('frontend.search',compact('data','search'));
        }
        else{
            return redirect()->back();
        }
        
    }

    function getsearchSecond(Request $r)
    {
        //dd($r);
        if ($r->search != '') {
            $data = $this->video->first();
            $search = $this->tour->where('name','like', '%'.$r->search.'%')->orderBy('id','Desc')->get();
            return view('frontend.seachsecond',compact('data','search'));
            
        }else{
            return redirect()->back();
        }
    }
    function renderProductView(Request $r){
        // nếu tồn tại ajax
        if ($r->ajax()) {
            $listID = $r->id;
            $tourView = Tour::whereIn('id',$listID)->get();
            
            $html = view('frontend.tour_view',compact('tourView'))->render();
            return response()->json(['data'=>$html]);
        }
        
    }

    
}
