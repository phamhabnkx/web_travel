<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Rules\Captcha;
use Hash;
use Illuminate\Support\Facades\Validator;

class informationFrontendController extends Controller
{
    function getInfor(){
    	if (Auth::check()) {
    		$user = Auth::user()->toarray();
    		//dd($user);
    		return view('frontend.informationCustom',compact('user'));
    		//dd($user->toarray());
    	}
    }
    protected function validator(array $data)
    {
    	return Validator::make($data,[
    		'g-recaptcha-response'=>new Captcha(),
    		'email'=>'required',
    		'password_old'=>'required',// la mk hien tai
    		'password_new' => 'required|min:6',
            'password_confirm' => 'required|same:password_new',
            

    	]);
    }

     function postInfor(Request $request)
    {
    	//dd($request->all());
    	$data = $this->Validator($request->all())->Validate();// đây là một mảng

    	if ($request->check == null) {

    		return redirect()->back()->with('fail','Bạn chưa tích vào ô đồng ý');
    	}
    	$checkUser = User::where('email',$data['email'])->first();
    	if (!$checkUser) {
    	 	return redirect()->back()->with('danger','email not found');
    	 }
    	
		if(!(Hash::check($request->get('password_old'), Auth::user()->password))) {
        
        	return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
    	}

    	if(strcmp($data['password_old'], $data['password_new']) == 0){
            //Current password and new password are same
            return redirect()->back()->with("errors1","New Password cannot be same as your current password. Please choose a different password.");
        }

        if(strcmp($data['password_confirm'], $data['password_new']) == 0){
        	$user = Auth::user();
	        $user->password = bcrypt($data['password_new']);
	       //dd($request->hasFile('avatar'));
	        if($request->hasFile('avatar')){
            
	            if(file_exists('/upload_avatar/'.str_slug($request->email))){
	                if($user->avatar !='no-img.jpg'){
	                    unlink('upload_avatar'.str_slug($request->email));
	                }
	            }
	          
	            $file = $request->avatar; 
	           
	            $fileName =  str_slug($request->email).'.'.$file->getClientOriginalExtension();

	            $file->move('upload_avatar', $fileName);
	            $user->avatar = '/upload_avatar/' . $fileName; 

	          //dd($user->avatar);
	        }

	        


	        $user->save();
	        return redirect()->back()->with("success","Password changed successfully !");
        }else {
        	return redirect()->back()->with('password_confirm','Mật khẩu bạn vừa tạo không đúng khi xác nhận');
        }





    	
    }
}
