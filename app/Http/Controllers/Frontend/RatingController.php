<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\{Rating,Tour};
use Auth;
use Carbon\Carbon;

class RatingController extends Controller
{
    function saveRating(Request $r,$id)
    {
    	//dd(123);
    		if ( $r->ajax()) {
	    		Rating::insert([
	    			'ra_product_id' =>$id,
	    			'ra_number' => $r->number,
	    			'ra_content'=> $r->ra_content,
	    			'ra_user_id'=>get_data_user('web'),
	    			'created_at'=>Carbon::now(),
	    			 'updated_at'=>Carbon::now(),
	    		]);
	    		$tour = Tour::find($id);
	    		$tour->tour_total_rating += $r->number ;
	    		$tour->tour_total_number +=1;
	    		$tour->save();
	    		return response()->json(['code'=>'1']);
	    	}
    	
    	

    }
}
