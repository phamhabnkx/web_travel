<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\{blog,blogDetails};

class tinTucController extends Controller
{
	private $blog, $blogDetails;
	const NUM_OF_PAGE= 5;
	public function __construct(blog $blog, blogDetails $blogDetails)
	{
		$this->blog = $blog;
		$this->blogDetails =$blogDetails;
	}

    function getshowTinTuc(){

    	$data['blog'] = $this->blog->get();
    	$data['blogDetails'] = $this->blogDetails->orderBy('id','Desc')->paginate(self::NUM_OF_PAGE);
    	//dd($data);
    	return view('frontend.tin_tuc.tin_tuc',$data);
    }

    function getshowTinTucDetails($str){
    	
    	$array = explode('-', $str);
        
        $id = array_pop($array);

        $data['blogDetails'] = $this->blogDetails->find($id);
       
        $date = $data['blogDetails']->updated_at->day;
        $month = $data['blogDetails']->updated_at->month;
        // dd($date);
        
    	return view('frontend.tin_tuc.tin_tuc_details',$data,compact('date','month'));
    }
}
