<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\{Tour,Address,Guide,Hotel,chiNhanh,PhuongTien};

class tourController extends Controller
{
	private $tour, $addresss, $guide, $hotel, $chiNhanh, $phuongTien;
	const NUM_OF_PAGE= 4;
	function __construct(Tour $tour, Address $address, Guide $guide, Hotel $hotel, chiNhanh $chiNhanh, PhuongTien $phuongTien)
	{
		$this->tour= $tour;
		$this->address= $address;
		$this->guide= $guide;
		$this->hotel= $hotel;
		$this->chiNhanh= $chiNhanh;
		$this->phuongTien= $phuongTien;
		
	}

    function getTour(Request $r){

        $query = Tour::query();
        //dd($r->all());
        if ($r->DepartureId != '' || $r->loai_tour != '' ||$r->DongTour != '' || $r->noi_den != '' ||$r->PriceId !=''||$r->ngay_khoi_hanh !='') {
            if ($r->PriceId!='') {
                
               
                if(!empty($r->DepartureId)) {
                    $query->orWhere('DepartureId',$r->DepartureId);
                }

                if (!empty($r->loai_tour)) {
                    $query->orWhere('loai_tour',$r->loai_tour);
                }

                if (!empty($r->dong_tour)) {
                    $query->orWhere('dong_tour',$r->dong_tour);
                }

                if (!empty($r->address_id)) {
                    $query->orWhere('address_id',$r->address_id);
                }

                if (!empty($r->ngay_khoi_hanh)) {
                    $query->orWhere('ngay_khoi_hanh',$r->ngay_khoi_hanh);
                }
                    $priceId = explode('-', $r->PriceId);
                $tour = $query->orWhere(function ($query) use ($priceId) {
                    
                    $query->whereBetween('price',[$priceId[0],$priceId[1]]);
                })->orderBy('id','DESC')->paginate(self::NUM_OF_PAGE);
                //dd($tour);

            } else {
                if(!empty($r->DepartureId)) {
                    $query->orWhere('DepartureId',$r->DepartureId);
                }

                if (!empty($r->loai_tour)) {
                    $query->orWhere('loai_tour',$r->loai_tour);
                }

                if (!empty($r->dong_tour)) {
                    $query->orWhere('dong_tour',$r->dong_tour);
                }

                if (!empty($r->address_id)) {
                    $query->orWhere('address_id',$r->address_id);
                }

                if (!empty($r->ngay_khoi_hanh)) {
                    $query->orWhere('ngay_khoi_hanh',$r->ngay_khoi_hanh);
                }
                    
                $tour = $query->orderBy('id','DESC')->paginate(self::NUM_OF_PAGE);
                //dd($tour);
            }
            // dd($tour);
            
        }else{
            $tour = $this->tour->orderBy('id','DESC')->paginate(self::NUM_OF_PAGE);
        }
    	$data['address'] = $this->address->all();
    	$data['chiNhanh'] = $this->chiNhanh->all();


        

    	
    	foreach ($tour as $value) {
    		$value->ngay_khoi_hanh = explode('-', $value->ngay_khoi_hanh);
    		
    	}
    	//dd($array);
	
    	return view('frontend.tour.tour',compact('tour'),$data);
    }

    function getTourDetails($str){
    	$array = explode('-', $str);
        
        $id = array_pop($array);

        $data['tour'] = $this->tour->find($id);

        $data['gia_rieng_le']=json_decode($data['tour']->gia_rieng_le,true);
        $data['img']=json_decode($data['tour']->img,true);
        $data['ten_chuong_trinh']=json_decode($data['tour']->ten_chuong_trinh,true);
        $data['chuong_trinh_tour_ngay']=json_decode($data['tour']->chuong_trinh_tour_ngay,true);
        $data['content']=json_decode($data['tour']->content,true);


        $data['chi_nhanh'] =  $this->chiNhanh->get();
        $data['guide'] =  $this->guide->get();
        $data['hotel'] =  $this->hotel->get();
        $data['phuongTien'] =  $this->phuongTien->get();

       
        // for ($i=0; $i <count($data['img']) ; $i++) { 
        //     echo $data['img'][$i];
        // }
        // dd($data);
    	return view('frontend.tour.tour_details',$data);
    }
}
