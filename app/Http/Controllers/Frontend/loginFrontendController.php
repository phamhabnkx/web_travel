<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Frontend\{RegisterRequest,LoginRequest,EmailVerifyAcountAgain};
use App\User;
use Auth;
use Carbon\Carbon;
use Mail;

class loginFrontendController extends Controller
{
	private $users;
    const NUM_OF_PAGE= 5;

    public function __construct(User $user){
        $this->users = $user;
    }

   function getlogin(){
   	return view('frontend.login_frontend');
   }

   function postlogin(LoginRequest $r){
   		$credentials = $r->only('email', 'password');
      $remember = $r->has('remember')?true:false;    
      if(Auth::attempt($credentials,$remember))
    	{
        if (get_data_user('web','active') == 2) {
           $user = auth()->user();
            return redirect('/');
        }
        Auth::logout();
        return redirect('/login')->with('tb','tai khoan chua xac thuc');


        }
    		
    	else
    	{
    		return redirect()->back()->withErrors(['email'=>'Tài khoản hoặc mật khẩu không chính xác'])->withInput();
    	}	
   }

    function getlogout(){
    	Auth::logout();
    	return redirect('/login');
    }

   function postRegister(RegisterRequest $r){
   		//dd($r->all());

        $this->users->email =$r->email_user;
        $this->users->password = bcrypt($r->password_user);
        $this->users->full= $r->name;
        
        $this->users->phone=$r->phone_number ;
        $this->users->level=3;
        $this->users->avatar = '/upload_avatar/' . 'no-img.jpg';
         
        //dd($this->users);
        $this->users->save();
        // dd($this->users->id);
        if ($this->users->id) {
          $email = $this->users->email;
          $code = bcrypt(md5(time().$email));
          $url = route('get.verify.Acount',['code' => $code, 'id' =>$this->users->id,'email'=>$email]);

          $this->users->code_active = $code;
          $this->users->time_active = Carbon::now();
          $this->users->save();


          $data = [
            'route' => $url
          ];
          //d($email);
          Mail::send('frontend.mail.VerifyAcount', $data, function ($message) use($email) {
                $message->from('datn20191@gmail.com', 'FriendTravels');
                $message->to($email, 'Khach Hang');
                $message->subject('Xác thực mật khẩu');
            

            });
            
           return redirect()->back()->with('thongbao','vui lòng xác nhận qua Email !!!');
        }
        return redirect()->back();

        
   }


   /*
    Xác thực tài khoản sau đăng kí
   */
   function verifyEmailAcount(Request $r){
      $code = $r->code;
      $id = $r->id;
      
      $checkUser = User::where([
          'code_active' => $code,
          'id' => $id
      ])->first();
      // dd($checkUser);
      if(!$checkUser)
      {
          return redirect('/')->with('danger','Xác thực thất bại');
      }
      $checkUser->active=2;
      $checkUser->save();
      return redirect('/login')->with('success','thanh cong');


   }
  function getVerifyAgain(){
    return view('frontend.VerifyAcount.EmailVerifyAcountAgain');
       
  }

  function postEmailVerifyAgain(EmailVerifyAcountAgain $r){
     $email = $r->email;

     $checkUser = User::where('email',$email)->first();
    if (!$checkUser) {

      return redirect()->back()->with('danger','email not found');
    }
    $code = bcrypt(md5(time().$email));
    $checkUser->code_active = $code;
    $checkUser->time_active = Carbon::now();
    $checkUser->save();
    $url = route('get.link.code.verify',['code' => $checkUser->code_active, 'email' => $email]);
    $data = [
        'route' => $url
      ];

    Mail::send('frontend.VerifyAcount.sendMailVerifyAgain', $data, function ($message) use($email) {
            $message->from('datn20191@gmail.com', 'FriendTravels');
            $message->to( $email, 'Khach Hang');
            $message->subject('Xác thực lại');
        

        });
      return redirect()->back()->with('success','Link xác thực đã được gửi vào mail của ban');


  }
  function resetCodeEmailVerifyAgain(Request $r){
    $code = $r->code;
    $email = $r->email;
    
    $checkUser = User::where([
        'code_active' => $code,
        'email' => $email
    ])->first();
     
    if(!$checkUser)
      {
          return redirect('/')->with('danger','Xác thực thất bại');
      }
      $checkUser->active=2;
      $checkUser->save();
      return redirect('/login')->with('success','thanh cong');



  }
}


