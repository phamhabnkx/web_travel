<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Cart;
use App\Model\{Tour,Order,Customers};
use App\Http\Requests\Frontend\postCheckoutFormRequest;
use  Mail;


class cartController extends Controller
{
	 private $orders;
    const NUM_OF_PAGE = 3;
    public function __construct(Order $order){
        $this->orders=$order;
    }
     function getCart()
    {
    	//dd(Cart::content());
    	$data['cart']=Cart::content();
   	 	$data['total']=Cart::total(0,'',',');
   	 	//dd($data);
    	return view('frontend.cart.cart',$data);
    }


    function addItem(Request $r){
    	$tour = Tour::find($r->tourId);
    	Cart::add([
   			'id' => $tour->code, 
   			'name' => $tour->name, 
   			'qty' => $r->quantity, 
   			'price' => $tour->price, 
   			'weight' => 0, 
   			'options' => ['img' => $tour->avatar, 'ngay_khoi_hanh' =>$tour->ngay_khoi_hanh , 'so_ngay'=>$tour->so_ngay]
   		]);

        return redirect('cart');
    }

    function updateItem($rowId,$qty){
    	Cart::update($rowId,$qty);
	   	echo "success";
    }

    function delItem($rowId)
	{
		Cart::remove($rowId);
		return redirect('/');
	}

	public function postCheckout(postCheckoutFormRequest $r)
	{

		if ($r->chkDieuKhoan == null) {
			return redirect()->back()->with('abc','fails');
		}else {
			$customer= new Customers();
	        $customer->email=$r->email;
	        $customer->full=$r->full;
	        $customer->address=$r->address;
	        $customer->phone=$r->phone;
	        $customer->total=Cart::total(0,'','');
	        $customer->state=2;
	        $customer->save();
	        $data['mail']=$r->email;

	        foreach (Cart::content() as $r) {
	            $order=new Order();
	            $order->code=$r->id;
	            $order->name=$r->name;
	            $order->price=$r->price;
	            $order->quantity=$r->qty;
	            $order->customers_id=$customer->id;
	            $order->img= $r->options->img;
	            $order->save();
        	}
        	$data['customer']=Customers::find($customer->id);
        
        	$data['informationOrder'] = $data['customer']->customer_order;
        	//dd($data);

        	Mail::send('frontend.mail.mailOrder', $data, function ($message) use($data) {
	            $message->from('datn20191@gmail.com', 'FriendTravels');
	            $message->to($data['mail'], 'Khach Hang');
	            $message->subject('Thông tin tour');
	        

	        });
	        Cart::destroy();
	     	return redirect('cart/complete/'.$customer->id);
	        
        	
 

		}
	}

	function getcomplete($id){
		$data['customer']=Customers::find($id);
        $data['inforCustomer'] = $data['customer']->customer_order;
        //dd($data);
    	return view('frontend.cart.complete',$data);
	}
}
