<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Backend\{forgetEmail,ForgotPasswordRequest};
use App\User;
use Mail;
use Carbon\Carbon;


class ForgotPasswordController extends Controller
{
    //1. Trả về view lấy email
   function getForgotPassword(){
     return view('backend.password.emailReset');
   }
   //2. gửi request để tạo mã code
   public function postemailForgotPassword(forgetEmail $r)
   {
      //dd($r->all());
      $email = $r->email;
      // check ktra email có trong bảng user hay ko
      $checkUser = User::where('email',$email)->first();
      if(!$checkUser)
      {
        return redirect()->back()->with('danger','Email not found!!!');
      }
      // tạo ra mã code
      $code = bcrypt(md5(time().$email));

      $checkUser->code= $code;
      $checkUser->time_code = Carbon::now();
      $checkUser->save();
      $check = $checkUser->toarray();

      $url = route('get.link.form.reset',['code' => $checkUser->code, 'email' => $email]);
      $data = [
        'route' => $url
      ];
// dd($data);
      Mail::send('backend.password.sendMailReset', $data, function ($message) use($email) {
            $message->from('datn20191@gmail.com', 'FriendTravels');
            $message->to( $email, 'Khach Hang');
            $message->subject('Cấp lại Mật khẩu');
        

        });
      return redirect()->back()->with('success','Link lấy lại mật khẩu đã được gửi vào mail của ban');
      // dd($code);

   }
   // 3. tạo password ms

   function resetpassword(Request $r){
    // dd($r->all());
    $code = $r->code;
    $email = $r->email;
    
    $checkUser = User::where([
        'code' => $code,
        'email' => $email
    ])->first();
    // dd($checkUser);
    if(!$checkUser)
    {
        return redirect('/admin')->with('danger','đường dẫn lấy mật khẩu ko đúng .Bạn vui lòng thử lại sau');
    }
  //dd($checkUser);

    return view('backend.password.reset',compact('checkUser'));
   }

   function postresetpassword(ForgotPasswordRequest $r)
   {
       //dd($r->all());
       if ($r->password) {
            $code = $r->code;
            $email = $r->email;
            
            $checkUser = User::where([
                'code' => $code,
                'email' => $email
            ])->first();
            // dd($checkUser);
            if(!$checkUser)
            {
                return redirect('/admin')->with('danger','đường dẫn lấy mật khẩu ko đúng .Bạn vui lòng thử lại sau');
            }

            $checkUser->password = bcrypt($r->password);
            $checkUser->save();
            return redirect('/admin/login')->with('success','success!!');

    //dd($r->all());
           
       }
       return redirect()->back()->with('danger','danger');
   }
}
