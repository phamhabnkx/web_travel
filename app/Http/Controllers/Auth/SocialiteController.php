<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\{Socials,User};
use Socialite;
use Auth;

use App\Http\Requests;
use Illuminate\Http\Request;




class SocialiteController extends Controller
{
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();
      // dd($user);

        $social  = Socials::where('provider_user_id',$user->getId())->where('provider','facebook')->first();
        if ($social) {
            $u = User::where('email',$user->getEmail())->first();
            Auth::login($u);
            return redirect('/');

        }else{
            $temp = new Socials();
            $temp->provider_user_id = $user->getId();
            $temp->provider = 'facebook';
             $u = User::where('email',$user->getEmail())->first();
             if (!$u) {
                 $u = User::create([
                   'full'=>$user->getName(),
                    'email'=>$user->getEmail(),
                    'password'=>bcrypt(md5(time().$user->getEmail())),
                    'level'=>'3',
                 ]);
                 $u->active = 2;
                 $u->save();
             }
             $temp->user_id = $u->id;
             $temp->save();
             Auth::login($u);
             return redirect('/');

        }
        // $user->token;
    }



    public function redirectToProviderGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallbackGoogle()
    {
        $actives = '2'; 
        $user = Socialite::driver('google')->user();
       //dd($user);
        //dd($user->user['id']);

        $social  = Socials::where('provider_user_id',$user->user['id'])->where('provider','google')->first();
        //dd($social);
        if ($social) {
            $u = User::where('email',$user->user['email'])->first();
                  //dd( $u);
            Auth::login($u);
            return redirect('/');


        }else{
            $temp = new Socials();
            //dd('hello');
            $temp->provider_user_id = $user->user['id'];
            $temp->provider = 'google';
            $u = User::where('email',$user->user['email'])->first();
             if (!$u) {
               
                 $u = User::create([
                    'full'=>$user->user['name'],
                    'email'=>$user->user['email'],
                    'password'=>bcrypt(md5(time().$user->getEmail())),
                    'level'=>'3',
                 ]);
                 $u->active = 2;
                 $u->save();
               
             }
             $temp->user_id = $u->id;
             $temp->save();

             Auth::login($u);
             return redirect('/');

        }
        // $user->token;
    }
}
