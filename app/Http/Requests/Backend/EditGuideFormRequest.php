<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class EditGuideFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
   public function rules()
    {
        // dd($this->route());
        return [
           'email'=>'required|unique:guide,email,'.$this->route('guide').',id',
        ];
    }

     public function messages()
    {
        return [
            'email.required'=>'Tên danh mục không được để trống',
            'email.unique'=>'Tên danh mục không được trùng',
          
        ];
    }
}
