<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class ForgotPasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => 'required',
            'password_confirm' => 'required|same:password'
        ];
    }
    public function messages()
    {
        return [
            'password.required'=>'vui lòng điền password',
            'password_confirm.required'=>'vui lòng điền xác nhận lại password',
            'password_confirm.same'=>'Mật khẩu xác nhận ko đúng',
            
           
        ];
    }
}
