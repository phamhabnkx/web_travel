<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class EditPhuongTienFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pt_code'=>'required|unique:phuong_tien,pt_code,'.$this->route('phuong_tien').',id',
        ];
    }

    public function messages()
    {
        return [
            'pt_code.required'=>'Tên danh mục không được để trống',
            'pt_code.unique'=>'Tên danh mục không được trùng',
          
        ];
    }
}
