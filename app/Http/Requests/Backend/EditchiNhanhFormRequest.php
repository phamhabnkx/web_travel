<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class EditchiNhanhFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required|unique:chi_nhanh,email,'.$this->route('chi_nhanh').',id',
            'ten_chi_nhanh'=>'required', 
            'address'=>'required', 
        ];
    }

      public function messages()
    {
        return [
            'email.required'=>'email không được để trống',
            'email.unique'=>'email không được trùng',
            'ten_chi_nhanh.required'=>'Không được để trống ',
            'address.required'=>'Không được để trống ',
          
        ];
    }
}
