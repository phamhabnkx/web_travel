<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class EditTourFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        // dd($this->route());
        return [
           'code'=>'required|min:3|unique:tour,code,'.$this->route('tour').',id' ,
            'name'=>'required|min:3',
            'price'=>'required|numeric',
           
        ];
    }
    public function messages()
    {
        return [
            'code.required'=>'Không được để trống Mã tour',
            'code.unique'=>'khong duoc trung',
            'code.min'=>'Mã Sản phâmr không được nhỏ hơn 3 ký tự',
            'name.required'=>'Không được để trống Tên tour',
            'name.min'=>'Tên tour không được nhỏ hơn 3 ký tự',
            'price.required'=>'Không được để trống Giá tour',
            'price.numeric'=>'Giá tour không đúng định dạng',
            
        ];
    }
}
