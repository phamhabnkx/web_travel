<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class editBlogDetailsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd($this->route());
        return [
            'name'=>'required',
            'content'=>'required',
            'bai_viet_code'=>'required|unique:blog_details,bai_viet_code,'.$this->route('blog_detail').',id',
            
        ];
    }
}
