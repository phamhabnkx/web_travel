<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class forgetEmail extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'=>'required|email|min:5',
           
        ];
    }
    public function messages()
    {
        return [
            'email.required'=>'Email không được để trống',
            'email.email'=>'email không đúng định dạng',
            'email.min'=>'email không được ít hơn 5 ký tự',
           
        ];
    }
}
