<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class EditHotelFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ks_code'=>'required|unique:hotel,ks_code,'.$this->id.',id',
            
             'ks_name'=>'required',
             'ks_tinh_trang'=>'required',
          
             'ks_address'=>'required',
             
        ];
    }
}
