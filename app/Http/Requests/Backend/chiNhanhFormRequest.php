<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;

class chiNhanhFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'email'=>'required|min:3|unique:chi_nhanh,email', 
           'ten_chi_nhanh'=>'required', 
           'address'=>'required', 
        ];
    }

     public function messages()
    {
        return [
            'email.required'=>'Không được để trống email',
            'ten_chi_nhanh.required'=>'Không được để trống ',
            'address.required'=>'Không được để trống ',
            'email.unique'=>'Email đã tồn tại',
           
        ];
    }
}
