<?php

namespace App\Http\Requests\Frontend;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email_user'=>'required|email|min:5|unique:users,email',
            'password_user'=>'required|min:6',
            'password_confirm' => 'required|same:password_user'
        ];
    }
    public function messages()
    {
        return [
            'email_user.required'=>'Email không được để trống',
            'email_user.email'=>'email không đúng định dạng',
            'email_user.unique'=>'Email Không được trùng!',
            'email_user.min'=>'email không được ít hơn 5 ký tự',
            'password_user.required'=>'password không được để trống',
            'password_user.min'=>'Không ít hơn 5 ký tự',
            'password_confirm.required'=>'vui lòng điền xác nhận lại password',
            'password_confirm.same'=>'Mật khẩu xác nhận ko đúng',
        ];
    }
}
