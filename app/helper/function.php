

<?php  
function showErrors($errors,$nameinput)
{
	 	if($errors->has($nameinput))
	 	{
            echo '<div class="alert alert-danger" role ="alert"><strong>';
            echo  $errors->first($nameinput);
            echo '</strong></div>';
       }
}

function getAddress($danhMuc,$idCha,$chuoiTab,$idSelected=null)
{
	foreach($danhMuc as $banGhi)
	{
		if($banGhi['parent']==$idCha)
		{
			if($banGhi['id']==$idSelected)
			{
				echo '<option selected value="'.$banGhi['id'].'">'.$banGhi['name'].'</option>';

			}else 
			{
				echo '<option value="'.$banGhi['id'].'">'.$chuoiTab.$banGhi['name'].'</option>'	;
			}
			
			getAddress($danhMuc,$banGhi['id'],$chuoiTab.'---|',$idSelected);
		}
		
	}	
   
}


function getAddressdanhmuc($danhMuc,$idCha,$chuoiTab)
{
	foreach($danhMuc as $banGhi)
	{
		if($banGhi['parent']==$idCha)
		{
			echo '<div class="item-menu"><span>'.$chuoiTab.$banGhi['name'].'</span>
										<div class="category-fix">
											<a class="btn-category btn-primary" href="/admin/dia-diem/'.$banGhi['id'].'/edit"><i class="fa fa-edit"></i></a>
											<a class="btn-category btn-danger"  onclick="onDelete('.$banGhi['id'].')" href="javascript:void(0)"><i class="fas fa-times"></i></a>

										</div>
									</div>'	;
			
			getAddressdanhmuc($danhMuc,$banGhi['id'],$chuoiTab.'---|');
		}
		
	}	
   
}


function getAddresstour($danhMuc,$idCha,$chuoiTab)
{
	foreach($danhMuc as $banGhi)
	{
		if($banGhi['parent']==$idCha)
		{
			echo '<option value="'.$banGhi['id'].'">'.$chuoiTab.$banGhi['name'].'</option>'	;
			
			getAddresstour($danhMuc,$banGhi['id'],$chuoiTab.'---|');
		}
		
	}	
   
}

function getEditAddresstour($danhMuc,$idCha,$chuoiTab,$idSelected=null)
{
	foreach($danhMuc as $banGhi)
	{
		if($banGhi['parent']==$idCha)
		{
			if($banGhi['id']==$idSelected)
			{
				echo '<option selected value="'.$banGhi['id'].'">'.$chuoiTab.$banGhi['name'].'</option>';

			}else 
			{
				echo '<option value="'.$banGhi['id'].'">'.$chuoiTab.$banGhi['name'].'</option>'	;
			}
			
			getEditAddresstour($danhMuc,$banGhi['id'],$chuoiTab.'---|',$idSelected);
		}
		
	}		
   
}

function getpt($danhMuc,$idCha)
{
	foreach($danhMuc as $banGhi)
	{
		if($banGhi['parent']==$idCha)
		{
			echo '<div class="xe_phuong_tien"><input type="checkbox" value="'.$banGhi['id'].'" name="phuong_tien_id[]" >  '.$banGhi['pt_name'].' + '.$banGhi['pt_cho'].'chỗ</div>'	;
			
			getpt($danhMuc,$banGhi['id']);
		}
		
	}	
   
}




function getLevel($danhMuc,$idParent,$soCap)
{
	foreach($danhMuc as $banGhi)
	{
		if($banGhi['id']==$idParent)
		{
			$soCap++;
			if($banGhi['parent'] == 0)
			{
				return $soCap;
			}
			return getLevel($danhMuc,$banGhi['parent'],$soCap);
		}
	}
}


function check_value($tour,$value_check){
	foreach ($tour->Tour_and_Phuong_tien as $value) {
		if ( $value->id == $value_check) {
			return true;
		}
	}
	return false;
	// dd(123);

}

function check_value_nv($tour,$value_check){
	foreach ($tour->Tour_and_Guide as $value) {
		if ( $value->id == $value_check) {
			return true;
		}
	}
	return false;
	// dd(123);

}

function check_value_hotel($tour,$value_check){
	foreach ($tour->Tour_and_Hotel as $value) {
		if ( $value->id == $value_check) {
			return true;
		}
	}
	return false;
	// dd(123);

}

if (!function_exists('get_data_user')) {

	function get_data_user($type,$filder='id'){
		return Auth::guard($type)->user()?Auth::guard($type)->user()->$filder:'';
	}
}