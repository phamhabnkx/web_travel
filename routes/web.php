<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*---------------------------------------------------Frontend-------------------------------------*/
Route::group(['namespace' => 'Frontend'], function() {
	// Route::resource('home', 'HomeController');
    Route::get('', 'indexController@getIndex' );
    Route::get('/search', 'indexController@getsearch' );
    Route::get('/search/second', 'indexController@getsearchSecond' );
	
	Route::get('contact', 'indexController@getContact');
	

	Route::group(['prefix'=>'cart'],function()
	{
		Route::get('', 'cartController@getCart');
		Route::get('add-item', 'cartController@addItem');
		Route::get('update-item/{rowId}/{qty}', 'cartController@updateItem');
		Route::get('del-item/{rowId}', 'cartController@delItem');
		Route::post('/checkout','cartController@postCheckout');
		Route::get('/complete/{id}','cartController@getcomplete' );

	});

	
	Route::group(['prefix'=>'tour'],function()
	{
		Route::get('', 'tourController@getTour');
		Route::get('tour-details/{str}.html', 'tourController@getTourDetails');
	
	});
	Route::group(['prefix' => 'tin-tuc'], function() {
	    Route::get('', 'tinTucController@getshowTinTuc');
	    Route::get('tin-tuc-details/{str}.html', 'tinTucController@getshowTinTucDetails');
	});
	Route::group(['prefix' => 'ajax','middleware'=>'loginUser'], function() {
	    Route::post('/danh-gia/{id}', 'RatingController@saveRating')->name('post.rating.product');
	    
	});
	Route::post('/ajax/view-product','indexController@renderProductView');
	Route::get('/login','loginFrontendController@getlogin');
	Route::post('/login','loginFrontendController@postlogin');
	Route::post('/register','loginFrontendController@postRegister');
	Route::get('/xac-nhan-tai-khoan','loginFrontendController@verifyEmailAcount')->name('get.verify.Acount');

	Route::get('/logout', 'loginFrontendController@getlogout');
	Route::get('/inforCus', 'informationFrontendController@getInfor');
	Route::post('/inforCus', 'informationFrontendController@postInfor');

	Route::get('/xac-thuc-lai', 'loginFrontendController@getVerifyAgain');
	Route::post('/xac-thuc-lai', 'loginFrontendController@postEmailVerifyAgain');
	Route::get('/xac-thuc-lai/reset', 'loginFrontendController@resetCodeEmailVerifyAgain')->name('get.link.code.verify');



});





Route::group(['prefix'=>'admin','namespace' => 'Backend'], function() {
	Route::get('/login','loginController@getLogin')->middleware('logoutadmin');
	Route::post('login', 'loginController@postlogin');
});
Route::group(['prefix'=>'admin','namespace' => 'Auth'], function() {
	Route::get('/forgotPassword', 'ForgotPasswordController@getForgotPassword')->name('get.fogort');

	Route::post('/forgotPassword', 'ForgotPasswordController@postemailForgotPassword');// link lấy email
	//resetpassword
	Route::get('/password/reset', 'ForgotPasswordController@resetpassword')->name('get.link.form.reset');
	Route::post('/password/reset', 'ForgotPasswordController@postresetpassword');

});


/*--------------------------------------------------Admin--------------------------------------*/

Route::group(['prefix'=>'admin','namespace' => 'Backend','middleware'=>'loginadmin'], function() {
	
    Route::get('/', 'indexController@getindex');


    Route::resource('user', 'UserController')->middleware(['can:show']);

    Route::resource('dia-diem', 'AddressControler');
    Route::resource('phuong-tien', 'PhuongTienControler');
    Route::resource('guide', 'GuideControler');

   	Route::group(['prefix'=>'order'],function()
	{
		Route::get('detail/{id}', 'orderController@getdetail');
		Route::get('pay/{id}', 'orderController@getpay');
		Route::get('delete/{id}/{x}', 'orderController@getdelete');
		Route::get('','orderController@getlist');
		Route::get('processed','orderController@getprocessed');
	});

	Route::resource('tour', 'TourController');
	Route::resource('chi-nhanh', 'ChiNhanhController');
	Route::resource('option', 'OptionController');
	Route::resource('blog', 'BlogController');
	Route::resource('blog-details', 'BlogDetailsController');

	Route::group(['prefix'=>'hotel'],function()
	{
		Route::get('', 'hotelController@gethotel');
		
		Route::get('/add','hotelController@getAddHotel');
		Route::post('/add','hotelController@postAddHotel');

		Route::get('/edit/{id}','hotelController@getEditHotel');
		Route::post('/edit/{id}','hotelController@postEditHotel');


		Route::get('/del/{id}','hotelController@delHotel');




	});
	Route::get('logout', 'indexController@getlogout');

	

   

});


/*Định tuyến đăng nhập bằng google và facebook*/
Route::get('facebook/redirect', 'Auth\SocialiteController@redirectToProvider');
Route::get('facebook/callback', 'Auth\SocialiteController@handleProviderCallback');

Route::get('google/redirect', 'Auth\SocialiteController@redirectToProviderGoogle');
Route::get('google/callback', 'Auth\SocialiteController@handleProviderCallbackGoogle');